@props(['section' => null , 'subsection' => null ])
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Joanan Sierra Sánchez">
    <!-- <meta http-equiv="refresh" content="{{ (config('session.lifetime') * 60) + 3 }}"> -->
    <title>{{ config('app.name') }}</title>

    <!-- <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" type="text/css"> -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <link rel="stylesheet" href="{{asset('vendor/fontawesome/css/all.min.css')}}">

    <!-- WireUI -->
    @wireUiScripts

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    @stack('css')

</head>

<body class="font-sans antialiased" id="body-url" data-url="{{ url('/') }}">

    <x-layout.aside class="relative" />
    <!-- Page Content -->
    <main class="wrapper__main__container" data-token="{{ Auth::user()->token_api }}" data-key="{{ Auth::user()->id }}">
        <x-layout.nav-bar :section="$section" :subsection="$subsection" />

        <section class="main-container-content px-2 pt-6 pb-24 md:px-5 lg:px-10">
            {{ $slot }}
        </section>

        <x-layout.footer />
    </main>

    @livewireScripts

    <script src="{{ asset('vendor/jquery/jquery-3.7.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/jquery/jquery.ba-throttle-debounce.js') }}"></script>
    <script src="{{ asset('js/jquery-aside.js') }}" type="text/javascript"></script>

    <script src="https://cdn.jsdelivr.net/npm/@ryangjchandler/alpine-tooltip@1.x.x/dist/cdn.min.js" type="text/javascript"></script>

    @stack('scripts')


</body>

</html>