<x-guest-layout>
    <div class="w-80 lg:w-1/3 2xl:w-1/4  max-w-md bg-white rounded-lg border-t-4 border-t-green-700 shadow-xl">
        <div class="mt-5 mb-2 flex justify-center items-center flex-wrap px-5 py-2">
            <img class="h-auto w-96" src="{{ asset('img/logo_nuevo.png') }}" alt="" /><br>
            <span class="mt-6 px-2 py-0.5 ml-3 text-xs sm:text-sm md:text-base font-semibold text-white bg-green-800 rounded-md">{{ config('app.name') }}</span>
        </div>

        <form class="p-5" method="POST" enctype="multipart/form-data" action="{{ route('login.store') }}">
            @csrf

            <x-flash-message />

            <div class="mb-6 group">
                <x-input label="{{ __('Nombre de usuario') }}" placeholder="Ingrese su nombre de usuario" name="username" value="{{ old('username') }}" />
            </div>

            <div class="mb-6 group">
                <x-password label="{{ __('Contraseña') }}" placeholder="Ingrese su contraseña" name="password" value="{{ old('password') }}" />
            </div>

            <button type=" submit" class="block m-auto text-white bg-green-800 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 font-medium rounded text-sm w-full sm:w-auto px-5 py-1.5 text-center">Accesar</button>
            <p class="mt-5 lg:mt-10 text-center text-xs font-normal text-slate-500">Dudas y aclaraciones ext. 5428</p>
        </form>
    </div>
</x-guest-layout>