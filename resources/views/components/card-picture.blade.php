
<div {{ $attributes->merge(['class' => 'bg-white rounded-lg border border-slate-200 shadow-md'] ) }}>
    {{ $slot }}
</div>