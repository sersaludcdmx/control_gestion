@props(['href' => false])

@if ($href)
<a {{ $attributes->merge([
        'class' => 'flex space-x-3 items-center bg-teal-800 hover:bg-teal-700 text-white focus:ring-2 focus:outline-none focus:ring-teal-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5'
    ]) }}>
    {{ $slot }}
</a>
@else
<button {{ $attributes->merge([
        'type' => 'submit',
        'class' => 'flex space-x-3 items-center bg-teal-800 hover:bg-teal-700 text-white focus:ring-2 focus:outline-none focus:ring-teal-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5'
    ]) }}>
    {{ $slot }}
</button>
@endif