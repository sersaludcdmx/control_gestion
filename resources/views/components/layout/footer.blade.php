<footer class="absolute bottom-0 w-full flex flex-row-reverse bg-white border-slate-200 px-10 py-5 border-t">
    <span class="text-xs sm:text-sm md:text-base text-slate-500">Servicios de Salud Pública CDMX Copyright © {{ date('Y') }}</span>
</footer>