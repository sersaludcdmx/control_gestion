@props(['section' => null, 'subsection' => null])
<nav class="flex justify-between bg-white border-b border-slate-200 h-16 sticky top-0 z-30">
    <section class="px-4 py-5 toggle__bar">
        <a href="#" class="toggle__menu__bar text-base text-slate-500"><i class="fas fa-bars"></i></a>
        <a href="#" class="hidden md:inline-flex justify-center items-center px-2 py-0.5 ml-3 text-xs sm:text-sm font-bold text-white bg-green-800 rounded-full">{{ config('app.name') }}</a>
    </section>


    <section class="relative flex items-center order-last pl-4 py-4  border-l border-slate-200">


        <!-- <img class="w-5 h-5 ring-1 md:w-7 md:h-7 p-1 md:right-2 rounded-full ring-gray-500" src="{{ asset('img/profile.png') }}" alt="Bordered avatar"> -->

        <x-dropdown>
            <x-slot name="trigger">
                <div class="flex items-center w-full order-last hover:bg-transparent md:w-auto text-xs sm:text-sm md:text-base text-slate-600 space-x-4 min-w-2 pr-7">
                    <button class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition">
                        <img class="h-8 w-8 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                    </button>
                    <p class="text-xs sm:text-sm">{{ Auth::user()->name }}</p>
                    <i class="fa-solid fa-angle-down text-sm font-bold"></i>
                </div>
            </x-slot>
        

            <x-dropdown.item class="hover:bg-gray-100 !text-red-700" separator label="Cerrar session" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" />
        </x-dropdown>

        <!-- Dropdown menu -->
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </section>
</nav>