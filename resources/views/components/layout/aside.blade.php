<aside>

    <nav class="wrapper__aside">
        <a href="#" class="flex items-center justify-center py-1.5 px-4 border-b border-slate-200">
            <img src="{{ asset('img/banner-alternativo.png') }}" class="w-full h-auto" alt="Logo SSPCDMX" />
        </a>
        <ul class="nav__main__container">

            <li class="list__item">
                <a href="{{ route('home.index') }}" class="@if(request()->routeIs('home.index') )  active  @endif ">
                    <i class="fas fa-home"></i>
                    <span>Inicio</span>
                </a>
            </li>

            @canany(['users.index', 'roles.index', 'permissions.index'] )
            <li class="list__item down__item">
                <a class="@if(request()->routeIs('users.index') || request()->routeIs('permissions.index') || request()->routeIs('roles.index') ) down__active @endif">
                    <i class="fa-solid fa-user-group"></i>
                    <span>Gestión de usuarios</span>
                    <i class="fa-solid fa-chevron-left"></i>
                </a>
                <ul class="list__down__container">

                    @can('permissions.index')
                    <li class="list__item">
                        <a href="{{ url('permisos') }}" class="@if( request()->routeIs('permissions.index') ) active @endif">
                            <i class="fa-solid fa-puzzle-piece"></i>
                            <p class="ml-3">Permisos</p>
                        </a>
                    </li>
                    @endcan

                    @can('roles.index')
                    <li class="list__item">
                        <a href="{{ url('roles') }}" class="@if(request()->routeIs('roles.index') ) active @endif">
                            <i class="fa-solid fa-user-tie"></i>
                            <p class="ml-3">Roles</p>
                        </a>
                    </li>
                    @endcan

                    @can('users.index')
                    <li class="list__item">
                        <a href="{{ route('users.index') }}" class="@if(request()->routeIs('users.index') ) active @endif">
                            <i class="fa-solid fa-users"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Usuarios</span>
                        </a>
                    </li>
                    @endcan

                </ul>
            </li>
            @endcanany

            @can('jurisdictions.index')
            <li class="list__item">
                <a href="{{ route('jurisdictions.index') }}" class="@if(request()->routeIs('jurisdictions.index') ) active @endif">
                    <i class="fa-solid fa-puzzle-piece"></i>
                    <span>Jurisdicciones</span>
                </a>
            </li>
            @endcan

            @can('documents.index')
            <li class="list__item">
                <a href="{{ route('documents.index') }}" class="@if(request()->routeIs('documents.index') ) active @endif">
                    <i class="fa-regular fa-folder-open"></i>
                    <span>Gestión de documentos</span>
                </a>
            </li>
            @endcan

            @can('pending.shifts.index')
            <li class="list__item">
                <a href="{{ route('shifts.index') }}" class="@if(request()->routeIs('shifts.index') ) active @endif">
                    <i class="fas fa-file-signature"></i>
                    <span>Turnos pendientes</span>
                </a>
            </li>
            @endcan

            @can('shifts.completed.index')
            <li class="list__item">
                <a href="{{ route('completed.index') }}" class="@if(request()->routeIs('completed.index') ) active @endif">
                    <i class="fa-regular fa-flag"></i>
                    <span>Turnos concluidos</span>
                </a>
            </li>
            @endcan

            @can('ccp.index')
            <li class="list__item">
                <a href="{{ route('copies.index') }}" class="@if(request()->routeIs('copies.index') ) active @endif">
                    <i class="fa-solid fa-copy"></i>
                    <span>Cccp</span>
                </a>
            </li>
            @endcan

            @can('reports.index')
            <li class="list__item">
                <a href="{{ route('reports.index') }}" class="@if(request()->routeIs('reports.index') ) active @endif">
                    <i class="fa-regular fa-floppy-disk"></i>
                    <span>Reportes</span>
                </a>
            </li>
            @endcan
        </ul>
    </nav>
</aside>