@props(['livewire' => false])
<!-- || ($openType != '' && $openType == 'success') -->
<div x-data="{open: @if ($livewire)
        @entangle('alert').live
    @else
        true
    @endif}" x-show="open">

    @if ( $message = Session::get('success') )
    <div class="flex items-start p-6 border-l-4 border-green-600 rounded-r-md bg-green-200 my-2">
        <svg class="flex-shrink-0 w-5 h-5 text-green-600" fill="currentColor" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
        </svg>
        <div class="ml-3 text-sm text-start font-medium text-green-700">
            <span class="font-medium">{{ $message }}</span>
        </div>
        <button x-on:click="open=false" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-200 text-green-700 rounded-full focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-300 flex">
            <span class="sr-only">Close</span>
            <svg class="w-3.5 h-3.5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
            </svg>
        </button>
    </div>

    @elseif ($message = Session::get('danger') )
    <div class="flex items-start p-6 border-l-4 border-red-600 rounded-r-md bg-red-200 my-2" role="alert">
        <svg class="flex-shrink-0 w-5 h-5 text-red-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd"></path>
        </svg>
        <div class="ml-3 text-sm text-start font-medium text-red-700">
            <span class="font-medium">{{ $message }}</span>
        </div>
        <button x-on:click="open=false" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-red-200 text-red-700 rounded-full focus:ring-2 hover:bg-red-300 focus:ring-red-400 p-1.5 flex">
            <span class="sr-only">Close</span>
            <svg class="w-3.5 h-3.5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
            </svg>
        </button>
    </div>
    @elseif ($message = Session::get('warning') )
    <div class="flex items-start p-6 border-l-4 border-yellow-600 rounded-r-md bg-yellow-100 my-2" role="alert">
        <svg class="flex-shrink-0 w-5 h-5 text-yellow-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path>
        </svg>
        <span class="sr-only">Info</span>
        <div class="ml-3 text-sm text-start font-medium text-yellow-700">
            <span class="font-medium">{{ $message }}</span>
        </div>
        <button x-on:click="open=false" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-yellow-100 text-yellow-500 rounded-full focus:ring-2 focus:ring-yellow-400 p-1.5 hover:bg-yellow-200 flex">
            <span class="sr-only">Close</span>
            <svg aria-hidden="true" class="w-3.5 h-3.5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
            </svg>
        </button>
    </div>
    @elseif ($message = Session::get('info'))
    <div class="flex items-start p-6 border-l-4 border-blue-600 rounded-r-md bg-blue-200 my-2" role="alert">
        <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-blue-700 dark:text-blue-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
        </svg>
        <span class="sr-only">Info</span>
        <div class="ml-3 text-sm text-start font-medium text-blue-700 dark:text-blue-800">
            <span class="font-medium">{{ $message }}</span>
        </div>
        <button x-on:click="open=false" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-blue-200 text-blue-500 rounded-full focus:ring-2 focus:ring-blue-400 p-1.5 hover:bg-blue-300 flex">
            <span class="sr-only">Close</span>
            <svg aria-hidden="true" class="w-3.5 h-3.5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
            </svg>
        </button>
    </div>

    @elseif ($errors->any())
    <div :class="{ 'flex': {{$errors->any()}} }" class="flex items-start p-6 border-l-4 border-red-600 rounded-r-md bg-red-200 my-2" role="alert">
        <svg class="flex-shrink-0 w-5 h-5 text-red-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd"></path>
        </svg>
        <div class="ml-3 text-sm font-medium text-red-700">
            <span class="font-medium">Por favor revise el formulario para ver si hay errores.</span>
        </div>
        <button x-on:click="open=false" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-red-200 text-red-700 rounded-full focus:ring-2 hover:bg-red-300 focus:ring-red-400 p-1 flex">
            <span class="sr-only">Close</span>
            <svg class="w-3.5 h-3.5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
            </svg>
        </button>
    </div>

    
    @endif
</div>