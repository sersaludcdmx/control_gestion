@props(['search'])
<div class="flex items-center justify-center mb-4">
    <div class="flex items-center h-10">
        <div class="flex rounded-lg w-52 lg:w-96">
            <div class="relative flex-grow focus-within:z-10">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg class="w-4 md:w-5 h-4 md:h-5 text-gray-400" viewBox="0 0 20 20" stroke="currentColor" fill="none">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                    </svg>
                </div>
                <form method="GET" role="search" id="search">
                    <input autocomplete="off" class="block w-full py-2 border-0 border-b-2 border-slate-400 pl-10 text-xs md:text-sm text-slate-600 text-semibold focus:border-slate-400 focus:ring-0 focus:outline-none" placeholder="Buscar..." type="text" name="username" id="text-search">
                    <div class="absolute inset-y-0 right-0 flex items-center pr-2">
                        <button class="text-slate-400 hover:text-green-600 focus:outline-none" type="button">
                            <svg class="stroke-current w-4 md:w-5 h-4 md:h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>