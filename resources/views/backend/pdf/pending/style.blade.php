<style>
    .w-200 {
        width: 200px;
    }

    .m-0 {
        margin: 0px;
    }

    .px {
        padding-left: 10px;
        padding-right: 10px;
    }

    .py {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .text-sm {
        font-family: 'lato';
        font-size: 9pt;
        color: #55585A;
    }

    .bg-grey {
        background: #eaeded;
    }

    .text-center {
        text-align: center;
    }

    .border_top {
        border-top: solid 1px #d5dbdb;
    }

    .border_left {
        border-left: solid 1px #d5dbdb;
    }

    .border_right {
        border-right: solid 1px #d5dbdb;
    }

    .border_bottom {
        border-bottom: solid 1px #d5dbdb;
    }

    .bold {
        font-weight: bold;
    }

    .italic {
        font-style: italic;
    }

    .w-16 {
        width: 16%;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }


    /*========== Inicio de estilos del membrete ==========*/
    .container-header {
        width: 400px;
        float: left;
    }


    @page {
        header: page-header;
        footer: page-footer;
    }
</style>