@php
$cherry = "#9d2148";
$golden = "#b28e5c";
$grey = "#55585A";
@endphp

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exportación de documento</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">

    @include('backend.pdf.pending.style')
</head>

<body>
    <div class="body">

        <!-- Tabla de detalles -->
        <table>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Reporte del turnado:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom" align="right">{{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="font-size: 10pt;" class="bg-grey py bold text-sm border_top border_left border_right border_bottom">Documento</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Nombre del usuario que registro el documento:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->user->name. ' '. $shift->document->user->paternal_surname. ' ' .$shift->document->user->maternal_surname }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Fecha de recepción:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ \Carbon\Carbon::parse($shift->document->reception_date)->isoFormat('D MMM YYYY') }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Número de volante:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->volante }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Fecha del documento:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ \Carbon\Carbon::parse($shift->document->document_date)->isoFormat('D MMM YYYY') }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Unidad administrativa:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document-> admin_unit}}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Referencia:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->reference }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Remitente:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->sender }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Cargo administrativo remitente:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->admin_position }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Destinatario:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->addressee }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Cargo administrativo destinatario:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->addres_position }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Prioridad del documento: </td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->priority->name }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Tipo de documento:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->type->name }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Anexo:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->annex->name }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Cantidad de anexos:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->document->quantity }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Asunto:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{!! nl2br($shift->document->subject) !!}</td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="background: #eaeded; font-size: 10pt;" class="py bold text-sm border_top border_left border_right border_bottom">Turnado</td>
            </tr>

            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Nombre del usuario que registro del turno:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->user->name. ' '. $shift->user->paternal_surname. ' ' .$shift->user->maternal_surname }}</td>
            </tr>
 
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Área:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->area }}</td>
            </tr>

            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Turnado a:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->turn }}</td>
            </tr>

            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Fecha de turno:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->turn_date }}</td>
            </tr>

            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Estatus:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->status == 'PEN' ? 'PENDIENTE' : 'CONCLUIDO' }}</td>
            </tr>

            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Indicación:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->indication->name }}</td>
            </tr>

            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Prioridad del turno:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $shift->priority->name }}</td>
            </tr>

            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Observaciones:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{!! nl2br($shift->observations) !!}</td>
            </tr>

            @foreach ($shift->copy as $copy)
                <tr>
                    <td colspan="2" align="center" style="background: #eaeded; font-size: 10pt;" class="py bold text-sm border_top border_left border_right border_bottom">Cccp</td>
                </tr>

                <tr>
                    <td class="px py bold text-sm border_top border_left border_right border_bottom">Dependecia/Unidad Administrativa:</td>
                    <td class="px py text-sm border_top border_left border_right border_bottom">{{ $copy->area }}</td>
                </tr>

                <tr>
                    <td class="px py bold text-sm border_top border_left border_right border_bottom">Responsable:</td>
                    <td class="px py text-sm border_top border_left border_right border_bottom">{{ $copy->responsable }}</td>
                </tr>

                <tr>
                    <td class="px py bold text-sm border_top border_left border_right border_bottom">Observaciones:</td>
                    <td class="px py text-sm border_top border_left border_right border_bottom">{!! nl2br($copy->description) !!}</td>
                </tr>
            @endforeach
        </table>
        <br>

    </div>

    @include('backend.pdf.pending.header')

</body>

</html>