@php
$cherry = "#9d2148";
$golden = "#b28e5c";
$grey = "#55585A";
@endphp

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exportación de documento</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">

    @include('backend.pdf.document.style')
</head>

<body>
    <div class="body">

        <!-- Tabla de detalles -->
        <table>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Reporte inicial:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom" align="right">{{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Nombre del usuario que registro el documento:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->user->name. ' '. $document->user->paternal_surname. ' ' .$document->user->maternal_surname }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Fecha de recepción:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ \Carbon\Carbon::parse($document->reception_date)->isoFormat('D MMM YYYY') }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Número de volante:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->volante }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Fecha del documento:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ \Carbon\Carbon::parse($document->document_date)->isoFormat('D MMM YYYY') }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Unidad administrativa:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document-> admin_unit}}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Referencia:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->reference }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Remitente:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->sender }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Cargo administrativo remitente:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->admin_position }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Destinatario:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->addressee }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Cargo administrativo destinatario:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->addres_position }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Prioridad del documento: </td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->priority->name }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Tipo de documento:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->type->name }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Anexo:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->annex->name }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Cantidad de anexos:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{{ $document->quantity }}</td>
            </tr>
            <tr>
                <td class="px py bold text-sm border_top border_left border_right border_bottom">Asunto:</td>
                <td class="px py text-sm border_top border_left border_right border_bottom">{!! nl2br($document->subject) !!}</td>
            </tr>
        </table>
        <br>

    </div>

    @include('backend.pdf.document.header')

</body>

</html>