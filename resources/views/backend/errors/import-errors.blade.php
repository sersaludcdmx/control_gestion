<table>
    <tr></tr>
    <tr>
        <td></td>
        <td colspan="2">Fecha de generación:</td>
        <td colspan="2">{{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
    </tr>

    <tr></tr>
    <tr></tr>
    <!-- Nombre de las columnas -->
    <thead>
        <tr>
            <th>#</th>
            @foreach (array_keys($information[0] ) as $item)
            <th>{{$item}}</th>
            @endforeach
        </tr>
    </thead>
    @php
    $index = 1;
    @endphp


    <tbody>
        @foreach ($information as $row)
        <tr>
            <td>{{ $index }}</td>
            @foreach ($row as $item => $key)
            @if ($loop->last)
            <td>
                @foreach ($key as $error )
                {{ $error ?? '' }}
                <br>
                @endforeach
            </td>
            @else
            <td>{{$key}}</td>
            @endif
            @endforeach
        </tr>
        @php
        $index++;
        @endphp

        @endforeach
    </tbody>
</table>