<x-app-layout>
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    @endpush


    <h5 class="text-sm sm:text-base md:text-lg lg:text-xl font-semibold text-slate-600">
        Bienvenid@:
        <br>
        <i class="fas fa-user-alt"></i> {{ Auth::user()->name }} {{ Auth::user()->paternal_surname }} {{ Auth::user()->maternal_surname }}
        <br>
        <i class="fas fa-calendar"></i> {{ strtoupper(\Carbon\Carbon::now()->isoFormat('D/MMMM/Y')) }}
    </h5>

    <br>

    @livewire('home.stats-component')

    @push('scripts')
    <script src="{{ asset('vendor/chartjs/chart.umd.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    
    <script src="{{asset('js/jquery-stats.js')}}" type="text/javascript"></script>
    @endpush
</x-app-layout>