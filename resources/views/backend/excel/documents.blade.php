<table>

    <tr>
        <td></td>
        <td colspan="8">Exportación de Documentos</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="8">Del {{$start_date->isoFormat('D MMM YYYY')}} al {{$end_date->isoFormat('D MMM YYYY')}}</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3">Fecha de exportación: {{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
        <td></td>
    </tr>

    <tr></tr>

    <tr>
        <td>No.</td>
        <td>Alta del registro</td>
        <td>Fecha de recepción</td>
        <td>Número de volante</td>
        <td>Fecha del documento</td>
        <td>Unidad administrativa</td>
        <td>Referencia</td>
        <td>Remitente</td>
        <td>Cargo administrativo remitente</td>
        <td>Destinatario</td>
        <td>Cargo administrativo destinatario</td>
        <td>Prioridad del documento</td>
        <td>Tipo de documento</td>
        <td>Anexo</td>
        <td>Cantidad de anexos</td>
        <td>Asunto</td>
    </tr>
    <tbody>

        @foreach ($documents as $document)

        <tr>
            <td>{{ ($loop->index + 1) }}</td>
            <td>{{ \Carbon\Carbon::parse($document->created_at)->isoFormat("DD MMM Y")}}</td>
            <td>{{ $document->reception_date }}</td>
            <td>{{ $document->volante }}</td>
            <td>{{ $document->document_date }}</td>
            <td>{{ $document->admin_unit }}</td>
            <td>{{ $document->reference }}</td>
            <td>{{ $document->sender }}</td>
            <td>{{ $document->admin_position }}</td>
            <td>{{ $document->addressee }}</td>
            <td>{{ $document->addres_position }}</td>
            <td>{{ $document->priority_name }}</td>
            <td>{{ $document->document_types_name }}</td>
            <td>{{ $document->annexes_name }}</td>
            <td>{{ $document->quantity }}</td>
            <td>{{ $document->subject }}</td>
        </tr>

        @endforeach


    </tbody>
</table>