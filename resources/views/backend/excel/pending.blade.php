<table>

    <tr>
        <td></td>
        <td colspan="8">Exportación de Turnos Pendientes</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="8">Del {{$start_date->isoFormat('D MMM YYYY')}} al {{$end_date->isoFormat('D MMM YYYY')}}</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3">Fecha de exportación: {{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
        <td></td>
    </tr>

    <tr></tr>

    <tr>
        <td>No.</td>
        <th>Fecha de recepción</th>
        <th>Referencia del documento</th>
        <th>Asunto del documento</th>
        <th>Número de volante</th>
        <th>Unidad administrativa</th>
        <th>Prioridad del turno</th>
        <th>Remitente</th>
        <th>Destinatario</th>
        <th>Area</th>
        <th>Turnado a</th>
        <th>Fecha de turno</th>
        <th>Estatus</th>
        <th>Indicación</th>
        <th>Observaciones</th>
    </tr>
    <tbody>

        @foreach ($documents as $document)

        <tr>
            <td>{{ ($loop->index + 1) }}</td>
            <td>{{ \Carbon\Carbon::parse($document->reception_date)->isoFormat("DD MMM Y")}}</td>
            <td>{{ $document->reference }}</td>
            <td>{{ $document->subject }}</td>
            <td>{{ $document->volante }}</td>
            <td>{{ $document->admin_unit }}</td>
            <td>{{ $document->priority }}</td>
            <td>{{ $document->sender }}</td>
            <td>{{ $document->addressee }}</td>
            <td>{{ $document->area }}</td>
            <td>{{ $document->turn }}</td>
            <td>{{ $document->turn_date }}</td>
            <td>{{ __('PENDIENTE') }}</td>
            <td>{{ $document->indications }}</td>
            <td>{{ $document->observations }}</td>
        </tr>

        @endforeach


    </tbody>
</table>