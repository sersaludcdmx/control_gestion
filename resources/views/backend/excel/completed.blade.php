<table>

    <tr>
        <td></td>
        <td colspan="8">Exportación de Turnos Completados</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="8">Del {{$start_date->isoFormat('D MMM YYYY')}} al {{$end_date->isoFormat('D MMM YYYY')}}</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3">Fecha de exportación: {{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
        <td></td>
    </tr>

    <tr></tr>

    <tr>
        <td>No.</td>
        <th>Fecha de conclusión</th>
        <th>Volante</th>
        <th>Area</th>
        <th>Turnado a</th>
        <th>Fecha de turno</th>
        <th>Estatus</th>
        <th>Indicación</th>
        <th>Observaciones</th>
        <th>Documento contestación</th>
        <th>Observaciones</th>
    </tr>
    <tbody>

        @foreach ($documents as $document)

        <tr>
            <td>{{ ($loop->index + 1) }}</td>
            <td>{{ \Carbon\Carbon::parse($document->conclusion_date)->isoFormat("DD MMM Y")}}</td>
            <td>{{ $document->volante }}</td>
            <td>{{ $document->area }}</td>
            <td>{{ $document->turn }}</td>
            <td>{{ $document->turn_date }}</td>
            <td>{{ __('CONCLUIDO') }}</td>
            <td>{{ $document->name }}</td>
            <td>{{ $document->observations }}</td>
            <td>{{ $document->answer_document }}</td>
            <td>{{ $document->observation }}</td>
        </tr>

        @endforeach


    </tbody>
</table>