<x-card-picture>


    <header class="flex items-center justify-between flex-wrap py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
        <h5 class="text-center lg:text-start text-sm lg:text-lg font-semibold text-slate-600 w-full lg:w-40 mb-4 lg:mb-0">Estadísticas</h5>
        <div class="flex items-center justify-between lg:justify-end space-x-2 w-full lg:w-auto">
            @can('select.area.for.graphics')
            <div class="w-full lg:w-80">
                <x-select :clearable='false' placeholder="Area..." option-label="name" option-value="id" :options="$areas" wire:model.defer="area_id" />
            </div>
            @endcan

            <div class="w-32">
                <x-select :clearable='false' placeholder="Año..." :options="$years" wire:model.defer="year" />
            </div>

            <div x-data="{ tooltip: 'Actualizar estadísticas' }" class="">
                <button id="refresh_purchases" wire:click="reloadCharts" x-tooltip="tooltip" type="button" class="w-full xl:w-12 border flex space-x-3 justify-center items-center bg-slate-50 hover:bg-slate-100 text-slate-600 focus:ring-2 focus:outline-none focus:ring-steal-50 font-semibold rounded text-sm lg:text-base px-3 py-3 ">
                    <i class="fa-solid fa-arrows-rotate"></i>
                </button>
            </div>
        </div>
    </header>

    <div class="p-3 md:p-5 lg:p-6">
        <input type="hidden" value="{{$area_id}}" id="area_selected">
        <input type="hidden" value="{{$year}}" id="year_selected">

        <section class="h-full flex flex-col">

            <article class="grow w-full flex items-center" wire:ignore>
                <div class="container__chart relative w-full p-3">

                    <div class="container__chart__documents box-border m-auto w-full px-1">
                        <div class="flex justify-center items-center w-full box-border overflow-auto">
                            <canvas id="chart__documents"></canvas>
                        </div>
                    </div>

                    <div class="container__spinner container__spinner__documents absolute w-full h-full top-0 hidden justify-center items-center flex-col">
                        <div class="spinner__chart"></div>
                        <p class="text-sm text-red-900">Cargando información...</p>
                    </div>
                </div>
            </article>
        </section>

        <x-section-border />

        <section class="h-full flex flex-col">

            <article class="grow w-full flex items-center" wire:ignore>
                <div class="container__chart relative w-full p-3">

                    <div class="container__chart__pending box-border m-auto w-full px-1">
                        <div class="flex justify-center items-center w-full box-border overflow-auto">
                            <canvas id="chart__pending"></canvas>
                        </div>
                    </div>

                    <div class="container__spinner container__spinner__pending absolute w-full h-full top-0 hidden justify-center items-center flex-col">
                        <div class="spinner__chart"></div>
                        <p class="text-sm text-red-900">Cargando información...</p>
                    </div>
                </div>
            </article>
        </section>

        <x-section-border />

        <section class="h-full flex flex-col">

            <article class="grow w-full flex items-center" wire:ignore>
                <div class="container__chart relative w-full p-3">

                    <div class="container__chart__completed box-border m-auto w-full px-1">
                        <div class="flex justify-center items-center w-full box-border overflow-auto">
                            <canvas id="chart__completed"></canvas>
                        </div>
                    </div>

                    <div class="container__spinner container__spinner__completed absolute w-full h-full top-0 hidden justify-center items-center flex-col">
                        <div class="spinner__chart"></div>
                        <p class="text-sm text-red-900">Cargando información...</p>
                    </div>
                </div>
            </article>
        </section>
    </div>

    @script
    <script>
        $wire.on('chargeChartDocument', ({
            status,
            area,
            year
        }) => {
            builChartDocuments(status, area, year);
        });

        $wire.on('chargeChartPending', ({
            status,
            area,
            year
        }) => {
            builChartPendings(status, area, year);
        });

        $wire.on('chargeChartCompleted', ({
            status,
            area,
            year
        }) => {
            builChartCompleted(status, area, year);
        });
    </script>
    @endscript
</x-card-picture>