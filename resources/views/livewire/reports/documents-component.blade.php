<div class="bg-white rounded-lg border border-slate-200 shadow-md">
    <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
        <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Gestión de documentos</h5>

        <div class="flex items-center space-x-2">
        </div>
    </header>

    <div class="p-3 md:p-5 lg:p-6">
        <form wire:submit.prevent="export" class="relative px-3">

            <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap flex" wire:loading.flex wire:target="search" wire:loading.class.remove="hidden">
                <p class="w-full justify-center font-semibold text-slate-700 text-xs md:text-sm flex">Generando archivo...</p>
                <span class="loader-download"></span>
            </div>

            <x-flash-message livewire="true" />

            <div class="grid xl:grid-cols-2 gap-y-3 gap-x-3 md:gap-y-4 md:gap-x-6">
                <x-datetime-picker 
                    errorless 
                    label="Desde:" 
                    placeholder="Desde..." 
                    without-time
                    parse-format="YYYY-MM-DD"
                    display-format="DD/MM/YYYY" 
                    :max="now()"
                    wire:model.live="start_date" />

                <x-datetime-picker 
                    errorless 
                    label="Hasta:" 
                    placeholder="Hasta..." 
                    without-time
                    parse-format="YYYY-MM-DD"
                    display-format="DD/MM/YYYY" 
                    :max="now()"
                    :min="$start_date"
                    wire:model.live="end_date" />
                @can('export.documents.by.area')
                <x-select errorless label="Area:" placeholder="Area a exportar..." :options="$areas" option-label="name" option-value="id" wire:model.defer="area" />
                @endcan
            </div>



            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">
                <div class="flex space-x-4">


                    <x-primary-button type="button" wire:click="export" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="export">
                        <i class="fa-solid fa-download"></i>
                        <span>Exportar</span>
                    </x-primary-button>

                </div>
            </div>
        </form>
    </div>
</div>
