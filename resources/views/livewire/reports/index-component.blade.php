<div>
    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Reportes" current="true" />
    </x-navigation>

    <div class="grid gap-y-5 gap-x-3 md:gap-y-8 md:gap-x-6">
        <!-- Reporte de Documentos -->

        @livewire('reports.documents-component')

        <!-- Turnos Pendientes -->
        @livewire('reports.pending-shifts-component')

        <!-- Turnos Completados -->
        @livewire('reports.completed-shifts-component')
    </div>
</div>
