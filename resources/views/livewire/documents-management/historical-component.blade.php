<div>
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Gestión de Documentos" :href="route('documents.index')" />
        <x-navigation-item label="Histórico" current="true" />
    </x-navigation>

    <x-card-picture>


        <header class="flex items-center justify-between flex-wrap py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center lg:text-start text-sm lg:text-lg font-semibold text-slate-600 w-full mb-4 lg:mb-0">Histórico del documento</h5>

        </header>

        <div class="p-6">


            <x-timeline>
                @foreach ($record as $row)

                <x-timeline-item :date="\Carbon\Carbon::parse($row['movement_date'])->isoFormat('DD MMM Y - hh:mm A')" :activity="$row['activity']" :description="$row['description']" />
                @endforeach

                @foreach ($document->shifts as $shift)

                @php
                $shifts_records = json_decode($shift->history, true);
                @endphp

                <div class="pl-5">
                    <hr><br>
                    <x-timeline>
                        @foreach ($shifts_records as $row)
                        <x-timeline-item :date="\Carbon\Carbon::parse($row['movement_date'])->isoFormat('DD MMM Y - hh:mm A')" :activity="$row['activity']" :description="$row['description']" />
                        @endforeach

                        @if ($shift->copy)

                        <div class="pl-5">
                            <x-timeline>
                                @foreach ($shift->copy as $copy)

                                @php
                                $copy_records = json_decode($copy->history, true);
                                @endphp

                                @foreach ($copy_records as $copy_record)
                                <x-timeline-item :date="\Carbon\Carbon::parse($copy_record['movement_date'])->isoFormat('DD MMM Y - hh:mm A')" :activity="$copy_record['activity']" :description="$copy_record['description']" />
                                @endforeach

                                @endforeach
                            </x-timeline>

                        </div>

                        @endif


                        @if ($shift->conclusion)
                        @php
                        $conclusion_records = json_decode($shift->conclusion->history, true);
                        @endphp

                        @foreach ($conclusion_records as $row)
                        <x-timeline-item :date="$row['shift_completed_date']" :activity="$row['activity']" :description="$row['description']" />
                        @endforeach
                        @endif

                    </x-timeline>


                </div>


                @endforeach

            </x-timeline>

            <!-- Botones  -->
            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">
                <div class="flex space-x-4">

                    <x-secondary-button x-on:click="close" type="button" class="button-after w-24 lg:w-28">
                        <i class="fa-solid fa-circle-arrow-left"></i>
                        <span>Regresar</span>
                    </x-secondary-button>

                </div>
            </div>
        </div>

    </x-card-picture>
</div>