<div>
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Gestión de Documentos" :href="route('documents.index')" />
        <x-navigation-item label="Editar documento" current="true" />
    </x-navigation>

    <div class="bg-white rounded-lg border border-slate-200 shadow-md">
        <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Editar documento</h5>

            <div class="flex items-center space-x-2">
            </div>
        </header>

        <div class="p-3 md:p-5 lg:p-6">
            <form wire:submit.prevent="update" class="relative px-3">
                <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="update" wire:loading.class.remove="hidden">
                    <span class="loader-download"></span>
                </div>

                <x-errors />

                <div class="grid xl:grid-cols-2 gap-y-3 gap-x-3 md:gap-y-4 md:gap-x-6">
                    <x-datetime-picker errorless label="Fecha de recepción" placeholder="Ingresa fecha de recepción" without-time="false" parse-format="YYYY-MM-DD" display-format="DD/MM/YYYY" wire:model="information.reception_date" />

                    <x-input errorless label="No. de Volante" placeholder="Ingresar no. de volante" wire:model="information.volante" />

                    <x-datetime-picker errorless label="Fecha del documento" placeholder="Ingresa fecha del documento" without-time="false" parse-format="YYYY-MM-DD" display-format="DD/MM/YYYY" wire:model="information.document_date" />

                    <x-input errorless label="Unidad administrativa" placeholder="Ingresar unidad administrativa" wire:model="information.admin_unit" />

                    <x-input errorless label="Referencia" placeholder="Ingresar referencia" wire:model="information.reference" />

                    <x-input errorless label="Remitente" placeholder="Ingresar remitente" wire:model="information.sender" />

                    <x-input errorless label="Cargo administrativo remitente" placeholder="Ingresar cargo administrativo remitente" wire:model="information.admin_position" />

                    <x-input errorless label="Destinatario" placeholder="Ingresar destinatario" wire:model="information.addressee" />

                    <x-input errorless label="Cargo administrativo destinatario" placeholder="Ingresar cargo administrativo destinatario" wire:model="information.addres_position" />

                    <x-select errorless label="Prioridad del documento" placeholder="Prioridad del documento..." :options="$priority" option-label="name" option-value="id" wire:model.defer="information.id_priority" />

                    <x-select errorless label="Tipo de documento" placeholder="Ingresa tipo de documento..." :options="$type_document" option-label="name" option-value="id" wire:model.defer="information.id_doctypes" />

                    <x-select errorless label="Anexo" placeholder="Ingresa anexo..." :options="$annex" option-label="name" option-value="id" wire:model.defer="information.id_annexes" />

                    <x-number errorless label="Cantidad de anexos" placeholder="0" wire:model.defer="information.quantity" />

                    <div>
                        <label class="block mb-0.5 text-sm font-medium disabled:opacity-60 text-gray-700 dark:text-gray-400 invalidated:text-negative-600 dark:invalidated:text-negative-700">
                            Documento
                        </label>
                        <x-input-file id="{{ $rand }}" label="Importar" accept=".pdf" type=" file" placeholder="Escoja el archivo de importación" wire:model="edit_file" />
                    </div>

                    <div class="xl:col-span-2">
                        <x-textarea errorless label="Asunto" placeholder="Ingresar asunto..." wire:model.defer="information.subject" />
                    </div>

                </div>



                <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">
                    <div class="flex space-x-4">

                        <x-secondary-button x-on:click="close" type="button" class="button-after w-24 lg:w-28">
                            <i class="fa-solid fa-circle-arrow-left"></i>
                            <span>Regresar</span>
                        </x-secondary-button>

                        <x-primary-button type="button" wire:click="update" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="update">
                            <i class="fa-regular fa-floppy-disk"></i>
                            <span>Guardar</span>
                        </x-primary-button>

                    </div>
                </div>
            </form>
        </div>
    </div>

</div>