<div>
    @push('css')
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.tailwindcss.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Gestión de Documentos" current="true" />
    </x-navigation>

    <div class="bg-white rounded-lg border border-slate-200 shadow-md">
        <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Listado de documentos</h5>

            <div class="flex items-center space-x-2">
                @can('documents.create')
                <div x-data="{ tooltip: 'Agregar un documento' }">
                    <a id="button__create--users" x-tooltip="tooltip" href="{{route('documents.new')}}" type="button" class="flex space-x-2 items-center bg-teal-800 hover:bg-teal-700 text-white focus:ring-2 focus:outline-none focus:ring-teal-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5">
                        <i class="fa-solid fa-plus"></i>
                        <span>Nuevo</span>
                    </a>
                </div>
                @endcan

                @can('documents.import')
                @livewire('documents-management.import-component')
                @endcan
            </div>
        </header>

        <div class="p-3 md:p-5 lg:p-6">
            <x-flash-message livewire="true" />

            <x-search />

            <div wire:ignore class="container__table relative">
                <table id='table__documents' class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-slate-500">#</th>
                            <th class="text-slate-500">Fecha de registro</th>
                            <th class="text-slate-500">Fecha de recepción</th>
                            <th class="text-slate-500">No. de volante</th>
                            <th class="text-slate-500">Fecha del documento</th>
                            <th class="text-slate-500">Unidad administrativa</th>
                            <th class="text-slate-500">Referencia</th>
                            <th class="text-slate-500">Remitente</th>
                            <th class="text-slate-500">Cargo administrativo del remitente</th>
                            <th class="text-slate-500">Prioridad del documento</th>
                            <th class="text-slate-500">Asunto</th>
                            <th class="text-slate-500 w-36">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div>
        @livewire('documents-management.details-component')
    </div>


    @push('scripts')
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    <script src="{{asset('vendor/sweetalert2/sweetalert2.all.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/dataTables.tailwindcss.js')}}" type="text/javascript"></script>

    <script src="{{asset('js/jquery-documents.js')}}" type="text/javascript"></script>
    @endpush
</div>