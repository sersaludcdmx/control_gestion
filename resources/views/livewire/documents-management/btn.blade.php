<div class="grid grid-cols-3 gap-y-3 gap-x-3 justify-items-center">

    @if ($history)
    <div x-data="{ tooltip: 'Ver histórico del documento' }">
        <a x-tooltip="tooltip" href="{{ route('documents.historical', $id) }}" type="button" class="button text-violet-700 border bg-violet-100 hover:bg-violet-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-violet-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-clock"></i>
        </a>
    </div>
    @endif

    @can('documents.rotate')
    <div x-data="{ tooltip: 'Turnar documento' }">
        <a x-tooltip="tooltip" href="{{route('shifts.new', $id)}}" type=" button" class="button btn-option-edit text-blue-700 border bg-blue-100 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fas fa-file-signature"></i>
        </a>
    </div>
    @endcan

    @can('documents.details')
    <div x-data="{ tooltip: 'Registro del documento' }">
        <button x-tooltip="tooltip" wire:click="$dispatchTo('documents-management.details-component', 'open-modal-details', { document: '{{$id}}' } )" type=" button" class="button text-fuchsia-700 border bg-fuchsia-100 hover:bg-fuchsia-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-fuchsia-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-circle-info"></i>
        </button>
    </div>
    @endcan

    @can('documents.make.pdf')
    <div x-data="{ tooltip: 'Generar reporte del documento' }">
        <a x-tooltip="tooltip" href="{{ route('document.print', $id) }}" target="_blank" type=" button" class="button text-amber-700 border bg-amber-100 hover:bg-amber-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-amber-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-file-pdf"></i>
        </a>
    </div>
    @endcan

    @can('documents.download')
    <div x-data="{ tooltip: 'Descargar documento' }">
        <button x-tooltip="tooltip" wire:click="$dispatchTo('documents-management.index-component', 'download-file', { document: '{{$id}}' } )" type="button" class="button text-emerald-700 border bg-emerald-100 hover:bg-emerald-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-emerald-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-cloud-arrow-down"></i>
        </button>
    </div>
    @endcan

    @can('documents.edit')
    <div x-data="{ tooltip: 'Editar documento' }">
        <a x-tooltip="tooltip" href="{{route('documents.edit', $id)}}" type=" button" class="button btn-option-edit text-blue-700 border bg-blue-100 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-pen-to-square"></i>
        </a>
    </div>
    @endcan

    @can('documents.destroy')
    <div x-data="{ tooltip: 'Borrar documento' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-inactive text-red-700 border bg-red-100 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-trash-can"></i>
        </button>
    </div>
    @endcan

</div>