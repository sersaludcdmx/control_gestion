<div>
    <!-- Modal para CRUD de usuarios -->
    <x-modal-card :title="$title" align="start" wire:model.defer="cardModal">
        <form wire:submit.prevent="store,update,passwordChange" class="relative px-3">
            @if ($current_document)
            <div class="w-full">

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Fecha de recepción:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ \Carbon\Carbon::parse($current_document->reception_date)->isoFormat("DD MMM Y") }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Número de volante:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->volante }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Fecha del documento:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ \Carbon\Carbon::parse($current_document->document_date)->isoFormat("DD MMM Y") }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Unidad administrativa:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document-> admin_unit}}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Referencia:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->reference }}</p>
                </div>
                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Remitente:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->sender }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Cargo administrativo remitente:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->admin_position }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Destinatario:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->addressee }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Cargo administrativo destinatario:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->addres_position }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Prioridad del documento:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->priority->name }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Tipo de documento:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->type->name }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Anexo:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->annex->name }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Cantidad de anexos:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->quantity }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Asunto:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_document->subject }}</p>
                </div>

            </div>
            @endif

            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">

                <div class="flex space-x-4">
                    <x-secondary-button x-on:click="close" type="button" class="w-24 lg:w-28">
                        <i class="fa-solid fa-ban"></i>
                        <span>Cerrar</span>
                    </x-secondary-button>
                </div>

            </div>


        </form>
    </x-modal-card>

</div>