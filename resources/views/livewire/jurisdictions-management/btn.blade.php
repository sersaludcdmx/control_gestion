<div class="flex items-center justify-around">

    @can('jurisdictions.update')
    <div x-data="{ tooltip: 'Editar jurisdicción' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" wire:click="edit('{{ $id }}')" type=" button" class="button btn-option-edit text-blue-700 border bg-blue-100 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-pen-to-square"></i>
        </button>
    </div>
    @endcan

    @can('jurisdictions.inactive')
        @if ($is_active != 0)
        <div x-data="{ tooltip: 'Desactivar jurisdicción' }">
            <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-inactive text-red-700 border bg-red-100 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
                <i class="fa-solid fa-power-off"></i>
            </button>
        </div>
        @endif
    @endcan

    @can('jurisdictions.active')
        @if ($is_active == 0)
        <div x-data="{ tooltip: 'Reactivar jurisdicción' }">
            <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-active text-emerald-700 border bg-emerald-100 hover:bg-emerald-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
                <i class="fa-solid fa-rotate-right"></i>
            </button>
        </div>
        @endif
    @endcan
</div>