<x-app-layout :section="$section">

    <x-flash-message />

    <div class="p-3 md:p-5 lg:p-6 bg-white rounded-lg border border-gray-200 shadow-md">
        <h5 class="text-center mb-2 text-base lg:text-xl font-semibold text-slate-700 flex items-center">

            Asignación de permisos del rol: <span class="italic font-bold">{{ $role->name}}</span>
        </h5>

        <form class="mt-4" method="POST" enctype="multipart/form-data" action="{{ route('rolespermisos.update', $role) }}">
            {{ csrf_field() }}
            @method('PUT')

            <div class="grid lg:grid-cols-2 xl:grid-cols-3 gap-y-3 gap-x-3 md:gap-y-3 md:gap-x-6">
                @if (!is_null($permissions) )
                @foreach ($permissions as $permission)

                <div class="group">
                    <label for="{{$permission->id}}" class="relative inline-flex items-center mr-5 cursor-pointer">
                        <input id="{{$permission->id}}" type="checkbox" value="{{$permission->name}}" class="sr-only peer" name="permissions[]" @if(in_array( $permission->name, $rolesPermissions) ) checked @endif>
                        <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-focus:ring-4 peer-focus:ring-green-300 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-green-600"></div>
                        <span class="ml-3 text-xs sm:text-sm font-medium text-slate-600">{{$permission->module.': '.$permission->description }}</span>
                    </label>
                </div>


                @endforeach
                @endif
            </div>

            <div class="flex justify-center items-center mt-6">
                <div class="flex space-x-4">

                    <x-secondary-button link="true" href="{{route('roles.index')}}" class="w-24 lg:w-28">
                        <i class="fa-solid fa-circle-arrow-left"></i>
                        <span>Regresar</span>
                    </x-secondary-button>

                    <x-primary-button type="submit" class="w-24 lg:w-28">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Guardar</span>
                    </x-primary-button>

                </div>
        </form>

    </div>
</x-app-layout>