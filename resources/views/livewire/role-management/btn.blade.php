<div class="flex items-center justify-around">
    @can('roles.assignment.permissions')
    <div x-data="{ tooltip: 'Asignar permisos al rol' }">
        <a x-tooltip="tooltip" href="{{ route('rolespermisos.edit', $id) }}" type=" button" class="button btn-option-permissions text-orange-400 border bg-orange-100 hover:bg-orange-400 hover:text-white focus:ring-2 focus:outline-none focus:ring-orange-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-handcuffs"></i>
        </a>
    </div>
    @endcan

    @can('roles.update')
    <div x-data="{ tooltip: 'Editar rol' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" wire:click="edit('{{ $id }}')" type=" button" class="button btn-option-edit text-blue-700 border bg-blue-100 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-pen-to-square"></i>
        </button>
    </div>
    @endcan

    @can('roles.inactive')
    @if ($is_active != 0)
    <div x-data="{ tooltip: 'Desactivar rol' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-inactive text-red-700 border bg-red-100 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-power-off"></i>
        </button>
    </div>
    @endif
    @endcan

    @can('roles.active')
    @if ($is_active == 0)
    <div x-data="{ tooltip: 'Reactivar rol' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-active text-teal-700 border bg-teal-100 hover:bg-teal-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-rotate-right"></i>
        </button>
    </div>
    @endif
    @endcan
</div>