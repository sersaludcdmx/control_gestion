<div>
    @push('css')
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.tailwindcss.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Gestión de usuarios" />
        <x-navigation-item label="Permisos" current="true" />
    </x-navigation>

    <div class="bg-white rounded-lg border border-slate-200 shadow-md">
        <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Lista de permisos</h5>

            @can('permissions.create')
            <div x-data="{ tooltip: 'Agregar un permiso' }">
                <button id="button__create--permission" x-tooltip="tooltip" wire:click="create" type="button" class="flex space-x-2 items-center bg-teal-800 hover:bg-teal-700 text-white focus:ring-2 focus:outline-none focus:ring-teal-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5">
                    <i class="fa-solid fa-plus"></i>
                    <span>Nuevo</span>
                </button>
            </div>
            @endcan
        </header>

        <div class="p-3 md:p-5 lg:p-6">
            <x-search />

            <div wire:ignore class="container__table relative">
                <table id="table__permissions" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-slate-500 w-24">#</th>
                            <th class="text-slate-500">Fecha de registro</th>
                            <th class="text-slate-500">Modulo</th>
                            <th class="text-slate-500">Permiso</th>
                            <th class="text-slate-500">descripción</th>
                            <th class="text-slate-500">Tipo</th>
                            <th class="text-slate-500">Estado</th>
                            <th class="text-slate-500 w-24">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <x-modal-card :title="$title" align="start" wire:model.defer="cardModal" persistent="true">
        <form wire:submit.prevent="save,updated" class="relative px-3">
            <div class="container-loader h-full w-full absolute items-center justify-center z-50 flex-wrap hidden" wire:loading.flex wire:target="save,update" wire:loading.class.remove="hidden">
                <span class="loader-download"></span>
            </div>

            <x-flash-message livewire="true" />


            <div class="grid gap-y-3 gap-x-3 md:gap-y-4 md:gap-x-6">

                <x-input label="Permiso" placeholder="Ingresar el nombre del permiso" wire:model.defer="information.name" />

                <x-input label="Modulo" placeholder="Modulo padre..." wire:model.defer="information.module" />

                <x-textarea wire:model.defer="information.description" label="Pequeña descripción:" placeholder="Pequeña descripción..." />

            </div>

            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">

                <div class="flex space-x-4">
                    @if ($action == 'store')
                    <x-primary-button type="button" wire:click="store" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="store">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Guardar</span>
                    </x-primary-button>
                    @elseif ($action == 'updated')
                    <x-primary-button type="button" wire:click="update" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="update">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Actualizar</span>
                    </x-primary-button>
                    @endif
                    <x-secondary-button x-on:click="close" type="button" class="w-24 lg:w-28">
                        <i class="fa-solid fa-ban"></i>
                        <span>Cerrar</span>
                    </x-secondary-button>
                </div>

            </div>


        </form>
    </x-modal-card>

    @push('scripts')
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    <script src="{{asset('vendor/sweetalert2/sweetalert2.all.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/dataTables.tailwindcss.js')}}" type="text/javascript"></script>

    <script src="{{asset('js/jquery-permissions.js')}}" type="text/javascript"></script>
    @endpush
</div>