<div class="flex items-center justify-around">
    @can('permissions.update')
    <div x-data="{ tooltip: 'Editar permiso' }">
        <button x-tooltip="tooltip" wire:click="edit('{{ $id }}')" type=" button" class="button btn-option-edit border text-blue-700 bg-blue-100 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-pen-to-square"></i>
        </button>
    </div>
    @endcan

    @can('permissions.inactive')
        @if ($is_active != 0)
        <div x-data="{ tooltip: 'Desactivar permiso' }">
            <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-inactive border text-red-700 bg-red-100 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
                <i class="fa-solid fa-power-off"></i>
            </button>
        </div>
        @endif
    @endcan


    @can('permissions.active')
        @if ($is_active == 0)
        <div x-data="{ tooltip: 'Reactivar permiso' }">
            <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-active border text-teal-700 bg-teal-100 hover:bg-teal-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
                <i class="fa-solid fa-rotate-right"></i>
            </button>
        </div>
        @endif
    @endcan

</div>