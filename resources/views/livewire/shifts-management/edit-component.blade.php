<div>
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Turnos pendientes" :href="route('shifts.index')" />
        <x-navigation-item label="Editar turno" current="true" />
    </x-navigation>

    <div class="bg-white rounded-lg border border-slate-200 shadow-md">
        <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Editar turno</h5>

            <div class="flex items-center space-x-2">
            </div>
        </header>

        <div class="p-3 md:p-5 lg:p-6">
            <form wire:submit.prevent="update" class="relative px-3">
                <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="update" wire:loading.class.remove="hidden">
                    <span class="loader-download"></span>
                </div>

                <x-errors />

                <div class="grid xl:grid-cols-2 gap-y-3 gap-x-3 md:gap-y-4 md:gap-x-6">

                    <x-input errorless label="No. de volante" placeholder="Ingresar no. de volante" wire:model="information.volante" readonly />

                    <x-input errorless label="Area de turnado" placeholder="Ingresar area de turnado" wire:model="information.area" />

                    <x-input errorless label="Turnado a" placeholder="Ingresar turnado" wire:model="information.turn" />

                    <x-datetime-picker errorless label="Fecha del turno" placeholder="Ingresa fecha del turno" requires-confirmation parse-format="YYYY-MM-DD HH:mm:ss" display-format="DD/MM/YYYY hh:mm A" wire:model="information.turn_date" />

                    <x-select errorless label="Prioridad del documento" placeholder="Prioridad del documento..." :options="$priority" option-label="name" option-value="id"
                        wire:model.defer="information.id_priority" />

                    <x-select errorless label="Indicación" placeholder="Indicación del turno..." :options="$indication" option-label="name" option-value="id"
                        wire:model.defer="information.id_indications" />

                    <div class="xl:col-span-2">
                        <x-textarea errorless label="Observaciones" placeholder="Ingresar observaciones..." wire:model.defer="information.observations" />
                    </div>

                </div>

                <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">
                    <div class="flex space-x-4">

                        <x-secondary-button x-on:click="close" type="button" class="button-after w-24 lg:w-28">
                            <i class="fa-solid fa-circle-arrow-left"></i>
                            <span>Regresar</span>
                        </x-secondary-button>

                        <x-primary-button type="button" wire:click="update" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="update">
                            <i class="fa-regular fa-floppy-disk"></i>
                            <span>Guardar</span>
                        </x-primary-button>

                    </div>
                </div>
            </form>
        </div>
    </div>

</div>