<div class="grid grid-cols-4 gap-y-3 gap-x-3 justify-items-center">

    @can('pending.shifts.copy')
    <div x-data="{ tooltip: 'Agregar Cccp' }">
        <a x-tooltip="tooltip" href="{{route('copies.new', $id)}}" type=" button" class="button btn-option-edit text-gray-700 border bg-gray-100 hover:bg-gray-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-copy"></i>
        </a>
    </div>
    @endcan

    @can('pending.shifts.conclude')
    <div x-data="{ tooltip: 'Concluir turno' }">
        <a x-tooltip="tooltip" href="{{route('completed.new', $id)}}" type=" button" class="button btn-option-edit text-cyan-700 border bg-cyan-100 hover:bg-cyan-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-cyan-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-flag"></i>
        </a>
    </div>
    @endcan

    @can('pending.shifts.details')
    <div x-data="{ tooltip: 'Registro del turno' }">
        <button x-tooltip="tooltip" wire:click="$dispatchTo('shifts-management.details-component', 'open-modal-details', { shift: '{{$id}}' } )" type=" button" class="button text-fuchsia-700 border bg-fuchsia-100 hover:bg-fuchsia-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-fuchsia-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-circle-info"></i>
        </button>
    </div>
    @endcan

    @can('pending.shifts.download')
    <div x-data="{ tooltip: 'Descargar documento' }">
        <button x-tooltip="tooltip" wire:click="$dispatchTo('shifts-management.index-component', 'download-file', { shift: '{{$id}}' } )" type="button" class="button text-emerald-700 border bg-emerald-100 hover:bg-emerald-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-emerald-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-cloud-arrow-down"></i>
        </button>
    </div>
    @endcan

    @can('pending.shifts.edit')
    <div x-data="{ tooltip: 'Editar turno' }">
        <a x-tooltip="tooltip" href="{{route('shifts.edit', $id)}}" type=" button" class="button btn-option-edit text-blue-700 border bg-blue-100 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-pen-to-square"></i>
        </a>
    </div>
    @endcan

    @can('pending.shifts.destroy')
    <div x-data="{ tooltip: 'Borrar turno' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-inactive text-red-700 border bg-red-100 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-trash-can"></i>
        </button>
    </div>
    @endcan

    @can('pending.shifts.make.pdf')
    <div x-data="{ tooltip: 'Generar reporte del turnado' }">
        <a x-tooltip="tooltip" href="{{ route('pending.print', $id) }}" target="_blank" type=" button" class="button text-amber-700 border bg-amber-100 hover:bg-amber-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-amber-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-file-pdf"></i>
        </a>
    </div>
    @endcan

</div>