<div class="">
    <button wire:click="openImport" type="button" class="flex space-x-2 items-center bg-red-800 hover:bg-red-700 text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5">
        <i class="fa-solid fa-file-arrow-up"></i>
        <span>Importar</span>
    </button>

    <!-- Log Out Other Devices Confirmation Modal -->
    <x-modal-card title="Importar turnos" align="start" wire:model.defer="modalImport">
        <form wire:submit.prevent="save" class="relative">

            <x-errors />
            <br>


            <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="save,shifts" wire:loading.class.remove="hidden">
                <p class="w-full justify-center font-semibold text-slate-700 text-xs md:text-sm hidden" wire:loading.flex wire:target="save" wire:loading.class.remove="hidden">Importando...</p>
                <span class="loader-download"></span>
            </div>

            <div class="group mb-4">
                <x-input-file id="{{ $rand }}" label="Importar" accept=".csv" type=" file" placeholder="Escoja el archivo de importación" wire:model="shifts" />
            </div>


            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">

                <div class="flex space-x-4">

                    <x-primary-button type="button" wire:click="save" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="save">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Importar</span>
                    </x-primary-button>

                    <x-secondary-button x-on:click="close" type="button" class="w-24 lg:w-28">
                        <i class="fa-solid fa-ban"></i>
                        <span>Cerrar</span>
                    </x-secondary-button>
                </div>

            </div>


        </form>
    </x-modal-card>

</div>