<div>
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Gestión de Documentos" :href="route('documents.index')" />
        <x-navigation-item label="Registro de Turno" current="true" />
    </x-navigation>

    <x-card title="Registro de Turno">

        <form wire:submit.prevent="save" class="relative px-3">
            <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="save" wire:loading.class.remove="hidden">
                <span class="loader-download"></span>
            </div>

            <div class="grid xl:grid-cols-2 gap-y-3 gap-x-3 md:gap-y-4 md:gap-x-6">

                <x-input label="No. de volante" placeholder="Ingresar no. de volante" wire:model="information.volante" readonly />

                <x-input label="Area de turnado" placeholder="Ingresar area de turnado" wire:model="information.area" />

                <x-input label="Turnado a" placeholder="Ingresar turnado" wire:model="information.turn" />

                <x-datetime-picker label="Fecha del turno" placeholder="Ingresa fecha del turno" requires-confirmation parse-format="YYYY-MM-DD HH:mm:ss" display-format="DD/MM/YYYY hh:mm A" wire:model="information.turn_date" />

                <x-select label="Prioridad del documento" placeholder="Prioridad del documento..." :options="$priority" option-label="name" option-value="id"
                    wire:model.defer="information.id_priority" />

                <x-select label="Indicación" placeholder="Indicación del turno..." :options="$indication" option-label="name" option-value="id"
                    wire:model.defer="information.id_indications" />

                <div class="xl:col-span-2">
                    <x-textarea label="Observaciones" placeholder="Ingresar observaciones..." wire:model.defer="information.observations" />
                </div>
            </div>

            <!-- Botones  -->
            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">
                <div class="flex space-x-4">

                    <x-secondary-button x-on:click="close" type="button" class="button-after w-24 lg:w-28">
                        <i class="fa-solid fa-circle-arrow-left"></i>
                        <span>Regresar</span>
                    </x-secondary-button>

                    <x-primary-button type="button" wire:click="save" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="save">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Guardar</span>
                    </x-primary-button>

                </div>
            </div>
        </form>

    </x-card>
</div>