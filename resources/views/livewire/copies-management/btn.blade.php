<div class="flex justify-center items-center">
    @can('ccp.destroy')
    <div x-data="{ tooltip: 'Borrar copia' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" type="button" class="button btn-option-inactive text-red-700 border bg-red-100 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-trash-can"></i>
        </button>
    </div>
    @endcan
</div>