<div class="flex justify-center items-center">

    <div x-data="{ tooltip: 'Registro del turno' }">
        <button x-tooltip="tooltip" wire:click="$dispatchTo('completed-shifts-management.details-component', 'open-modal-details', { shift: '{{$id_shifts}}' } )" type=" button" class="button text-fuchsia-700 border bg-fuchsia-100 hover:bg-fuchsia-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-fuchsia-300 font-medium rounded text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-solid fa-circle-info"></i>
        </button>
    </div>

</div>