<div>
    @push('css')
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.tailwindcss.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Turnos concluidos" current="true" />
    </x-navigation>

    <div class="bg-white rounded-lg border border-slate-200 shadow-md">
        <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Listado de turnos concluidos</h5>
        </header>

        <div class="p-3 md:p-5 lg:p-6">
            <x-flash-message livewire="true" />

            <x-search />

            <div wire:ignore class="container__table relative">
                <table id='table__conclusions' class="display nowrap">
                    <thead>
                        <tr>
                            <th class="text-slate-500">#</th>
                            <th class="text-slate-500">No. de volante turnado</th>
                            <th class="text-slate-500">Fecha de conclusión</th>
                            <th class="text-slate-500">Documento contestación</th>
                            <th class="text-slate-500">Observaciones</th>
                            <th class="text-slate-500 w-36">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div>
        @livewire('completed-shifts-management.details-component')
    </div>


    @push('scripts')
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    <script src="{{asset('vendor/sweetalert2/sweetalert2.all.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/dataTables.tailwindcss.js')}}" type="text/javascript"></script>

    <script src="{{asset('js/jquery-conclusions.js')}}" type="text/javascript"></script>
    @endpush
</div>