<div>
    <!-- Modal para CRUD de usuarios -->
    <x-modal-card :title="$title" align="start" wire:model.defer="cardModal">
        <form wire:submit.prevent="" class="relative px-3">
            @if ($current_shift)
            <div class="w-full">


                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Número de volante:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->document->volante }}</p>
                </div>

                <p class="w-full text-center text-base md:text-lg font-bold text-slate-500 border-t border-b">Turnado</p>
                <br>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Área:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->area }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Turnado a:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->turn }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Fecha de turno:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ \Carbon\Carbon::parse($current_shift->turn_date)->isoFormat("DD MMM Y") }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Estatus:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->status == 'PEN' ? 'PENDIENTE' : 'CONCLUIDO' }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Indicación:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->indication->name }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Prioridad:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->priority->name }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Observaciones:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->observations }}</p>
                </div>

                <p class="w-full text-center text-base md:text-lg font-bold text-slate-500 border-t border-b">Turnado concluido</p>
                <br>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Fecha de conclusión:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ \Carbon\Carbon::parse($current_shift->conclusion->conclusion_date)->isoFormat("DD MMM Y") }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Documento contestación:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->conclusion->answer_document }}</p>
                </div>

                <div class="w-full flex items-start justify-center mb-2 pb-3">
                    <p class="w-2/4 text-end text-sm text-slate-500">Observaciones:</p>
                    <p class="w-2/4 text-start text-sm md:text-base font-bold text-slate-500 ml-2">{{ $current_shift->conclusion->observation }}</p>
                </div>

            </div>
            @endif

            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">

                <div class="flex space-x-4">
                    <x-secondary-button x-on:click="close" type="button" class="w-24 lg:w-28">
                        <i class="fa-solid fa-ban"></i>
                        <span>Cerrar</span>
                    </x-secondary-button>
                </div>

            </div>


        </form>
    </x-modal-card>

</div>