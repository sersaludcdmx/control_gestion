<div>
    @push('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Turnos pendientes" :href="route('shifts.index')" />
        <x-navigation-item label="Concluir turno" current="true" />
    </x-navigation>


    <div class="bg-white rounded-lg border border-slate-200 shadow-md">
        <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center text-sm lg:text-lg text-slate-600"><span class="font-semibold">N° de Volante: </span>{{$volante}}</h5>
        </header>

        <div class="p-3 md:p-5 lg:p-6">
            <form wire:submit.prevent="save" class="relative px-3">
                <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="save" wire:loading.class.remove="hidden">
                    <span class="loader-download"></span>
                </div>

                <x-errors />

                <div class="grid xl:grid-cols-2 gap-y-3 gap-x-3 md:gap-y-4 md:gap-x-6 mt-2">

                    <x-datetime-picker label="Fecha de conclusión" placeholder="Ingresa fecha de conclusión" without-time requires-confirmation parse-format="YYYY-MM-DD" display-format="DD/MM/YYYY" wire:model="information.conclusion_date" />

                    <x-input label="Documento contestación" placeholder="Ingresar documento contestación" wire:model="information.answer_document" />

                    <div class="xl:col-span-2">
                        <x-textarea label="Observaciones" placeholder="Ingresar observaciones..." wire:model.defer="information.observation" />
                    </div>
                </div>

                <!-- Botones  -->
                <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">
                    <div class="flex space-x-4">

                        <x-secondary-button x-on:click="close" type="button" class="button-after w-24 lg:w-28">
                            <i class="fa-solid fa-circle-arrow-left"></i>
                            <span>Regresar</span>
                        </x-secondary-button>

                        <x-primary-button type="button" wire:click="save" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="save">
                            <i class="fa-regular fa-floppy-disk"></i>
                            <span>Guardar</span>
                        </x-primary-button>

                    </div>
                </div>
            </form>
        </div>

    </div>
</div>