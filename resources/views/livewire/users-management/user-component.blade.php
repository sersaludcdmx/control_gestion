<div>
    @push('css')
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.tailwindcss.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@6/dist/tippy.css" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    @endpush

    <x-navigation>
        <x-navigation-item label="Inicio" home="true" :href="url('/inicio')" />
        <x-navigation-item label="Gestión de usuarios" />
        <x-navigation-item label="Usuarios" current="true" />
    </x-navigation>

    <div class="bg-white rounded-lg border border-slate-200 shadow-md">
        <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
            <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Lista de usuarios</h5>

            <div class="flex items-center space-x-2">
                @can('users.create')
                <div x-data="{ tooltip: 'Agregar un usuario' }">
                    <button id="button__create--users" x-tooltip="tooltip" wire:click="$dispatchTo('users-management.new-or-update-component', 'open-add-modal' )" type="button" class="flex space-x-2 items-center bg-teal-800 hover:bg-teal-700 text-white focus:ring-2 focus:outline-none focus:ring-teal-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5">
                        <i class="fa-solid fa-plus"></i>
                        <span>Nuevo</span>
                    </button>
                </div>
                @endcan

            </div>
        </header>

        <div class="p-3 md:p-5 lg:p-6">
            <x-search />


            <div wire:ignore class="container__table relative">
                <table class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-slate-500">#</th>
                            <th class="text-slate-500">Tipo de usuario</th>
                            <th class="text-slate-500">Usuario</th>
                            <th class="text-slate-500">Nombre completo</th>
                            <th class="text-slate-500">Jurisdicción</th>
                            <th class="text-slate-500">Ubicación</th>
                            <th class="text-slate-500">Teléfono</th>
                            <th class="text-slate-500">Extension</th>
                            <th class="text-slate-500">Correo electrónico</th>
                            <th class="text-slate-500">Estado</th>
                            <th class="text-slate-500 w-36">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        @livewire('users-management.new-or-update-component')
    </div>

    @push('scripts')
    <link rel="stylesheet" href="{{asset('vendor/sweetalert2/sweetalert2.min.css')}}">
    <script src="{{asset('vendor/sweetalert2/sweetalert2.all.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/dataTables.tailwindcss.js')}}" type="text/javascript"></script>

    <script src="{{asset('js/jquery-users.js')}}" type="text/javascript"></script>
    @endpush
</div>