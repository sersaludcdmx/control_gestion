<div>
    <!-- Modal para CRUD de usuarios -->
    <x-modal-card :title="$title" align="start" wire:model.defer="cardModal" persistent="true">
        <form wire:submit.prevent="store,update,passwordChange" class="relative px-3">
            <div class="container-loader h-full w-full absolute items-center justify-center z-50 flex-wrap hidden" wire:loading.flex wire:target="store,update,passwordChange" wire:loading.class.remove="hidden">
                <span class="loader-download"></span>
            </div>

            <x-flash-message livewire="true" />

            <div class="grid xl:grid-cols-2 gap-y-3 gap-x-3 md:gap-y-4 md:gap-x-6">

                @if ($action != 'CrudPassword')
                <x-select label="Jurisdicción" placeholder="Jurisdicción..." :options="$jurisdictions" option-label="name" option-value="id" wire:model.defer="information.jurisdiction_id" />

                <x-select label="Area" placeholder="Area..." :options="$areas" option-label="name" option-value="id" wire:model.defer="information.area_id" />

                <x-input label="Nombre" placeholder="Nombre..." wire:model="information.name" />

                <x-input label="Apellido paterno" placeholder="Apellido paterno..." wire:model="information.paternal_surname" />

                <x-input label="Apellido materno" placeholder="Apellido materno..." wire:model="information.maternal_surname" />

                <x-input label="Nombre de usuario" placeholder="Ingresar username..." wire:model="information.username" />



                <x-select label="Rol" placeholder="Rol de usuario..." :options="$roles" option-label="name" option-value="name" wire:model.defer="role_name" />

                <x-input label="Correo electrónico" type="email" placeholder="Correo electrónico..." wire:model="information.email" />

                <x-input label="Teléfono" placeholder="Teléfono de contacto..." wire:model="information.phone" />

                <x-input label="Extensión" placeholder="Extension del usuario..." wire:model="information.extension" />
                @endif

                @if ($action == 'CrudStore' || $action == 'CrudPassword')
                <x-password label="Contraseña" placeholder="Ingresar la contraseña" wire:model="information.password" name="information.password" />

                <x-password label="Confirmar contraseña" placeholder="Confirmar Contraseña" wire:model="information.password_confirmation" name="information.password_confirmation" />
                @endif

            </div>


            <div class="mt-4 pt-4 px-4 sm:px-6 bg-white rounded-t-none flex justify-center gap-x-4">

                <div class="flex space-x-4">
                    @if ($action == 'CrudStore')
                    <x-primary-button type="button" wire:click="store" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="store">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Guardar</span>
                    </x-primary-button>
                    @elseif ($action == 'CrudUpdated')
                    <x-primary-button type="button" wire:click="update" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="update">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Actualizar</span>
                    </x-primary-button>
                    @elseif ($action == 'CrudPassword')
                    <x-primary-button type="button" wire:click="passwordChange" wire:loading.attr="disabled" class="w-24 lg:w-28" wire:target="passwordChange">
                        <i class="fa-regular fa-floppy-disk"></i>
                        <span>Actualizar</span>
                    </x-primary-button>
                    @endif
                    <x-secondary-button x-on:click="close" type="button" class="w-24 lg:w-28">
                        <i class="fa-solid fa-ban"></i>
                        <span>Cerrar</span>
                    </x-secondary-button>
                </div>

            </div>


        </form>
    </x-modal-card>

</div>