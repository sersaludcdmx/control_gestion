import './bootstrap';

window.backgroundChart = {
    id: 'custom_canvas_background_color',
    beforeDraw: (chart) => {
        const {
            ctx
        } = chart;
        ctx.save();
        ctx.globalCompositeOperation = 'destination-over';
        ctx.fillStyle = '#FFFFFF';
        ctx.fillRect(0, 0, chart.width, chart.height);
        ctx.restore();
    }
};

window.applyOrder = function (table, id) {

    var order = table.order();
    let text = null;

    if (order[0][1] == 'asc' || order[0][1] == 'ASC') {
        text = table.column(order[0][0], {
            order: 'applied'
        }).title() + ' de la A-Z';
    } else if (order[0][1] == 'desc' || order[0][1] == 'DESC') {
        text = table.column(order[0][0], {
            order: 'applied'
        }).title() + ' de la Z-A';
    }

    if (text != null) {
        $("#toolbar__table__" + id).html('<p class="text-xs text-slate-500">Ordenando por: <span id="ordering__apply' + id + '" class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-indigo-100 text-indigo-800 capitalize">' + text + '</span></p>');
    } else {
        $("#toolbar__table__" + id).html("");
    }

}

$(document).ready(function () {
    //Función para realizar búsqueda en el dataTable
    window.searchDataTable = function () {
        table.search($('#text-search').val()).draw();
    }
    //Acción del botón de limpiar el campo de búsqueda general del dataTable
    $("#search button").click(function (event) {
        event.preventDefault();
        $("#text-search").val('');
        searchDataTable();
    });

    $("body").on('blur', '.text-uppercase', function () {
        let end = $(this).prop('selectionEnd');
        $(this).val($(this).val().toUpperCase());
        $(this).prop('selectionEnd', end);
    });
    //Se convierte a mayúsculas la cadena al presionar una tecla
    $('body').on('keyup', '.text-uppercase', function () {
        let end = $(this).prop('selectionEnd');
        $(this).val($(this).val().toUpperCase());
        $(this).prop('selectionEnd', end);
    });
    //Detecta la acción de submit en el formulario de búsqueda de la tabla
    $('#search').submit(function (event) {
        event.preventDefault();
        searchDataTable()
    });
    //Invoca la búsqueda automática al ir escribiendo sobre el text de búsqueda en la tabla
    $('#text-search').keyup($.debounce(250, function (e) {
        searchDataTable();
    }));
    //Acción para los botones de regresar a la sección anterior
    $('.button-after').click(function () {
        window.history.back();
    });

    if (typeof Livewire != 'undefined') {
        Livewire.on('action-success', ({
            title,
            text
        }) => {
            Swal.fire({
                title: title,
                text: text,
                icon: 'success',
                confirmButtonColor: '#239B56',
                confirmButtonText: 'Aceptar',
            })
        });

        Livewire.on('action-oops', ({
            text
        }) => {
            Swal.fire({
                title: "Oops...",
                text: text,
                icon: "error",
                confirmButtonColor: '#9d2148',
                confirmButtonText: 'Aceptar',
            })
        });
    }
});