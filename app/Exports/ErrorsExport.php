<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ErrorsExport extends DefaultValueBinder implements WithStyles, FromView, ShouldAutoSize, WithTitle, WithCustomValueBinder
{
    private $info;
    public function __construct($info)
    {
        $this->info = $info;
    }
    public function view(): View
    {
        return view('backend.errors.import-errors', [
            'information' => $this->info
        ]);
    }

    /**
     * Función para generar los estilos que contendrá el archivo exportado
     */
    public function styles(Worksheet $sheet)
    {
        $col = count($this->info[0]) + 1;

        $sheet->getStyle('B2:D2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);

        $sheet->getStyle([1, 5, $col, 5])->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'F1C40F',
                ],
                'endColor' => [
                    'argb' => 'F1C40F'
                ]
            ]
        ]);

        $temp = count($this->info) +  5;

        $sheet->getStyle([1, 6, $col, $temp])->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
            ],
        ]);
    }

    /**
     * Función para asignar el titulo del documento
     */
    public function title(): string
    {
        return 'Errores de reportados';
    }

    /**
     * Función que fuerza a los campos enteros a guardarlos como string en el archivo
     */
    public function bindValue(Cell $cell, $value)
    {
        /* if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        } */

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
