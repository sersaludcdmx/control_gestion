<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Project;
use Livewire\Livewire;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Livewire::setScriptRoute(function ($handle) {
            $custom_url = config('app.url') . '/livewire/livewire.js';
            return Route::get($custom_url, $handle);
        });

        Livewire::setUpdateRoute(function ($handle) {
            $relative_path = Project::getFolderApp(2) . 'livewire/update';
            return Route::post($relative_path, $handle); //->middleware('web');
        });

        // Implicitly grant "Super Admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('Super Admin') ? true : null;
        });
    }
}
