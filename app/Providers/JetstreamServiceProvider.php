<?php

namespace App\Providers;

use App\Actions\Jetstream\DeleteUser;
use Illuminate\Support\ServiceProvider;
use Laravel\Jetstream\Jetstream;

use App\Models\User;
use Laravel\Fortify\Fortify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class JetstreamServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->configurePermissions();

        Jetstream::deleteUsersUsing(DeleteUser::class);

        Fortify::authenticateUsing(function (Request $request) {
            $user = User::where('username', $request->username)->where('is_active', 1)->first();

            if ($user && Hash::check($request->password, $user->password)) {
                $exists = DB::table('personal_access_tokens')->where('tokenable_id', $user->id)->exists();
                if (!$exists) {
                    $token = $user->createToken($user->email)->plainTextToken;
                    $user->token_api = $token;
                    $user->save();
                }
                return $user;
            } /* else {
                return redirect('/')->withErrors(['email' => 'Credenciales incorrectas'])->withInput();
            } */
        });
    }

    /**
     * Configure the permissions that are available within the application.
     */
    protected function configurePermissions(): void
    {
        Jetstream::defaultApiTokenPermissions(['read']);

        Jetstream::permissions([
            'create',
            'read',
            'update',
            'delete',
        ]);
    }
}
