<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;


class Shift extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'is_active',
        'id_indications',
        'id_users',
        'id_priority',
        'id_documents',
        'area',
        'turn',
        'turn_date',
        'turn_hour',
        'observations',
        'status',
        'history'
    ];

    /**
     * A que prioridad pertenece
     */
    public function priority(): BelongsTo
    {
        return $this->belongsTo(Priority::class, 'id_priority');
    }

    /**
     * A que indicación pertenece
     */
    public function indication(): BelongsTo
    {
        return $this->belongsTo(Indication::class, 'id_indications');
    }

    /**
     * A que tipo de documento pertenece
     */
    public function document(): BelongsTo
    {
        return $this->belongsTo(Document::class, 'id_documents');
    }

    /**
     * Tiene una sola conclusion.
     */
    public function conclusion(): HasOne
    {
        return $this->hasOne(Conclusion::class, 'id_shifts');
    }

   /**
     * Tiene una o varias copias.
     */
    public function copy(){

        return $this->hasMany(Copy::class, 'id_shifts');
    }

      /**
     * Tiene un usuario.
     */
    public function user(){

        return $this->belongsTo(User::class, 'id_users');
    }
}
