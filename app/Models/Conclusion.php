<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Conclusion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'is_active',
        'id_users',
        'id_shifts',
        'conclusion_date',
        'answer_document',
        'observation',
        'history'
    ];

    /**
     * Pertenece a un turno.
     */
    public function shift(): BelongsTo
    {
        return $this->belongsTo(shift::class, 'id_shifts');
    }
}
