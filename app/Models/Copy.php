<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Copy extends Model
{
    protected $table = "copys";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'is_active',
        'id_users',
        'id_shifts',
        'area',
        'responsable',
        'description',
        'history'
    ];
}
