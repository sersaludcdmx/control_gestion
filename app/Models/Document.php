<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'is_active',
        'name',
        'id_priority',
        'id_doctypes',
        'id_annexes',
        'id_users',
        'reception_date',
        'volante',
        'document_date',
        'admin_unit',
        'reference',
        'sender',
        'admin_position',
        'subject',
        'addressee',
        'file',
        'quantity',
        'addres_position',
        'history'
    ];

    /**
     * A que prioridad pertenece
     */
    public function priority(): BelongsTo
    {
        return $this->belongsTo(Priority::class, 'id_priority');
    }

    /**
     * A que tipo de documento pertenece
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(DocumentType::class, 'id_doctypes');
    }

    /**
     * A que tipo de documento pertenece
     */
    public function annex(): BelongsTo
    {
        return $this->belongsTo(Annex::class, 'id_annexes');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_users');
    }

    /**
     * A que turno corresponde.
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Shift::class, 'id_documents');
    }
    /**
     * Tiene turnos
     */
    public function shifts(): HasMany
    {
        return $this->hasMany(Shift::class, 'id_documents');
    }
}
