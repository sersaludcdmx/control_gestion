<?php

namespace App\Http\Controllers\Pdf;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;
use PDF;

class MakeDocumentController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Document $document, Request $request)
    {
        $data['document'] = $document;

        $pdf = PDF::loadView('backend.pdf.document.body', $data, [], [
            'format' => 'A4',
            'title'      => 'Documento con volante '.$document->volante,
            'margin_top' => 40,
            'margin_header' => 5

        ]);
        return $pdf->stream( 'Documento con volante ' . $document->volante. '.pdf');
    }
}
