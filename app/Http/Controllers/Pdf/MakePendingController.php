<?php

namespace App\Http\Controllers\Pdf;

use App\Http\Controllers\Controller;
use App\Models\Shift;
use Illuminate\Http\Request;
use PDF;

class MakePendingController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Shift $shift, Request $request)
    {
        $data['shift'] = $shift;

        $pdf = PDF::loadView('backend.pdf.pending.body', $data, [], [
            'format' => 'A4',
            'title'      => 'Turno pendiente',
            'margin_top' => 40,
            'margin_header' => 5

        ]);
        return $pdf->stream('Turno-pendiente-'.$shift->id.'-'. md5(date('Y-m-d H:i:s')) . '.pdf');
    }
}
