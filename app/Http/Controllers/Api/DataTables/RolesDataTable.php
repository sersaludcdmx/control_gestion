<?php

namespace App\Http\Controllers\Api\DataTables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;
use App\Models\Role;
use Carbon\Carbon;

class RolesDataTable extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $model = Role::where('id', '!=', 1);

        return DataTables::eloquent($model)
            ->editColumn('created_at', function ($model) {
                return Carbon::parse($model->created_at)->isoFormat("DD MMM Y - hh:mm");
            })
            ->editColumn('is_active', function ($model) {
                if ($model->is_active == 0) {
                    return '
                    <i class="fas fa-circle text-red-500 mr-2 text-xs"></i>
                    <span class="align-middle text-xs whitespace-nowrap text-red-600 font-bold">Desactivado</span>
                    ';
                } else if ($model->is_active == 1) {
                    return '
                    <i class="fas fa-circle text-emerald-500 mr-2 text-xs"></i>
                    <span class="align-middle text-xs whitespace-nowrap text-emerald-600 font-bold">Activo</span>
                    ';
                }
            })
            ->orderColumn('id', false)
            ->addColumn('btn', 'livewire.role-management.btn') //Agrega la vista html de los botones para las acciones de editar o eliminar.
            ->rawColumns(['btn', 'is_active'])
            ->toJson();
    }
}
