<?php

namespace App\Http\Controllers\Api\DataTables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Document;
use App\Models\Priority;

class ShiftsDataTable extends Controller
{
    public function __invoke(Request $request)
    {
        $model = DB::table('view_pending_shifts')->where(function ($query) {
            if (auth()->user()->can('view.all.pending.shifts')) {
                //No aplica filtro
            } elseif ( auth()->user()->can('view.pending.shifts.only.from.your.area')) {
                $query->where('area_id', '=', auth()->user()->area_id);
            }
        })->where('is_active',config('const.active'));

        return DataTables::query($model)
            ->editColumn('volante', function ($model) {
                if($model->priority_id != 1) {
                    $priority = Priority::find( $model->priority_id );
                    
                    $time = $priority->response_time / 3;
                    

                    $date = $model->turn_date;
                    $hour = ($model->turn_hour) ? $model->turn_hour : '00:00:00';

                    $receptionTime = Carbon::parse("$date $hour");
                    
                    $currentTime = Carbon::now();

                    $section = $receptionTime->diffInHours($currentTime);

                    if($section < $time) {
                        return "<span class='bg-emerald-300 text-emerald-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded'>$model->volante</span >";
                    } else if ($time < $section && ($time * 2) > $section) {
                        return "<span class='bg-amber-300 text-amber-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded'>$model->volante</span >";
                    } else if (($time * 2) < $section) {
                        return "<span class='bg-red-300 text-red-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded'>$model->volante</span >";
                    } else {
                        return "<span class='bg-slate-300 text-slate-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded'>$model->volante</span >";
                    }
                } else {
                    return $model->volante;
                }
            })
            ->addColumn('btn', 'livewire.shifts-management.btn')
            ->rawColumns(['btn', 'is_active', 'volante'])
            ->toJson();
    }   
}
