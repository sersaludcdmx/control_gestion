<?php

namespace App\Http\Controllers\Api\DataTables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Area;

class DocumentsDataTable extends Controller
{


    public function __invoke(Request $request)
    {
        $model = DB::table('view_documents')->where(function ($query) use ($request){

            if( auth()->user()->can('view.all.documents') ) {
                //No aplica filtro
            } elseif( auth()->user()->can('view.documents.only.from.your.area')) {
                $query->where('area_id', '=', auth()->user()->area_id);
            }
        })->where('is_active', config('const.active'));

        return DataTables::query($model)

            //->orderColumn('id', false)
            ->addColumn('btn', 'livewire.documents-management.btn') //Agrega la vista html de los botones para las acciones de editar o eliminar.
            ->rawColumns(['btn', 'is_active'])
            ->toJson();
    }   
}
