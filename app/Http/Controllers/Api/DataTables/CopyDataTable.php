<?php

namespace App\Http\Controllers\Api\DataTables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CopyDataTable extends Controller
{
    public function __invoke(Request $request)
    {
        $model = DB::table('view_copies')
        ->where('is_active', config('const.active'))
        ->where(function ($query) {
            if (auth()->user()->can('view.all.ccp')) {
                //No aplica filtro
            } elseif ( auth()->user()->can('view.ccp.only.from.your.area')) {
                $query->where('area_id', '=', auth()->user()->area_id);
            }
        });

        return DataTables::query($model)

            ->addColumn('btn', 'livewire.copies-management.btn') 
            ->rawColumns(['btn', 'is_active'])
            ->toJson();
    } 
}
