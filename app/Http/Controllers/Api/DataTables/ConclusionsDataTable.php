<?php

namespace App\Http\Controllers\Api\DataTables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ConclusionsDataTable extends Controller
{
    public function __invoke(Request $request)
    {
        $model = DB::table('view_completed_shifts')->where('is_active', config('const.active'))
            ->where(function ($query) {
                if (auth()->user()->can('view.all.shifts.completed')) {
                    //No aplica filtro
                } elseif ( auth()->user()->can('view.shifts.completed.only.from.your.area')) {
                    $query->where('area_id', '=', auth()->user()->area_id);
                }
            });

        return DataTables::query($model)

            //->orderColumn('id', false)
            ->addColumn('btn', 'livewire.completed-shifts-management.btn') //Agrega la vista html de los botones para las acciones de editar o eliminar.
            ->rawColumns(['btn', 'is_active'])
            ->toJson();
    } 
}
