<?php

namespace App\Http\Controllers\Api\DataTables;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class UsersDataTable extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(User $user, Request $request)
    {
        $model = DB::table('view_users')->where(function ($query) use ($user) {
            //$query->where('is_active', 1);
            //$query->where('id', '!=', $user->id);

            if (!auth()->user()->hasRole('Super Admin')) {
                $query->where('role', '!=', 'Super Admin');
            }
        });

        return DataTables::query($model)
            ->editColumn('created_at', function ($user) {
                return Carbon::parse($user->created_at)->diffForHumans();
            })
            ->editColumn('is_active', function ($model) {
                if ($model->is_active == 0) {
                    return '
                    <i class="fas fa-circle text-red-500 mr-2 text-xs"></i>
                    <span class="align-middle text-xs whitespace-nowrap text-red-600 font-bold">Desactivado</span>
                    ';
                } else if ($model->is_active == 1) {
                    return '
                    <i class="fas fa-circle text-emerald-500 mr-2 text-xs"></i>
                    <span class="align-middle text-xs whitespace-nowrap text-emerald-600 font-bold">Activo</span>
                    ';
                }
            })
            ->orderColumn('id', false)
            ->addColumn('btn', 'livewire.users-management.btn') //Agrega la vista html de los botones para las acciones de editar o eliminar.
            ->rawColumns(['btn', 'is_active'])
            ->toJson();
    }
}
