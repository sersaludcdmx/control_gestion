<?php

namespace App\Http\Controllers\Api\Charts;

use App\Http\Controllers\Controller;
use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DocumentsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Area $area, $year, Request $request)
    {

        $border = 'rgb(52, 152, 219)';
        $background = 'rgba(52, 152, 219, 0.3)';

        $countForMonth = collect();

         for($i = 1; $i <= 12; $i++ ) {

            $month = Str::padLeft($i, 2, '0');
            $documents = DB::table('view_documents')->where('created_at', 'LIKE', "$year-$month%")->where('area_id', $area->id)->count();
            $countForMonth->push( $documents );
        }

        $data['labels'] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        

        $data['datasets'] = [
            [
                'backgroundColor' => $background,
                'borderColor' => $border,
                'fill' => true,
                'label' => 'No. documentos',
                'data' => $countForMonth->toArray(),
                'cubicInterpolationMode' => 'monotone',
                'tension' => 0.4
            ]
        ];

        return response()->json($data);
        

    }
}
