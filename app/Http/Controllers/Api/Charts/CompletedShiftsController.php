<?php

namespace App\Http\Controllers\Api\Charts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Area;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CompletedShiftsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Area $area, $year, Request $request)
    {
        $border = 'rgb(251, 96, 144)';
        $background = 'rgba(251, 96, 144, 0.5)';

        $countForMonth = collect();

        for ($i = 1; $i <= 12; $i++) {

            $month = Str::padLeft($i, 2, '0');
            $documents = DB::table('view_completed_shifts')->where('conclusion_date', 'LIKE', "$year-$month%")->where('area_id', $area->id)->count();
            $countForMonth->push($documents);
        }

        $data['labels'] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $data['datasets'] = [
            [
                'backgroundColor' => $background,
                'borderColor' => $border,
                'fill' => true,
                'label' => 'No. documentos',
                'data' => $countForMonth->toArray(),
                'cubicInterpolationMode' => 'monotone',
                'tension' => 0.4
            ]
        ];

        return response()->json($data);
    }
}
