<?php

namespace App\Http\Controllers\Api\Charts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Area;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PendingShiftsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Area $area, $year, Request $request)
    {
        $count_for_month = collect();
        $all_in_zeros = true;

        for ($i = 1; $i <= 12; $i++) {

            $month = Str::padLeft($i, 2, '0');
            $documents = DB::table('view_pending_shifts')->where('turn_date', 'LIKE', "$year-$month%")->where('area_id', $area->id)->count();
            if($documents > 0 ) {
                $all_in_zeros = false;
                $count_for_month->push($documents);
            } else {
                $count_for_month->push(null);
            }
        }

        $data['labels'] = ($all_in_zeros) ? [] : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $data['datasets'] = [
            [
                'backgroundColor' => [
                    'rgba(255, 131, 131, 0.8)',
                    'rgba(161, 214, 203, 0.8)',
                    'rgba(161, 154, 211, 0.8)',
                    'rgba(76, 201, 254, 0.8)',
                    'rgba(255, 119, 183, 0.8)',
                    'rgba(254, 236, 55, 0.8)',
                    'rgba(120, 157, 188, 0.8)',
                    'rgba(199, 91, 122, 0.8)',
                    'rgba(33, 156, 144, 0.8)',
                    'rgba(238, 78, 78, 0.8)',
                    'rgba(69, 63, 120, 0.8)',
                    'rgba(255, 201, 74, 0.8)' 
                ],
                'label' => 'No. documentos',
                'data' => ($all_in_zeros) ? [] : $count_for_month->toArray(),
            ]
        ];

        return response()->json($data);
    }
}
