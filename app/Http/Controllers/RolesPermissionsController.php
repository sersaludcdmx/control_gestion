<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;

class RolesPermissionsController extends Controller
{
    public function edit(Role $role)
    {
        if (auth()->user()->can('roles.assignment.permissions')) {
            return view('livewire.role-management.assignment', [
                'section' => 'Roles',
                'subSection' => 'Asignación',
                'role' => $role,
                'rolesPermissions' => $role->permissions->pluck('name')->toArray(),
                'permissions' => Permission::where('is_active', config('const.active'))->orderBy('module', 'ASC')->get()
            ]);
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Role $role)
    {
        $permissions = Permission::where('is_active', 1)->orderBy('name', 'DESC')->get();

        foreach ($permissions as $permission) {
            $permission->removeRole($role);
        }

        if ($request->permissions) {
            $role->syncPermissions($request->permissions);
        }

        return back()->with('success', 'Permisos asignados al rol con éxito!');
    }
}
