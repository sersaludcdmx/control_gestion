<?php

namespace App\Imports;

use App\Models\Document;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Facades\Validator;

class DocumentsImport implements ToModel, WithHeadingRow
{
    use Importable;
    private $errors = []; // array to accumulate errors

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $rules = [
            'volante' => ['required', 'max:100'],
            'reception_date' => ['required', 'date'],
            'document_date' =>  ['required', 'date'],
            'admin_unit' => ['required', 'max:255'],
            'reference' => ['required', 'max:255'],
            'sender' => ['required', 'max:255'],
            'admin_position' => ['required', 'max:255'],
            'subject' => ['required'],
            'addressee' => ['required', 'max:255'],
            'addres_position' => ['required', 'max:255'],
            'id_priority' => ['required', 'exists:priorities,id'],
            'id_doctypes' => ['required', 'exists:document_types,id'],
            'id_annexes' => ['required', 'exists:annexes,id'],
            'quantity' => ['required', 'integer'],
            'file' =>  ['required'],
        ];

        if (isset($row['id']) && !is_null($row['id'])) {
            $rules['id'] = [ 'numeric', 'unique:documents,id'];
        }

        $validator = Validator::make($row, $rules, $this->validationMessages());

        if ($validator->fails()) {
            $temp = [];
            foreach ($validator->errors()->messages() as $messages) {
                foreach ($messages as $error) {
                    // accumulating errors:
                    $temp[] = $error;
                }
            }
            $row['errors'] = $temp;
            $this->errors[] = $row;
        } else {

            $row['file'] = Str::replace('public', 'documents', $row['file']);
            
            $document = Document::create($row);
            
            return $document;
        }

    }

    // this function returns all validation errors after import:
    public function getErrors()
    {
        return $this->errors;
    }

    public function validationMessages()
    {
        return [
            /* 'name.string' => 'El nombre del medicamento debe ser una cadena de caracteres',
            'name.max' => 'El nombre del medicamento no debe exceder 150 caracteres.',

            'concentration.string' => 'La concentración del medicamento debe ser una cadena de caracteres',
            'concentration.max' => 'La concentración del medicamento no debe exceder 150 caracteres.',

            'description.string' => 'La concentración del medicamento debe ser una cadena de caracteres',
            'description.required' => 'La descripción del medicamento es obligatoria.',

            'presentation.required' => 'La presentación del medicamento es obligatoria.',
            'presentation.string' => 'La presentación del medicamento debe ser una cadena de caracteres',
            'presentation.max' => 'La presentación del medicamento no debe exceder 150 caracteres.',

            'controlled.string' => 'El campo de medicamento controlado debe ser una cadena de caracteres',
            'controlled.max' => 'El campo de medicamento controlado no debe exceder 5 caracteres.',

            'classification_antimicrobials.string' => 'El campo de clasificación de antimicrobianos del medicamento debe ser una cadena de caracteres',

            'fluid_therapy.string' => 'El campo de Fluido Terapéutico debe ser una cadena de caracteres',
            'fluid_therapy.max' => 'El campo de Fluido Terapéutico no debe exceder 20 caracteres.',

            'therapeutic_group.string' => 'El campo de grupo Terapéutico debe ser una cadena de caracteres',
            'therapeutic_group.max' => 'El campo de grupo Terapéutico no debe exceder 150 caracteres.',

            'saica_key.string' => 'El campo de clave SAICA vigente del medicamento debe ser una cadena de caracteres',
            'saica_key.max' => 'El campo de clave SAICA vigente del medicamento no debe exceder 150 caracteres.',

            'csg_key.string' => 'El campo de clave Compendio (CSG) del medicamento debe ser una cadena de caracteres',
            'csg_key.max' => 'El campo de clave Compendio (CSG) del medicamento no debe exceder 150 caracteres.',

            'is_high_cost.digits_between' => 'El campo de si es un medicamento de alto costo debe ser un valor entre 0 y 1.',
            'is_high_cost.numeric' => 'El campo de si es un medicamento de alto costo debe ser un valor numérico.' */
        ];
    }
}
