<?php

namespace App\Livewire\CompletedShiftsManagement;

use App\Models\Shift;
use Livewire\Component;
use Livewire\Attributes\On;

class DetailsComponent extends Component
{
    public $cardModal = false;
    public $title;

    public $current_shift = null;

    public function render()
    {
        return view('livewire.completed-shifts-management.details-component');
    }

    #[On('open-modal-details')]
    public function index(Shift $shift)
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();

        $this->title = 'Registro del turno';
        $this->current_shift = $shift;
        $this->cardModal = true;

    }
}
