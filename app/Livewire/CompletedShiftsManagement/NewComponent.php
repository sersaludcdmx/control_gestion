<?php

namespace App\Livewire\CompletedShiftsManagement;

use Livewire\Component;
use App\Models\Conclusion;
use App\Models\Shift;
use Carbon\Carbon;

class NewComponent extends Component
{
    public $volante;

    public $information = [
        'answer_document' => null,
        'conclusion_date' => null,
        'observation' => null
    ];

    public $rules = [
        'information.answer_document' => ['required', 'max:200'],
        'information.conclusion_date' => ['required', 'date'],
        'information.observation' => ['required'],

    ];

    protected $validationAttributes = [
        'information.answer_document' => 'Fecha de Conclusión',
        'information.conclusion_date' => 'Documento Contestación',
        'information.observation' =>  'Observaciones',
    ];

    public $shift_id;

    public function mount(Shift $shift)
    {
        $this->information['conclusion_date'] = Carbon::now()->toDateString();
        $this->volante = $shift->document->volante;
        $this->shift_id = $shift->id;
    }

    public function render()
    {
        return view('livewire.completed-shifts-management.new-component');
    }

    public function save()
    {

        $this->validate($this->rules);

        $conclusions = Conclusion::create([
            'id_shifts' => $this->shift_id,
            'answer_document' => $this->information['answer_document'],
            'id_users' => auth()->user()->id,
            'conclusion_date' => $this->information['conclusion_date'],
            'observation' => $this->information['observation'],
        ]);

        $description = "Documento contestación: $conclusions->answer_document<br>";
        $description .= "Fecha de conclusión: $conclusions->conclusion_date<br>";
        $description .= "Observaciones: " . $conclusions->observation . "<br><br>";

        $record[] = [
            'movement_date' => Carbon::now()->toDateTimeString(),
            'shift_completed_date' => Carbon::parse($this->information['conclusion_date'])->isoFormat("DD MMM Y"),
            'activity' => 'Conclusión del turno',
            'user_id' => auth()->user()->id,
            'full_name' => auth()->user()->name . " " . auth()->user()->paternal_surname . " " . auth()->user()->maternal_surname,
            'description' => $description,
            'data' => [
                'id_shifts' => $conclusions->id_shifts,
                'answer_document' => $conclusions->answer_document,
                'id_users' => $conclusions->id_users,
                'conclusion_date' => $conclusions->conclusion_date,
                'observation' => $conclusions->observation,
            ]
        ];

        $conclusions->history = json_encode($record);
        $conclusions->save();

        $shift = Shift::find($this->shift_id);
        $shift->status = 'CON';
        $shift->save();

        request()->session()->flash('success', 'Turno concluido con éxito!');
        $this->redirectRoute('completed.index');
    }
}
