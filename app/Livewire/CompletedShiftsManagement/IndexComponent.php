<?php

namespace App\Livewire\CompletedShiftsManagement;

use Livewire\Component;

class IndexComponent extends Component
{
    public $alert = true;

    public function render()
    {
        return view('livewire.completed-shifts-management.index-component');
    }
}
