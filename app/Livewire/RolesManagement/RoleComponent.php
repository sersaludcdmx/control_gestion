<?php

namespace App\Livewire\RolesManagement;

use Livewire\Component;
use Livewire\Attributes\On;

use App\Models\Role;
use Illuminate\Validation\Rule;

class RoleComponent extends Component
{
    public $cardModal = false;
    public $title;
    public $alert = true;
    public $view = 'blank';
    public $action;

    /* Variables de inserción */
    public $information = [
        'name' => null,
        'guard_name' => 'web'
    ];

    public $rol_id;
    /* Variables de catálogos */
    public $roles = [];

    protected $validationAttributes = [
        'information.name' => 'rol'
    ];

    public function render()
    {
        return view('livewire.role-management.role-component');
    }

    public function create()
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();
        $this->action = "CrudStore";
        $this->view = 'create';
        $this->title = 'Creación de rol';
        $this->cardModal = true;
    }

    public function edit(Role $rol)
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();
        $this->action = "CrudUpdated";
        $this->view = 'updated';
        $this->title = 'Actualización de rol';

        $this->information = [
            'name' => $rol->name,
            'guard_name' => 'web'
        ];
        $this->rol_id = $rol->id;
        $this->cardModal = true;
    }

    public function store()
    {
        $this->alert = true;
        $this->validate([
            'information.name' => ['required', 'string', 'max:100', Rule::unique('roles', 'name')->where('is_active', 1)],
        ]);

        $rol = Role::create($this->information);
        $this->reset('information');
        session()->flash('success', 'Rol registrado con éxito!');
        $this->dispatch('refreshTable');
    }

    public function update()
    {
        $this->alert = 1;

        $this->validate([
            'information.name' => ['required', 'string', 'max:100', Rule::unique('roles', 'name')->ignore($this->rol_id)->where('is_active', 1)],
        ]);

        Role::where('id', $this->rol_id)->update($this->information);
        session()->flash('success', 'Rol actualizado con éxito!');
        $this->dispatch('refreshTable');
    }

    #[On('destroy-roles')]
    public function destroy(Role $rol, $status)
    {
        $rol->is_active = $status;
        $rol->save();
        if ($status == 1) {
            $this->dispatch('deleted', title: 'Rol activado', text: 'Rol activado con éxito!');
        }

        if ($status == 0) {
            $this->dispatch('deleted', title: 'Rol desactivado', text: 'Rol desactivado con éxito!');
        }
    }
}
