<?php

namespace App\Livewire\PermissionsManagement;

use Livewire\Component;
use Livewire\Attributes\On; 

use Illuminate\Validation\Rule;
use \App\Models\Permission;

class PermissionComponent extends Component
{
    public $cardModal = false;
    public $title;
    public $alert = true;
    public $view = 'blank';
    public $action;

    /* Variables de inserción */
    public $information = [
        'name' => null,
        'guard_name' => 'web',
        'module' => null,
        'description' => null
    ];

    public $role_old, $permission_id;
    /* Variables de catálogos */
    public $roles = [];

    protected $validationAttributes = [
        'information.name' => 'permiso',
        'information.module' => 'modulo',
        'information.description' => 'descripción'
    ];

    public function render()
    {
        return view('livewire.permission-management.permission-component');
    }

    public function create()
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();

        $this->action = "store";
        $this->view = 'create';
        $this->title = 'Nuevo permiso';
        $this->cardModal = true;
    }

    public function edit(Permission $permission)
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();

        $this->view = 'updated';
        $this->action = "updated";
        $this->title = 'Actualización de permiso';

        $this->information = [
            'name' => $permission->name,
            'guard_name' => 'web',
            'module' => $permission->module,
            'description' => $permission->description
        ];
        $this->permission_id = $permission->id;
        $this->cardModal = true;
    }

    public function store()
    {
        $this->alert = true;
        $this->validate([
            'information.name' => ['required', 'string', 'max:100', Rule::unique('permissions', 'name')->where('is_active', 1)],
            'information.module' => ['required', 'string', 'max:100'],
            'information.description' => ['required', 'string', 'max:100'],
        ]);

        $permission = Permission::create($this->information);
        session()->flash('success', 'Permiso registrado con éxito!');
        $this->reset('information');
        $this->dispatch('refreshTable');
    }

    public function update()
    {
        $this->alert = 1;

        $this->validate([
            'information.name' => ['required', 'string', 'max:100', Rule::unique('permissions', 'name')->ignore($this->permission_id)->where('is_active', 1)],
            'information.module' => ['required', 'string', 'max:100'],
            'information.description' => ['required', 'string', 'max:100'],
        ]);

        Permission::where('id', $this->permission_id)->update($this->information);
        session()->flash('success', 'Permiso actualizado con éxito!');
        $this->dispatch('refreshTable');
    }

    #[On('destroy-permission')]
    public function destroy(Permission $permission, $status)
    {
        $permission->is_active = $status;
        $permission->save();
        if ($status == 1) {
            $this->dispatch('deleted', title: 'Permiso activado', text: 'Permiso activado con éxito!');
        }

        if ($status == 0) {
            $this->dispatch('deleted', title: 'Permiso desactivado', text: 'Permiso desactivado con éxito!');
        }
    }
}
