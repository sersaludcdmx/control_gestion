<?php

namespace App\Livewire\ShiftsManagement;

use Livewire\Component;

use Livewire\WithFileUploads;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Exports\ErrorsExport;
use App\Imports\ShiftsImport;

class ImportComponent extends Component
{
    use WithFileUploads;

    public $modalImport = false;
    public $alert = true;
    public $shifts;
    public $rand;

    protected $messages = [
        'shifts.required' => 'Es obligatorio seleccionar un archivo para importar los datos.',
        'shifts.mimes' => 'El archivo debe ser en formato CSV.',
    ];

    public function render()
    {
        return view('livewire.shifts-management.import-component');
    }

    public function openImport()
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();
        $this->rand = rand();
        $this->modalImport = true;
    }

    public function save()
    {
        $this->alert = true;
        $this->validate([
            'shifts' => 'required|mimes:txt,csv,xlsx|max:1800'
        ]);

        $filename = $this->shifts->getFilename();
        $import = new ShiftsImport();

        $import->import(storage_path('app/public/livewire-tmp/' . $filename));
        $haveErrors = $import->getErrors();

        Storage::delete('livewire-tmp/' . $filename);
        $this->dispatch('refreshTable');
        if (count($haveErrors) > 0) {
            $text = "Se encontraron algunos errores durante la importación, por favor revise el archivo descargado para mas detalles.";
        } else {
            $this->shifts->delete();
            $this->shifts = null;
            $text = 'Archivo importado con éxito!';
        }

        $this->modalImport = false;

        $this->dispatch('action-success', title: 'Importación completa', text: $text);

        if (count($haveErrors) > 0) {
            return Excel::download(new ErrorsExport($haveErrors), Carbon::now()->toDateTimeString() . '.xlsx');
        }
    }
}
