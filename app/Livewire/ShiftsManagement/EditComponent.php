<?php

namespace App\Livewire\ShiftsManagement;

use Livewire\Component;
use App\Models\Shift;
use App\Models\Priority;
use App\Models\Indication;
use App\Models\Document;
use Carbon\Carbon;

class EditComponent extends Component
{
    public $priority = [];
    public $indication = [];
    public $information = [];
    public $document = [];
    public $current_shift_id = null;

    protected $validationAttributes = [
        'information.area' => 'Area',
        'information.turn' => 'Turnado',
        'information.turn_date' =>  'Fecha Turno',
        'information.id_priority' => 'Prioridad del Turno',
        'information.id_doctypes' => 'Tipo de Documento',
        'information.id_indications' => 'Indicacion',
        'information.observations' => 'Observaciones'
    ];

    public $rules = [
        'information.area' => ['required', 'max:100'],
        'information.turn_date' => ['required', 'date'],
        'information.turn' => ['required', 'max:255'],
        'information.observations' => ['required', 'max:255'],
        'information.id_priority' => ['required', 'exists:priorities,id'],
        'information.id_indications' => ['required', 'exists:indications,id'],
    ];

    public function mount(Shift $shift){
        $this->priority = Priority::where('is_active', config('const.active'))->orderBy('name', 'ASC')->get();
        $this->indication = Indication::where('is_active', config('const.active'))->orderBy('name', 'ASC')->get();

        $this->information = [
            'is_active' => $shift->is_active,
            'area' => $shift->area,
            'turn' => $shift->turn,
            'turn_date' => $shift->turn_date .' '. $shift->turn_hour,
            'observations' => $shift->observations,
            'id_priority' => $shift->id_priority,
            'id_indications' => $shift->id_indications,
            'id_users' => $shift->id_users,
            'volante' => $shift->document->volante,
            
        ];

        $this->current_shift_id = $shift->id;
    }

    public function render()
    {
        return view('livewire.shifts-management.edit-component');
    }

    public function update()
    {

        $this->validate($this->rules);

        $shift = Shift::find($this->current_shift_id);

        $date = Carbon::parse($this->information['turn_date']);

        $shift->area = $this->information['area'];
        $shift->turn = $this->information['turn'];
        $shift->turn_date = $date->toDateString();
        $shift->turn_hour = $date->toTimeString();
        $shift->observations = $this->information['observations'];
        $shift->id_priority = $this->information['id_priority'];
        $shift->id_indications = $this->information['id_indications'];
        $shift->id_users = auth()->user()->id;

        if ( $shift->isDirty(['area', 'turn', 'turn_date', 'turn_hour', 'observations', 'id_priority', 'id_indications'])) {
            $record = json_decode($shift->history, true);

            $data = null;
            $description = "";

            if ($shift->isDirty('area')) {
                $description .= "Area de turnado: $shift->area<br>";
                $data['area'] = $shift->area;
            }

            if ($shift->isDirty('turn')) {
                $description .= "Turnado a: $shift->turn<br>";
                $data['turn'] = $shift->turn;
            }

            if ($shift->isDirty('observations')) {
                $description .= "Observaciones: $shift->observations<br>";
                $data['observations'] = $shift->observations;
            }

            if ($shift->isDirty('id_priority')) {
                $description .= "Prioridad del documento: " . $shift->priority->name . "<br>";
                $data['id_priority'] = $shift->id_priority;
            }

            if ($shift->isDirty('id_indications')) {
                $description .= "Indicación: " . $shift->indication->name . "<br>";
                $data['id_indications'] = $shift->id_indications;
            }

            if ($shift->isDirty(['turn_date', 'turn_hour'])) {
                $description .= "Fecha de turnado: $shift->turn_date<br>";
                $data['turn_date'] = $shift->turn_date. ' '.$shift->turn_hour;
            }

            $record[] = [
                'movement_date' => Carbon::now()->toDateTimeString(),
                'shift_date' => $date->isoFormat("DD MMM Y - hh:mm A"),
                'activity' => 'Modificación de turnado',
                'user_id' => auth()->user()->id,
                'full_name' => auth()->user()->name . " " . auth()->user()->paternal_surname . " " . auth()->user()->maternal_surname,
                'description' => $description,
                'data' => $data
            ];

            $shift->id_users = auth()->user()->id;

            $shift->history = json_encode($record);
        }

        $shift->save();

        session()->flash('success', 'Turno actualizado con éxito!');
        $this->redirectRoute('shifts.index');
    }
}
