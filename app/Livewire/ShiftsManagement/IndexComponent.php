<?php

namespace App\Livewire\ShiftsManagement;

use Livewire\Component;

use Livewire\Attributes\On;
use Illuminate\Support\Facades\Storage;
use App\Models\Shift;

class IndexComponent extends Component
{
    public $alert = true;
    
    public function render()
    {
        return view('livewire.shifts-management.index-component');
    }

    #[On('destroy-shifts')]
    public function destroy(Shift $shift, $status)
    {
        $shift->is_active = $status;

        $this->dispatch('deleted', title: 'Turno desactivado', text: 'Turno desactivado con éxito!');

        $shift->save();

        $this->dispatch('deleted', title: 'Turno eliminado', text: 'El turno fue eliminado con éxito!');
    }

    #[On('download-file')]
    public function download(Shift $shift)
    {
        $have_document = $shift->document;
        if($have_document) {
            if (Storage::disk()->exists($shift->document->file)) {
                return Storage::disk()->download($shift->document->file);
            } else {
                $this->dispatch('action-oops', text: 'El archivo ya no se encuentra disponible.');
            }
        } else {
            $this->dispatch('action-oops', text: 'Ocurrió un error al recuperar la información original de la base de datos.');
        }
        
    }
}
