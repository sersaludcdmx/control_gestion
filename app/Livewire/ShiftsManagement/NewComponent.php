<?php

namespace App\Livewire\ShiftsManagement;

use Livewire\Component;
use App\Models\Priority;
use App\Models\Indication;
use App\Models\Document;
use Carbon\Carbon;
use App\Models\Shift;

class NewComponent extends Component
{

    public $information = [
        'is_active' => 1,
        'area' => null,
        'turn' => null,
        'turn_date' => null,
        'observations' => null,
        'status' => null,
        'id_priority' => null,
        'id_indications' => null,
        'id_documents' => null,
        'id_users' => null,
        'volante' => null,
    ];

    public $id_documents;

    public $rules = [
        'information.area' => ['required', 'max:100'],
        'information.turn_date' => ['required', 'date'],
        'information.turn' => ['required', 'max:255'],
        'information.observations' => ['required', 'max:255'],
        'information.id_priority' => ['required', 'exists:priorities,id'],
        'information.id_indications' => ['required', 'exists:indications,id'],
        
    ];

    protected $validationAttributes = [
        'information.area' => 'Area',
        'information.turn' => 'Turnado',
        'information.turn_date' =>  'Fecha Turno',
        'information.id_priority' => 'Prioridad del Turno',
        'information.id_doctypes' => 'Tipo de Documento',
        'information.id_indications' => 'Indicacion',
        'information.observations' => 'Observaciones'
    ];

    public $priority = [];
    public $indication = [];
    public $document_id = null;
    public $shifts = [];
    public $rep_date = [];
    public $doc_date = [];

    public function mount(Document $document){

        $this->information['turn_date'] = Carbon::now()->toDateTimeString();
        $this->priority = Priority::where('is_active', config('const.active') )->orderBy('name', 'ASC')->get();
        $this->indication = Indication::where('is_active', config('const.active') )->orderBy('name', 'ASC')->get();
        $this->document_id = $document->id;

        $this->information['volante']= $document->volante;
    }

    public function render()
    {
        return view('livewire.shifts-management.new-component');
    }
    
    public function save()
    {
        $this->validate($this->rules);

        $date = Carbon::parse($this->information['turn_date']);

        $shift = Shift::create([
            'id_documents'=> $this->document_id,
            'id_indications'=>$this->information['id_indications'],
            'id_users'=> auth()->user()->id,
            'id_priority'=>$this->information['id_priority'],
            'area'=>$this->information['area'],
            'turn'=>$this->information['turn'],
            'turn_date'=> $date->toDateString(),
            'turn_hour' => $date->toTimeString(),
            'observations'=>$this->information['observations'],
            'status'=>'PEN'
        ]);

        $description = "Area de turnado: $shift->area<br>";
        $description .= "Turnado a: $shift->turn<br>";
        $description .= "Fecha del turno: $shift->turn_date<br>";
        $description .= "Prioridad del documento: " . $shift->priority->name . "<br>";
        $description .= "Indicación: " . $shift->indication->name . "<br>";
        $description .= "Observaciones: " . $shift->observations . "<br><br>";

        $record[] = [
            'movement_date' => Carbon::now()->toDateTimeString(),
            'shift_date' => $date->isoFormat("DD MMM Y - hh:mm A"),
            'activity' => 'Alta de turnado',
            'user_id' => auth()->user()->id,
            'full_name' => auth()->user()->name . " " . auth()->user()->paternal_surname . " " . auth()->user()->maternal_surname,
            'description' => $description,
            'data' => [
                'id_documents' => $shift->id_documents,
                'id_indications' => $shift->id_indications,
                'id_users' => auth()->user()->id,
                'id_priority' => $shift->id_priority,
                'area' => $shift->area,
                'turn' => $shift->turn,
                'turn_date' => $shift->turn_date,
                'turn_hour' => $shift->turn_hour,
                'observations' => $shift->observations,
                'status' => 'PEN'
            ]
        ];

        $shift->history = json_encode($record);
        $shift->save();

        request()->session()->flash('success', 'Turno registrado con éxito!');

        $this->redirectRoute('shifts.index');
    }

}
