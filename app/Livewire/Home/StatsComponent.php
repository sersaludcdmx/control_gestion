<?php

namespace App\Livewire\Home;

use Livewire\Component;
use App\Models\Area;
use App\Models\Document;

class StatsComponent extends Component
{
    public $areas = [];
    public $area_id;
    public $years;
    public $year;
    
    public $period;
    public $status = 2;

    public function mount() {
        
        if (auth()->user()->can('select.area.for.graphics')) {
            $this->areas = Area::where('is_active', config('const.active'))->orderBy('name', 'ASC')->get();
        }
        
        $this->area_id = auth()->user()->area_id;

        $yearInit = Document::selectRaw('year(created_at) as `year`')->groupBy('year')->orderBy('year', 'ASC')->first();

        $yearInit = ($yearInit) ? intval($yearInit->year) : intval(date('Y'));
        $yearEnd = intval(date('Y'));

        for ($i = $yearInit; $i <= $yearEnd; $i++) {
            $this->years[] = intval($i);
        }

        $this->year = $yearEnd;

        $this->dispatch('chargeChartDocument', status: 0, area: $this->area_id, year: $this->year);
        $this->dispatch('chargeChartPending', status: 0, area: $this->area_id, year: $this->year);
        $this->dispatch('chargeChartCompleted', status: 0, area: $this->area_id, year: $this->year);
    }

    public function render()
    {
        return view('livewire.home.stats-component');
    }

    public function reloadCharts()
    {
        $this->dispatch('chargeChartDocument', status: 1, area: $this->area_id, year: $this->year);
        $this->dispatch('chargeChartPending', status: 1, area: $this->area_id, year: $this->year);
        $this->dispatch('chargeChartCompleted', status: 1, area: $this->area_id, year: $this->year);
    }
}
