<?php

namespace App\Livewire\Reports;

use Livewire\Component;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Area;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DocumentsExport;

class DocumentsComponent extends Component
{
    public $start_date;
    public $end_date;
    public $area;
    public $alert = true;

    public $areas = [];

    protected $validationAttributes = [
        'start_date' => 'desde',
        'end_date' => 'hasta',
        'area' => 'area'
    ];

    public $rules = [
        'start_date' => ['required', 'date'],
        'end_date' => ['required', 'date']
    ];

    Public function mount()
    {
        if(auth()->user()->can('export.documents.by.area')) {
            $this->areas = Area::where('is_active', config('const.active'))->orderBy('name', 'ASC')->get();
        }
    }

    public function render()
    {
        return view('livewire.reports.documents-component');
    }

    public function export()
    {
        $this->alert = true;
        $rules = $this->rules;

        if(auth()->user()->can('export.documents.by.area')) {
            $rules['area'] = ['required', 'exists:areas,id'];
        }

        $this->validate($rules);

        $start = Carbon::parse($this->start_date);
        $end = Carbon::parse($this->end_date);

        $documents = DB::table('view_documents')->where('is_active', config('const.active') )
            ->whereBetween('created_at', [ $start->startOfDay()->toDateTimeString(), $end->endOfDay()->toDateTimeString() ])
            ->where( function($sQuery) {
                if(auth()->user()->can('export.documents.by.area')) {
                    $sQuery->where('area_id', $this->area);
                } else {
                    $sQuery->where('area_id', auth()->user()->area_id);
                }
            })
            ->orderBy('created_at', 'ASC')->get();

        if($documents->count() < 1) {
            session()->flash('info', 'No se encontraron registros disponibles para generar el reporte.');
        } else if ($documents->count() > 3500) {
            session()->flash('warning', 'No se puede generar el archivo excel por que excede los 3500 registros.');
        } else {

            $filename = 'Documentos '.md5(date('d-m-Y H:i:s') ). '.xlsx';
            //Realiza la ASCarga del archivo
            return Excel::download(new DocumentsExport($documents, $start, $end), $filename);
            
        }
    }
}
