<?php

namespace App\Livewire\Reports;

use Livewire\Component;

class IndexComponent extends Component
{
    public function render()
    {
        return view('livewire.reports.index-component');
    }
}
