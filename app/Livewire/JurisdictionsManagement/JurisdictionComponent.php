<?php

namespace App\Livewire\JurisdictionsManagement;

use Livewire\Component;
use Livewire\Attributes\On;

use App\Models\Jurisdiction;
use Illuminate\Validation\Rule;

class JurisdictionComponent extends Component
{
    public $cardModal = false;
    public $title;
    public $alert = true;
    public $action;

    /* Variables de inserción */
    public $information = [
        'name' => null
    ];

    public $jurisdiction_id;
    /* Variables de catálogos */
    public $roles = [];

    protected $validationAttributes = [
        'information.name' => 'jurisdicción'
    ];

    public function render()
    {
        return view('livewire.jurisdictions-management.jurisdiction-component');
    }

    public function create()
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();
        $this->action = "CrudStore";
        $this->title = 'Registro de jurisdicción';
        $this->cardModal = true;
    }

    public function store()
    {
        $this->alert = true;
        $this->validate([
            'information.name' => ['required', 'string', 'max:50', Rule::unique('jurisdictions', 'name')->where('is_active', 1)],
        ]);

        $jurisdiction = Jurisdiction::create($this->information);
        $this->reset('information');
        session()->flash('success', 'Jurisdicción registrada con éxito!');
        $this->dispatch('refreshTable');
        $this->cardModal = false;
    }

    public function edit(Jurisdiction $jurisdiction)
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();
        $this->action = "CrudUpdated";
        $this->title = 'Actualización de jurisdicción';

        $this->information = [
            'name' => $jurisdiction->name,
        ];
        $this->jurisdiction_id = $jurisdiction->id;
        $this->cardModal = true;
    }

    public function update()
    {
        $this->alert = 1;

        $this->validate([
            'information.name' => ['required', 'string', 'max:100', Rule::unique('jurisdictions', 'name')->ignore($this->jurisdiction_id)->where('is_active', 1)],
        ]);

        Jurisdiction::where('id', $this->jurisdiction_id)->update($this->information);
        session()->flash('success', 'Jurisdicción actualizada con éxito!');
        $this->dispatch('refreshTable');
        $this->cardModal = false;
    }

    #[On('destroy-jurisdictions')]
    public function destroy(Jurisdiction $jurisdiction, $status)
    {
        $jurisdiction->is_active = $status;
        $jurisdiction->save();
        if ($status == 1) {
            $this->dispatch('deleted', title: 'Jurisdicción activada', text: 'Jurisdicción activada con éxito!');
        }

        if ($status == 0) {
            $this->dispatch('deleted', title: 'Jurisdicción desactivada', text: 'Jurisdicción desactivada con éxito!');
        }
    }
}
