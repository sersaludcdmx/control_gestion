<?php

namespace App\Livewire\DocumentsManagement;

use Livewire\Component;

use App\Models\Document;

class HistoricalComponent extends Component
{
    public $document;
    public $record;

    public function mount(Document $document)
    {
        $this->document = $document;

        $this->record = json_decode($document->history, true);
    }

    public function render()
    {
        return view('livewire.documents-management.historical-component');
    }
}
