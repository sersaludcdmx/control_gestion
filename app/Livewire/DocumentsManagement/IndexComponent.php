<?php

namespace App\Livewire\DocumentsManagement;

use Livewire\Component;

use Livewire\Attributes\On;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Document;

class IndexComponent extends Component
{
    public $priority = [];
    public $alert = true;
    public function mount(){
        $this->alert = true;
    }

    public function render()
    {
        return view('livewire.documents-management.index-component');
    }

    #[On('destroy-documents')]
    public function destroy(Document $document, $status)
    {
        $document->is_active = $status;
        $record = json_decode($document->history, true);

        $this->dispatch('deleted', title: 'Documento desactivado', text: 'Documento desactivado con éxito!');

        $record[] = [
            'movement_date' => Carbon::now()->toDateTimeString(),
            'activity' => 'delete',
            'user_id' => auth()->user()->id,
            'full_name' => auth()->user()->name . " " . auth()->user()->paternal_surname . " " . auth()->user()->maternal_surname,
            'data' => null
        ];

        $document->history = json_encode($record);
        $document->save();

        $this->dispatch('deleted', title: 'Documento eliminado', text: 'El documento fue eliminado con éxito!');
    }

    #[On('download-file')]
    public function download(Document $document)
    {
        if (Storage::disk()->exists($document->file)) {
            return Storage::disk()->download($document->file);
        } else {
            $this->dispatch('action-oops', 'Importación completa', text: 'El archivo ya no se encuentra disponible.');
        }
        
    }
}
