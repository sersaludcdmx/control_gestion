<?php

namespace App\Livewire\DocumentsManagement;

use Livewire\Component;
use App\Models\Priority;
use App\Models\Document;
use App\Models\DocumentType;
use App\Models\Annex;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class EditComponent extends Component
{
    use WithFileUploads;

    public $priority = [];
    public $type_document = [];
    public $annex = [];
    public $rep_date = [];
    public $doc_date = [];
    public $rand;
    public $information = [];
    public $current_document_id;
    public $edit_file = null;


    protected $validationAttributes = [
        'information.volante' => 'N° de Volante',
        'information.reception_date' => 'Fecha de Recepción',
        'information.document_date' =>  'Fecha del Documento',
        'information.admin_unit' => 'Unidad Administrativa',
        'information.reference' => 'Referencia',
        'information.sender' => 'Remitente',
        'information.admin_position' => 'Cargo Administrativo Remitente',
        'information.subject' => 'Asunto',
        'information.addressee' => 'Destinatario',
        'information.addres_position' => 'Cargo Administrativo Destinatario',
        'information.id_priority' => 'Prioridad del Documento',
        'information.id_doctypes' => 'Tipo de Documento',
        'information.id_annexes' => 'Anexo',
        'information.quantity' => 'Cantidad de Anexos',
        'edit_file' => 'documento no seleccionado'
    ];
    
    public function render()
    {
        return view('livewire.documents-management.edit-component');
    }

    public $rules = [
        'information.volante' => ['required', 'max:100'],
        'information.reception_date' => ['required', 'date'],
        'information.document_date' =>  ['required', 'date'],
        'information.admin_unit' => ['required', 'max:255'],
        'information.reference' => ['required', 'max:255'],
        'information.sender' => ['required', 'max:255'],
        'information.admin_position' => ['required', 'max:255'],
        'information.subject' => ['required'],
        'information.addressee' => ['required', 'max:255'],
        'information.addres_position' => ['required', 'max:255'],
        'information.id_priority' => ['required', 'exists:priorities,id'],
        'information.id_doctypes' => ['required', 'exists:document_types,id'],
        'information.id_annexes' => ['required', 'exists:annexes,id'],
        'information.quantity' => ['required', 'integer'],
        //'information.file' => 'mimes:pdf|max:2048'
        
    ];

    public function mount(Document $document)
    {
        $this->priority=Priority::where('is_active',1)->orderBy('name', 'ASC')->get();
        $this->type_document=DocumentType::where('is_active',1)->orderBy('name', 'ASC')->get();
        $this->annex=Annex::where('is_active',1)->orderBy('name', 'ASC')->get();

        $this->information = [
            'is_active' => $document->is_active,
            'volante' => $document->volante,
            'reception_date' => $document->reception_date,
            'document_date' => $document->document_date,
            'admin_unit' => $document->admin_unit,
            'reference' => $document->reference,
            'sender' => $document->sender,
            'admin_position' => $document->admin_position,
            'subject' => $document->subject,
            'addressee' => $document->addressee,
            'file' => $document->file,
            'quantity' => $document->quantity,
            'addres_position' => $document->addres_position,
            'id_priority' => $document->id_priority,
            'id_annexes' => $document->id_annexes,
            'id_doctypes' => $document->id_doctypes,
            'id_users' => $document->id_users,
            
        ];

        $this->current_document_id = $document->id;
    }

    public function update()
    {
        $rules = $this->rules;
        if( !is_null($this->edit_file) ) {
            $rule['edit_file'] = ['required', 'mimes:pdf', 'max:2048'];
        }

        $this->validate($rules);

        $document = Document::find($this->current_document_id);

        $document->volante = $this->information['volante'];
        $document->reception_date = $this->information['reception_date'];
        $document->document_date = $this->information['document_date'];
        $document->admin_unit = $this->information['admin_unit'];
        $document->reference = $this->information['reference'];
        $document->sender = $this->information['sender'];
        $document->admin_position = $this->information['admin_position'];
        $document->subject = $this->information['subject'];
        $document->addressee = $this->information['addressee'];
        
        $document->quantity = $this->information['quantity'];
        $document->addres_position = $this->information['addres_position'];
        $document->id_priority = $this->information['id_priority'];
        $document->id_annexes = $this->information['id_annexes'];
        $document->id_doctypes = $this->information['id_doctypes'];

        if (!is_null($this->edit_file)) {

            $volante = $document->id . "-" . Str::replace('/', '-', $document->volante);

            $file_path = "documents/$volante.pdf";
            
            if (Storage::disk()->exists($file_path)) {
                Storage::delete($file_path);
            }

            $this->edit_file->storeAs('documents', "$volante.pdf");

            Storage::delete('livewire-tmp/' . $this->edit_file->getFilename());

            $document->file = $file_path;
            
        }

        if(!is_null($this->edit_file) || $document->isDirty(['reception_date', 'document_date', 'admin_unit', 'reference', 'sender', 'admin_position', 'subject', 'addressee', 'quantity', 'addres_position', 'id_priority', 'id_annexes', 'id_doctypes']) ) {
            $record = json_decode($document->history, true);

            $data = null;
            $description = "";

            if(!is_null($this->edit_file)) {
                $description .= "Se actualizo el archivo pdf.<br>";
                $data['file'] = 'Se actualizo el archivo pdf';
            }

            if($document->isDirty('volante')) {
                $description .= "Volante: $document->volante<br>";
                $data['volante'] = $document->volante;
            }

            if($document->isDirty('reception_date')) {
                $description .= "Fecha de recepción: $document->reception_date<br>";
                $data['reception_date'] = $document->reception_date;
            }

            if ($document->isDirty('document_date')) {
                $description .= "Fecha del documento: $document->document_date<br>";
                $data['document_date'] = $document->document_date;
            }

            if ($document->isDirty('admin_unit')) {
                $description .= "Unidad administrativa: $document->admin_unit<br>";
                $data['admin_unit'] = $document->admin_unit;
            }

            if ($document->isDirty('reference')) {
                $description .= "Referencia: $document->reference<br>";
                $data['reference'] = $document->reference;
            }

            if ($document->isDirty('sender')) {
                $description .= "Remitente: $document->sender<br>";
                $data['sender'] = $document->sender;
            }

            if ($document->isDirty('admin_position')) {
                $description .= "Cargo administrativo remitente: $document->admin_position<br>";
                $data['admin_position'] = $document->admin_position;
            }

            if ($document->isDirty('subject')) {
                $description .= "Asunto: $document->subject<br>";
                $data['subject'] = $document->subject;
            }

            if ($document->isDirty('addressee')) {
                $description .= "Destinatario: $document->addressee<br>";
                $data['addressee'] = $document->addressee;
            }

            if ($document->isDirty('quantity')) {
                $description .= "Cantidad de anexos: $document->quantity<br>";
                $data['quantity'] = $document->quantity;
            }

            if ($document->isDirty('addres_position')) {
                $description .= "Cargo administrativo destinatario: $document->addres_position<br>";
                $data['addres_position'] = $document->addres_position;
            }

            if ($document->isDirty('id_priority')) {
                $data['id_priority'] = $document->id_priority;
                $description .= "Prioridad del documento: " . $document->priority->name . "<br>";
            }

            if ($document->isDirty('id_annexes')) {
                $data['id_annexes'] = $document->id_annexes;
                $description .= "Anexo: " . $document->annex->name . "<br>";
            }

            if ($document->isDirty('id_doctypes')) {
                $data['id_doctypes'] = $document->id_doctypes;
                $description .= "Tipo de documento: " . $document->type->name . "<br>";
            }

            $record[] = [
                'movement_date' => Carbon::now()->toDateTimeString(),
                'activity' => 'Modificación de registro',
                'user_id' => auth()->user()->id,
                'full_name' => auth()->user()->name." ". auth()->user()->paternal_surname. " ". auth()->user()->maternal_surname,
                'data' => $data,
                'description' => $description
            ];

            $document->id_users = auth()->user()->id;

            $document->history = json_encode($record);

        }

        $document->save();
        session()->flash('success', 'Documento actualizado con éxito!');


        $this->dispatch('refreshTable');
        $this->redirectRoute('documents.index');
    }
}
