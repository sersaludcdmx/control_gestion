<?php

namespace App\Livewire\DocumentsManagement;

use Livewire\Component;
use App\Models\Priority;
use App\Models\Document;
use App\Models\DocumentType;
use App\Models\Annex;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Str;

class NewComponent extends Component
{
    use WithFileUploads;

    public $information = [
        'is_active' => 1,
        'volante' => null,
        'reception_date' => null,
        'document_date' => null,
        'admin_unit' => null,
        'reference' => null,
        'sender' => null,
        'admin_position' => null,
        'subject' => null,
        'addressee' => null,
        'file' => null,
        'quantity' => null,
        'addres_position' => null,
        'id_priority' => null,
        'id_doctypes' => null,
        'id_annexes' => null,
        'id_users' => null,
    ];

    public $rules = [
        'information.volante' => ['required', 'max:100', 'unique:documents,volante'],
        'information.reception_date' => ['required', 'date'],
        'information.document_date' =>  ['required', 'date'],
        'information.admin_unit' => ['required', 'max:255'],
        'information.reference' => ['required', 'max:255'],
        'information.sender' => ['required', 'max:255'],
        'information.admin_position' => ['required', 'max:255'],
        'information.subject' => ['required'],
        'information.addressee' => ['required', 'max:255'],
        'information.addres_position' => ['required', 'max:255'],
        'information.id_priority' => ['required', 'exists:priorities,id'],
        'information.id_doctypes' => ['required', 'exists:document_types,id'],
        'information.id_annexes' => ['required', 'exists:annexes,id'],
        'information.quantity' => ['required', 'integer'],
        'information.file' =>  ['required', 'mimes:pdf', 'max:2048'],
        
    ];

    protected $validationAttributes = [
        'information.volante' => 'N° de Volante',
        'information.reception_date' => 'Fecha de Recepción',
        'information.document_date' =>  'Fecha del Documento',
        'information.admin_unit' => 'Unidad Administrativa',
        'information.reference' => 'Referencia',
        'information.sender' => 'Remitente',
        'information.admin_position' => 'Cargo Administrativo Remitente',
        'information.subject' => 'Asunto',
        'information.addressee' => 'Destinatario',
        'information.addres_position' => 'Cargo Administrativo Destinatario',
        'information.id_priority' => 'Prioridad del Documento',
        'information.id_doctypes' => 'Tipo de Documento',
        'information.id_annexes' => 'Anexo',
        'information.quantity' => 'Cantidad de Anexos',
        'information.file' => 'Documento'
    ];

    public $priority = [];
    public $type_document = [];
    public $annex = [];
    public $rep_date = [];
    public $doc_date = [];
    public $rand;

    public function mount(){
        $this->priority=Priority::where('is_active',1)->orderBy('name', 'ASC')->get();
        $this->type_document=DocumentType::where('is_active',1)->orderBy('name', 'ASC')->get();
        $this->annex=Annex::where('is_active',1)->orderBy('name', 'ASC')->get();
    }

    public function render()
    {
        return view('livewire.documents-management.new-component');
    }

    public function save()
    {
    $this->validate($this->rules);
        $rules = $this->rules;
        $this->validate($rules);
        $this->information['id_users']=auth()->user()->id;

        $document = Document::create($this->information);

        $volante = $document->id . "-" . Str::replace('/', '-', $document->volante);

        $file_path = "documents/$volante.pdf";
        if (Storage::disk()->exists($file_path)) {
            Storage::delete($file_path);
        }

        $this->information['file']->storeAs('documents', "$volante.pdf");

        $description = "Número de volante: $document->volante<br>";
        $description .= "Fecha de recepción: $document->reception_date<br>";
        $description .= "Fecha del documento: $document->document_date<br>";
        $description .= "Unidad administrativa: $document->admin_unit<br>";
        $description .= "Referencia: $document->reference<br>";
        $description .= "Remitente: $document->sender<br>";
        $description .= "Cargo administrativo remitente: $document->admin_position<br>";
        $description .= "Destinatario: $document->addressee<br>";
        $description .= "Cargo administrativo destinatario: $document->addres_position<br>";
        $description .= "Prioridad del documento: ".$document->priority->name."<br>";
        $description .= "Tipo de documento: ". $document->type->name."<br>";
        $description .= "Anexo: ". $document->annex->name."<br>";
        $description .= "Cantidad de anexos: $document->quantity<br><br>";
        

        Storage::delete('livewire-tmp/' . $this->information['file']->getFilename());

        $record[] = [
            'movement_date' => Carbon::now()->toDateTimeString(),
            'activity' => 'Alta de documento',
            'user_id' => auth()->user()->id,
            'full_name' => auth()->user()->name . " " . auth()->user()->paternal_surname . " " . auth()->user()->maternal_surname,
            'description' => $description,
            'data' => [
                'volante' => $document->volante,
                'reception_date' => $document->reception_date,
                'document_date' => $document->document_date,
                'admin_unit' => $document->admin_unit,
                'reference' => $document->reference,
                'sender' => $document->sender,
                'admin_position' => $document->admin_position,
                'subject' => $document->subject,
                'addressee' => $document->addressee,
                'addres_position' => $document->addres_position,
                'id_priority' => $document->id_priority,
                'id_doctypes' => $document->id_doctypes,
                'id_annexes' => $document->id_annexes,
                'quantity' => $document->quantity,
                'file' => $file_path
            ]
        ];

        $document->history = json_encode($record);
        
        $document->file = $file_path;
        $document->save();

        request()->session()->flash('success', 'Documento registrado con éxito!');
        $this->dispatch('refreshTable');
        $this->redirectRoute('documents.index');
    }


}
