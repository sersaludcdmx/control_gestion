<?php

namespace App\Livewire\DocumentsManagement;

use Livewire\Component;

use Livewire\Attributes\On;
use App\Models\Document;

class DetailsComponent extends Component
{
    public $cardModal = false;
    public $title;

    public $current_document = null;

    public function render()
    {
        return view('livewire.documents-management.details-component');
    }

    #[On('open-modal-details')]
    public function index(Document $document)
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();

        $this->title = 'Registro del documento';
        $this->current_document = $document;
        $this->cardModal = true;
    }
}
