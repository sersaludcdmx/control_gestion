<?php

namespace App\Livewire\CopiesManagement;

use Livewire\Attributes\On;
use Livewire\Component;
use App\Models\Copy;

class IndexComponent extends Component
{
    public $alert = true;
    
    public function render()
    {
        return view('livewire.copies-management.index-component');
    }

    #[On('destroy-copies')]
    public function destroy(Copy $copy, $status)
    {
        $copy->is_active = $status;

        $this->dispatch('deleted', title: 'Copia eliminada', text: 'Copia eliminada con éxito!');

        $copy->save();

        $this->dispatch('deleted', title: 'Copia eliminada', text: 'La oopia fue eliminada con éxito!');
    }
}
