<?php

namespace App\Livewire\CopiesManagement;

use Livewire\Component;
use App\Models\Copy;
use Carbon\Carbon;

class NewComponent extends Component
{
    public $information = [
        'is_active' => 1,
        'area' => null,
        'responsable' => null,
        'description' => null,
        'id_shifts' => null,
        'id_users' => null,
    ];

    public $rules = [
        'information.area' => ['required', 'max:200'],
        'information.responsable' => ['required', 'max:200'],
        'information.description' => ['required'],
        
    ];

    protected $validationAttributes = [
        'information.area' => 'Dependecia / Unidad Administrativa',
        'information.responsable' => 'Responsable',
        'information.description' =>  'Observaciones',
    ];

    public $shift = [];


    public function render()
    {
        return view('livewire.copies-management.new-component');
    }
    
    public function save()
    {

    $this->validate($this->rules);
        $rules = $this->rules;
        $this->validate($rules);
        $this->information['id_users']=auth()->user()->id;
        $this->information['id_shifts']=$this->shift;

        $copies = Copy::create([
            'id_shifts'=>$this->information['id_shifts'],
            'id_users'=>$this->information['id_users'],
            'area'=>$this->information['area'],
            'responsable'=>$this->information['responsable'],
            'description'=>$this->information['description'],
        ]);

        $description = "Dependencia/Unidad Administrativa: $copies->area<br>";
        $description .= "Responsable: $copies->responsable<br>";
        $description .= "Observaciones: " . $copies->description . "<br><br>";

        $record[] = [
            'movement_date' => Carbon::now()->toDateTimeString(),
            'activity' => 'Creación de copia',
            'user_id' => auth()->user()->id,
            'full_name' => auth()->user()->name . " " . auth()->user()->paternal_surname . " " . auth()->user()->maternal_surname,
            'description' => $description,
            'data' => [
                'id_shifts' => $copies->id_shifts,
                'id_users' => $copies->id_users,
                'area' => $copies->area,
                'responsable' => $copies->responsable,
                'description' => $copies->description,
            ]
        ];

        $copies->history = json_encode($record);
        $copies->save();


        request()->session()->flash('success', 'Copia registrada con éxito!');
        $this->dispatch('refreshTable');
        $this->redirectRoute('copies.index');
    }
}
