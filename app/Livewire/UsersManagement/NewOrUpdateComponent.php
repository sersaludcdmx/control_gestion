<?php

namespace App\Livewire\UsersManagement;

use App\Models\Area;
use Livewire\Component;

use App\Models\Jurisdiction;
use App\Models\User;
use App\Models\Role;

use Livewire\Attributes\On;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class NewOrUpdateComponent extends Component
{
    public $action;
    public $cardModal = false;
    public $title;
    public $alert = true;
    public $current_user_id = null;
    public $jurisdictions;
    public $roles;
    public $areas;
    public $role_old;

    public $information = [
        'name' => null,
        'paternal_surname' => null,
        'maternal_surname' => null,
        'phone' => null,
        'extension' => null,
        'email' => null,
        'username' => null,
        'password' => null,
        'jurisdiction_id' => null,
        'area_id' => null,
        'password_confirmation' => null
    ];

    public $role_name;

    public $rules = [
        'information.name' => ['required', 'max:50'],
        'information.paternal_surname' => ['required', 'max:50'],
        'information.phone' => ['required', 'max:10'],
        'information.jurisdiction_id' => ['required', 'exists:jurisdictions,id'],
        'information.area_id' => ['required', 'exists:areas,id'],
        'role_name' => ['required', 'exists:roles,name'],
    ];

    protected $validationAttributes = [
        'information.name' => 'nombre',
        'information.paternal_surname' => 'apellido paterno',
        'information.maternal_surname' => 'apellido materno',
        'information.username' => 'nombre de usuario',
        'information.email' => 'correo electrónico',
        'information.phone' => 'teléfono',
        'information.extension' => 'extension',
        'information.password' => 'contraseña',
        'information.password_confirmation' => 'confirmación de contraseña',
        'information.jurisdiction_id' => 'jurisdicción',
        'information.area_id' => 'area',
        'role_name' => 'rol',
    ];

    public function render()
    {
        return view('livewire.users-management.new-or-update-component');
    }

    #[On('open-add-modal')]
    public function create()
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();

        $this->loadingCatalogs();
        $this->action = "CrudStore";
        $this->title = 'Registro de usuario';
        $this->cardModal = true;
    }

    #[On('open-edit-modal')]
    public function edit(User $user)
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();

        $this->action = "CrudUpdated";
        $this->title = 'Datos del usuario';

        $this->loadingCatalogs();

        $this->information = [
            'name' => $user->name,
            'paternal_surname' => $user->paternal_surname,
            'maternal_surname' => $user->maternal_surname,
            'phone' => $user->phone,
            'extension' => $user->extension,
            'email' => $user->email,
            'username' => $user->username,
            'jurisdiction_id' => $user->jurisdiction_id,
            'area_id' => $user->area_id
        ];

        $this->role_name = $user->getRoleNames()[0];
        $this->current_user_id = $user->id;
        $this->role_old = $user->getRoleNames()[0];
        $this->cardModal = true;
    }

    #[On('open-password-modal')]
    public function changePasswordModal(User $user)
    {
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();

        $this->current_user_id = $user->id;
        $this->action = "CrudPassword";
        $this->title = 'Actualización de contraseña';
        $this->cardModal = true;
    }

    public function store()
    {
        $this->alert = true;
        $rules = $this->rules;
        if (!is_null($this->information['maternal_surname'])) {
            $rules['information.maternal_surname'] = ['string', 'max:50'];
        }

        if (!is_null($this->information['extension'])) {
            $rules['information.extension'] = ['numeric', 'digits_between:1,5'];
        }

        $rules['information.username'] = ['required', 'max:50', 'unique:users,username'];
        $rules['information.email'] = ['required', 'email', 'max:100', 'unique:users,email'];
        $rules['information.password'] = ['required','min:8' ,'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/', 'confirmed'];

        $this->alert = true;
        $this->validate($rules);

        $this->information['password'] = Hash::make($this->information['password']);


        $user = User::create([
            'name' => $this->information['name'],
            'paternal_surname' => $this->information['paternal_surname'],
            'maternal_surname' => $this->information['maternal_surname'],
            'username' => $this->information['username'],
            'email' => $this->information['email'],
            'phone' => $this->information['phone'],
            'extension' => $this->information['extension'],
            'password' => Hash::make( $this->information['password'] ),
            'jurisdiction_id' => $this->information['jurisdiction_id'],
            'area_id' => $this->information['area_id']
        ]);

        $user->assignRole($this->role_name);
        $this->dispatch('refreshTable');
        $this->cardModal = false;

        $this->dispatch('action-success', title: 'Registro guardado', text: 'Usuario registrado con éxito!');
    }

    public function update()
    {
        $this->alert = true;
        $rules = $this->rules;
        if (!is_null($this->information['maternal_surname'])) {
            $rules['information.maternal_surname'] = ['string', 'max:50'];
        }

        if (!is_null($this->information['extension'])) {
            $rules['information.extension'] = ['numeric', 'digits_between:1,5'];
        }

        $rules['information.username'] = ['required', 'max:50', Rule::unique('users', 'username')->ignore($this->current_user_id)];
        $rules['information.email'] = ['required', 'email', 'max:255', Rule::unique('users', 'email')->ignore($this->current_user_id)];
        $this->validate($rules);

        $user = User::find($this->current_user_id);
        $user->removeRole($this->role_old);

        $role = $this->role_name;

        $user->name = $this->information['name'];
        $user->paternal_surname = $this->information['paternal_surname'];
        $user->maternal_surname = $this->information['maternal_surname'];
        $user->username = $this->information['username'];
        $user->email = $this->information['email'];
        $user->phone = $this->information['phone'];
        $user->extension = $this->information['extension'];
        $user->jurisdiction_id = $this->information['jurisdiction_id'];
        $user->area_id = $this->information['area_id'];

        $user->save();

        $user->assignRole($role);

        $this->dispatch('refreshTable');
        $this->cardModal = false;
        $this->dispatch('action-success', title: 'Registro actualizado', text: 'Usuario actualizado con éxito!');
    }

    public function passwordChange()
    {
        $this->alert = true;
        $this->validate([
            'information.password' => ['required','min:8' ,'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/', 'confirmed']
        ]);

        $this->information['password'] = Hash::make($this->information['password']);
        User::where('id', $this->current_user_id)->update(['password' => $this->information['password']]);
        
        $this->dispatch('action-success', title: 'Contraseña actualizada', text: 'Contraseña actualizada con éxito!');
        $this->dispatch('refreshTable');
        $this->cardModal = false;
    }

    #[On('destroy-users')]
    public function destroy(User $user, $status)
    {
        $user->is_active = $status;
        if ($status == 1) {
            $this->dispatch('deleted', title: 'Usuario activado', text: 'Usuario activado con éxito!');
        }

        if ($status == 0) {
            $this->dispatch('deleted', title: 'Usuario desactivado', text: 'Usuario desactivado con éxito!');
        }
        $user->save();
    }

    public function loadingCatalogs() {
        $this->jurisdictions = Jurisdiction::where('is_active', config('const.active'))->get();
        $this->areas = Area::where('is_active', config('const.active'))->get();

        $this->roles = Role::where(function ($query) {
            if (!auth()->user()->hasRole('Super Admin')) {
                $query->where('name', '!=', 'Super Admin');
            }

            $query->where('is_active', config('const.active') );
        })->orderBy('name', 'ASC')->get();
    }
}
