<?php

namespace App\Livewire\UsersManagement;

use Livewire\Component;

class UserComponent extends Component
{

    public function render()
    {
        return view('livewire.users-management.user-component');
    }

}
