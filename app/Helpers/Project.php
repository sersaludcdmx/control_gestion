<?php

namespace App\Helpers;

class Project
{
    /**
     * Create a new class instance.
     */
    public function __construct()
    {
        //
    }

    /** 
     * @param int $level
     * @return String $relative_path
     */
    public static function getFolderApp($level)
    {
        $relative_path = '';
        if (isset($_SERVER["SERVER_NAME"]) && isset($_SERVER['REQUEST_URI'])) {
            $project_absolute_path = dirname(__DIR__);
            $segments = explode(DIRECTORY_SEPARATOR, $project_absolute_path);
            $project_folder = $segments[count($segments) - $level];
            $requestUrl = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $segments = explode('/', $requestUrl);
            if (is_array($segments) && in_array($project_folder, $segments)) {
                foreach ($segments as $segment) {
                    if ($segment != '') {
                        $relative_path .= $segment . '/';
                        if ($segment == $project_folder) {
                            break;
                        }
                    }
                }
            }
        }

        //$relative_path = substr($relative_path, 0, -1);
        return $relative_path;
    }

    /** 
     * @param int $level
     * @return String $web_url
     */
    public static function buildUrlDynamic($level)
    {
        $web_url = '';

        if (isset($_SERVER['HTTPS'])) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }


        if (isset($_SERVER["SERVER_NAME"]) && isset($_SERVER['REQUEST_URI'])) {
            $project_absolute_path = dirname(__DIR__);
            $segments = explode(DIRECTORY_SEPARATOR, $project_absolute_path);
            $project_folder = $segments[count($segments) - $level];

            $requestUrl = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $segments = explode('/', $requestUrl);
            $web_url = $protocol . $_SERVER["SERVER_NAME"] . '/';
            if (is_array($segments) && in_array($project_folder, $segments)) {
                foreach ($segments as $segment) {
                    if ($segment != '') {
                        $web_url .= $segment . '/';
                        if ($segment == $project_folder) {
                            break;
                        }
                    }
                }
            }
        }

        $web_url = substr($web_url, 0, -1);
        return $web_url;
    }
}
