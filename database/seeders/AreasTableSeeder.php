<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('areas')->delete();
        
        \DB::table('areas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'DIRECCIÓN GENERAL',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'DIRECCIÓN DE ATENCIÓN MÉDICA',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'DIRECCIÓN DE PROMOCIÓN DE LA SALUD',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'DIRECCIÓN DE ADMINISTRACIÓN Y FINANZAS',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'DIRECCIÓN DE ASUNTOS JURÍDICOS',
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P. DE CONTROL Y SEGUMIENTO',
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P. DE NORMATIVIDAD OPERATIVA',
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ABASTECIMIENTO DE INSUMOS',
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ATENCIÓN DOMICILIARIA',
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P. DE SEGUIMIENTO DE LA ATENCIÓN DOMICILIARIA "A"',
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P. DE  SEGUIMIENTO DE LA ATENCIÓN DOMICILIARIA "B"',
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE OPERACIÓN Y SUPERVISIÓN',
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2021-02-26 12:07:26',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE PROGRAMACIÓN Y EVALUACIÓN',
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE SOPORTE PARA EL DIAGNÓSTICO Y TRATAMIENTO',
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ANÁLISIS E INVESTIGACIÓN EPIDEMIOLOGÍA',
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE VIGILANCIA Y EVALUACIÓN EPIDEMIOLOGÍA',
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ENFERMEDADES TRANSMISIBLES',
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ENFERMEDADES NO TRANSMISIBLES',
            ),
            19 => 
            array (
                'id' => 20,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE PROYECTOS E INTERVENCIONES ESTRATÉGICAS',
            ),
            20 => 
            array (
                'id' => 21,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P. DE EVALUACIÓN DE MORBILIDAD Y MORTALIDAD',
            ),
            21 => 
            array (
                'id' => 22,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P. DE PREVENCIÓN DEL VIH SIDA',
            ),
            22 => 
            array (
                'id' => 23,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE EDUCACIÓN PARA LA SALUD',
            ),
            23 => 
            array (
                'id' => 24,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE PARTICIPACIÓN SOCIAL',
            ),
            24 => 
            array (
                'id' => 25,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE EDUCACIÓN Y CULTURA DEL ENVEJECIMIENTO',
            ),
            25 => 
            array (
                'id' => 26,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE PROMOCIÓN DE LA SALUD PARA LOS GRUPOS DE MAYOR RIESGO',
            ),
            26 => 
            array (
                'id' => 27,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ADMINISTRACIÓN DE CAPITAL HUMANO',
            ),
            27 => 
            array (
                'id' => 28,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P. DE ADMINISTRACIÓN DE CAPITAL HUMANO',
            ),
            28 => 
            array (
                'id' => 29,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D DE CONTROL DE PERSONAL',
            ),
            29 => 
            array (
                'id' => 30,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D. DE PRESTACIONES Y POLÍTICA LABORAL',
            ),
            30 => 
            array (
                'id' => 31,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D. DE NOMINAS',
            ),
            31 => 
            array (
                'id' => 32,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE RECURSOS MATERIALES, ABASTECIMIENTO Y SERVICIOS',
            ),
            32 => 
            array (
                'id' => 33,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D. DE COMPRAS Y CONTROL DE MATERIALES',
            ),
            33 => 
            array (
                'id' => 34,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D. DE ALMACENES E INVENTARIOS',
            ),
            34 => 
            array (
                'id' => 35,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D. DE ABASTECIMIENTO Y SERVICIOS',
            ),
            35 => 
            array (
                'id' => 36,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE FINANZAS',
            ),
            36 => 
            array (
                'id' => 37,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'L.C.P DE SEGUIMIENTO FINANCIERO',
            ),
            37 => 
            array (
                'id' => 38,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D. DE CONTROL PRESUPUESTAL',
            ),
            38 => 
            array (
                'id' => 39,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'J.U.D. DE CONTABILIDAD Y REGISTRO',
            ),
            39 => 
            array (
                'id' => 40,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ASUNTOS ADMINISTRATIVOS',
            ),
            40 => 
            array (
                'id' => 41,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SUBDIRECCIÓN DE ASUNTOS CONTENCIOSOS',
            ),
            41 => 
            array (
                'id' => 42,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 1,
                'name' => 'DIRECCIÓN ÁLVARO OBREGÓN',
            ),
            42 => 
            array (
                'id' => 43,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 2,
                'name' => 'DIRECCIÓN AZCAPOTZALCO',
            ),
            43 => 
            array (
                'id' => 44,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 3,
                'name' => 'DIRECCIÓN BENITO JUARÉZ',
            ),
            44 => 
            array (
                'id' => 45,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 4,
                'name' => 'DIRECCIÓN COYOACÁN',
            ),
            45 => 
            array (
                'id' => 46,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 5,
                'name' => 'DIRECCIÓN CUAUHTÉMOC',
            ),
            46 => 
            array (
                'id' => 47,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 6,
                'name' => 'DIRECCIÓN CUAJIMALPA',
            ),
            47 => 
            array (
                'id' => 48,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 7,
                'name' => 'DIRECCIÓN IZTACALCO',
            ),
            48 => 
            array (
                'id' => 49,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 8,
                'name' => 'DIRECCIÓN IZTAPALAPA',
            ),
            49 => 
            array (
                'id' => 50,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 9,
                'name' => 'DIRECCIÓN GUSTAVO A. MADERO',
            ),
            50 => 
            array (
                'id' => 51,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 10,
                'name' => 'DIRECCIÓN MAGDALENA CONTRERAS',
            ),
            51 => 
            array (
                'id' => 52,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 11,
                'name' => 'DIRECCIÓN MIGUEL HIDALGO',
            ),
            52 => 
            array (
                'id' => 53,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 12,
                'name' => 'DIRECCIÓN MILPA ALTA',
            ),
            53 => 
            array (
                'id' => 54,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 13,
                'name' => 'DIRECCIÓN TLÁHUAC',
            ),
            54 => 
            array (
                'id' => 55,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 14,
                'name' => 'DIRECCIÓN TLALPAN',
            ),
            55 => 
            array (
                'id' => 56,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 15,
                'name' => 'DIRECCIÓN VENUSTIANO CARRANZA',
            ),
            56 => 
            array (
                'id' => 57,
                'created_at' => '2019-10-21 12:54:06',
                'updated_at' => '2019-10-21 12:54:06',
                'is_active' => 1,
                'jurisdiction_id' => 16,
                'name' => 'DIRECCIÓN XOCHIMILCO',
            ),
            57 => 
            array (
                'id' => 58,
                'created_at' => '2019-11-13 11:16:09',
                'updated_at' => '2019-11-13 11:16:09',
                'is_active' => 1,
                'jurisdiction_id' => 1,
                'name' => 'TECNOLOGÍAS DE LA INFORMACIÓN',
            ),
            58 => 
            array (
                'id' => 59,
                'created_at' => '2020-04-07 11:16:45',
                'updated_at' => '2020-04-07 11:16:45',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'PRINCIPAL SUBDIRECCIÓN DE RECURSOS MATERIALES, ABASTECIMIENTO Y SERVICIOS',
            ),
            59 => 
            array (
                'id' => 60,
                'created_at' => '2020-04-27 15:17:05',
                'updated_at' => '2020-04-27 15:17:05',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'SERVICIOS GENERALES ',
            ),
            60 => 
            array (
                'id' => 61,
                'created_at' => '2022-08-02 14:40:03',
                'updated_at' => '2022-08-02 14:40:03',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'PROMOCIÓN ENLACE ADMINISTRATIVO',
            ),
            61 => 
            array (
                'id' => 62,
                'created_at' => '2022-08-02 14:40:03',
                'updated_at' => '2022-08-02 14:40:03',
                'is_active' => 1,
                'jurisdiction_id' => 17,
                'name' => 'PROMOCIÓN APOYO LOGISTICA',
            ),
        ));
        
        
    }
}