<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AnnexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Annex::create([
            'id' => 1,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'CAJA'
        ]);

        \App\Models\Annex::create([
            'id' => 2,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'CARPETA'
        ]);

        \App\Models\Annex::create([
            'id' => 3,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'CD'
        ]);

        \App\Models\Annex::create([
            'id' => 4,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'FOLDER'
        ]);

        \App\Models\Annex::create([
            'id' => 5,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'USB'
        ]);

        \App\Models\Annex::create([
            'id' => 6,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'GACETA'
        ]);

        \App\Models\Annex::create([
            'id' => 7,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'HOJAS'
        ]);

        \App\Models\Annex::create([
            'id' => 8,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'SOBRE'
        ]);

        \App\Models\Annex::create([
            'id' => 9,
            'created_at' => '2019-10-21 12:53:40',
            'updated_at' => '2019-10-21 12:53:40',
            'is_active' => 1,
            'name' => 'OTRO'
        ]);

        \App\Models\Annex::create([
            'id' => 10,
            'created_at' => '2019-11-13 10:44:40',
            'updated_at' => '2019-11-13 10:44:40',
            'is_active' => 1,
            'name' => 'SIN ANEXOS'
        ]);
    }
}
