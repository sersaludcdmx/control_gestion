<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Role::create([
            'id' => 1,
            'is_active' => 1,
            'name' => 'Super Admin',
            'guard_name' => 'web',
        ]);
        \App\Models\Role::create([
            'id' => 2,
            'is_active' => 1,
            'name' => 'ADMINISTRADOR',
            'guard_name' => 'web',
        ]);
        \App\Models\Role::create([
            'id' => 3,
            'is_active' => 1,
            'name' => 'CAPTURISTA',
            'guard_name' => 'web',
        ]);
        \App\Models\Role::create([
            'id' => 4,
            'is_active' => 1,
            'name' => 'SUPERVISOR',
            'guard_name' => 'web',
        ]);
    }
}
