<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'is_active' => 1,
                'name' => 'permissions.index',
                'module' => 'Permisos',
                'description' => 'Visualización de sección de permisos',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:12:06',
                'updated_at' => '2025-01-29 17:18:44',
            ),
            1 => 
            array (
                'id' => 2,
                'is_active' => 1,
                'name' => 'roles.index',
                'module' => 'Roles',
                'description' => 'Visualización de sección de roles',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:12:13',
                'updated_at' => '2025-01-29 17:18:33',
            ),
            2 => 
            array (
                'id' => 3,
                'is_active' => 1,
                'name' => 'users.index',
                'module' => 'Usuarios',
                'description' => 'Visualización de sección de usuarios',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:12:21',
                'updated_at' => '2025-01-29 17:18:23',
            ),
            3 => 
            array (
                'id' => 4,
                'is_active' => 1,
                'name' => 'jurisdictions.index',
                'module' => 'Jurisdicciones',
                'description' => 'Visualización de sección de jurisdicciones',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:12:29',
                'updated_at' => '2025-01-29 17:16:37',
            ),
            4 => 
            array (
                'id' => 5,
                'is_active' => 1,
                'name' => 'permissions.create',
                'module' => 'Permisos',
                'description' => 'Creación de permiso',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:12:49',
                'updated_at' => '2025-01-29 17:20:36',
            ),
            5 => 
            array (
                'id' => 6,
                'is_active' => 1,
                'name' => 'permissions.update',
                'module' => 'Permisos',
                'description' => 'Actualización de datos del permiso',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:12:58',
                'updated_at' => '2025-01-29 17:24:18',
            ),
            6 => 
            array (
                'id' => 7,
                'is_active' => 1,
                'name' => 'permissions.inactive',
                'module' => 'Permisos',
                'description' => 'Permitir desactivar un permiso',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:13:05',
                'updated_at' => '2025-01-20 17:13:05',
            ),
            7 => 
            array (
                'id' => 8,
                'is_active' => 1,
                'name' => 'permissions.active',
                'module' => 'Permisos',
                'description' => 'Permitir activar un permiso',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:13:12',
                'updated_at' => '2025-01-20 17:13:12',
            ),
            8 => 
            array (
                'id' => 9,
                'is_active' => 1,
                'name' => 'roles.create',
                'module' => 'Roles',
                'description' => 'Creación de rol',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:13:32',
                'updated_at' => '2025-01-29 17:20:28',
            ),
            9 => 
            array (
                'id' => 10,
                'is_active' => 1,
                'name' => 'roles.assignment.permissions',
                'module' => 'Roles',
                'description' => 'Permitir asignación de permisos a un rol',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:13:43',
                'updated_at' => '2025-01-20 17:13:43',
            ),
            10 => 
            array (
                'id' => 11,
                'is_active' => 1,
                'name' => 'roles.update',
                'module' => 'Roles',
                'description' => 'Actualización de datos del rol',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:13:51',
                'updated_at' => '2025-01-29 17:23:59',
            ),
            11 => 
            array (
                'id' => 12,
                'is_active' => 1,
                'name' => 'roles.inactive',
                'module' => 'Roles',
                'description' => 'Permitir desactivar un rol',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:14:00',
                'updated_at' => '2025-01-20 17:14:00',
            ),
            12 => 
            array (
                'id' => 13,
                'is_active' => 1,
                'name' => 'roles.active',
                'module' => 'Roles',
                'description' => 'Permitir activar un rol',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:14:09',
                'updated_at' => '2025-01-20 17:14:09',
            ),
            13 => 
            array (
                'id' => 14,
                'is_active' => 1,
                'name' => 'users.create',
                'module' => 'Usuarios',
                'description' => 'Creación de usuario',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:14:30',
                'updated_at' => '2025-01-29 17:20:21',
            ),
            14 => 
            array (
                'id' => 15,
                'is_active' => 0,
                'name' => 'users.import',
                'module' => 'Usuarios',
                'description' => 'Permitir importar usuarios',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:14:37',
                'updated_at' => '2025-02-03 17:22:29',
            ),
            15 => 
            array (
                'id' => 16,
                'is_active' => 1,
                'name' => 'users.update',
                'module' => 'Usuarios',
                'description' => 'Actualización de datos del usuario',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:14:47',
                'updated_at' => '2025-01-29 17:23:51',
            ),
            16 => 
            array (
                'id' => 17,
                'is_active' => 1,
                'name' => 'users.password',
                'module' => 'Usuarios',
                'description' => 'Permitir cambiar contraseña a un usuario',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:14:55',
                'updated_at' => '2025-01-20 17:14:55',
            ),
            17 => 
            array (
                'id' => 18,
                'is_active' => 1,
                'name' => 'users.inactive',
                'module' => 'Usuarios',
                'description' => 'Permitir desactivar un usuario',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:15:07',
                'updated_at' => '2025-01-20 17:15:07',
            ),
            18 => 
            array (
                'id' => 19,
                'is_active' => 1,
                'name' => 'users.active',
                'module' => 'Usuarios',
                'description' => 'Permitir activar un usuario',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:15:15',
                'updated_at' => '2025-01-20 17:15:15',
            ),
            19 => 
            array (
                'id' => 20,
                'is_active' => 1,
                'name' => 'jurisdictions.create',
                'module' => 'Jurisdicciones',
                'description' => 'Creación de jurisdicción',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:16:47',
                'updated_at' => '2025-01-29 17:20:13',
            ),
            20 => 
            array (
                'id' => 21,
                'is_active' => 1,
                'name' => 'jurisdictions.update',
                'module' => 'Jurisdicciones',
                'description' => 'Actualización de datos de la jurisdicción',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:16:57',
                'updated_at' => '2025-01-29 17:23:42',
            ),
            21 => 
            array (
                'id' => 22,
                'is_active' => 1,
                'name' => 'jurisdictions.inactive',
                'module' => 'Jurisdicciones',
                'description' => 'Permitir desactivar una jurisdicción',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:17:04',
                'updated_at' => '2025-01-29 17:26:18',
            ),
            22 => 
            array (
                'id' => 23,
                'is_active' => 1,
                'name' => 'jurisdictions.active',
                'module' => 'Jurisdicciones',
                'description' => 'Permitir activar una jurisdicción',
                'guard_name' => 'web',
                'created_at' => '2025-01-20 17:17:12',
                'updated_at' => '2025-01-29 17:25:47',
            ),
            23 => 
            array (
                'id' => 24,
                'is_active' => 1,
                'name' => 'reports.index',
                'module' => 'Reportes',
                'description' => 'Permite el acceso al modulo de reportes',
                'guard_name' => 'web',
                'created_at' => '2025-01-29 17:14:11',
                'updated_at' => '2025-02-03 17:18:04',
            ),
            24 => 
            array (
                'id' => 25,
                'is_active' => 1,
                'name' => 'export.documents.by.area',
                'module' => 'Reportes',
                'description' => 'Exportar documentos seleccionando el área',
                'guard_name' => 'web',
                'created_at' => '2025-01-29 17:34:36',
                'updated_at' => '2025-01-29 17:35:38',
            ),
            25 => 
            array (
                'id' => 26,
                'is_active' => 1,
                'name' => 'export.pending.shifts.by.area',
                'module' => 'Reportes',
                'description' => 'Exportar turnos pendientes seleccionando el área',
                'guard_name' => 'web',
                'created_at' => '2025-01-29 17:35:23',
                'updated_at' => '2025-01-29 17:35:23',
            ),
            26 => 
            array (
                'id' => 27,
                'is_active' => 1,
                'name' => 'export.completed.shifts.by.area',
                'module' => 'Reportes',
                'description' => 'Exportar turnos completados seleccionando el área',
                'guard_name' => 'web',
                'created_at' => '2025-01-29 17:36:19',
                'updated_at' => '2025-01-29 17:36:19',
            ),
            27 => 
            array (
                'id' => 28,
                'is_active' => 1,
                'name' => 'view.documents.only.from.your.area',
                'module' => 'Documentos',
                'description' => 'Ver documentos activos solo del área del usuario',
                'guard_name' => 'web',
                'created_at' => '2025-01-29 17:44:15',
                'updated_at' => '2025-01-29 17:45:05',
            ),
            28 => 
            array (
                'id' => 29,
                'is_active' => 1,
                'name' => 'view.all.documents',
                'module' => 'Documentos',
                'description' => 'Ver todos los documentos activos',
                'guard_name' => 'web',
                'created_at' => '2025-01-29 17:44:42',
                'updated_at' => '2025-01-29 17:44:42',
            ),
            29 => 
            array (
                'id' => 30,
                'is_active' => 1,
                'name' => 'select.area.for.graphics',
                'module' => 'Inicio',
                'description' => 'Seleccionar gráficas por área',
                'guard_name' => 'web',
                'created_at' => '2025-01-30 12:29:57',
                'updated_at' => '2025-01-30 12:29:57',
            ),
            30 => 
            array (
                'id' => 31,
                'is_active' => 0,
                'name' => 'shifts.import',
                'module' => 'Turnos pendientes',
                'description' => 'Permite importar turnos',
                'guard_name' => 'web',
                'created_at' => '2025-01-30 13:53:40',
                'updated_at' => '2025-02-03 17:22:24',
            ),
            31 => 
            array (
                'id' => 32,
                'is_active' => 1,
                'name' => 'documents.index',
                'module' => 'Documentos',
                'description' => 'Permite el acceso al modulo de gestión de documentos',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:02:19',
                'updated_at' => '2025-02-03 17:02:19',
            ),
            32 => 
            array (
                'id' => 33,
                'is_active' => 1,
                'name' => 'pending.shifts.index',
                'module' => 'Turnos pendientes',
                'description' => 'Permite el acceso al modulo de turnos pendientes',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:02:55',
                'updated_at' => '2025-02-03 17:02:55',
            ),
            33 => 
            array (
                'id' => 34,
                'is_active' => 1,
                'name' => 'view.all.pending.shifts',
                'module' => 'Turnos pendientes',
                'description' => 'Permite ver todos los turnos pendientes en el data table',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:03:34',
                'updated_at' => '2025-02-03 17:03:34',
            ),
            34 => 
            array (
                'id' => 35,
                'is_active' => 1,
                'name' => 'view.pending.shifts.only.from.your.area',
                'module' => 'Turnos pendientes',
                'description' => 'Permite ver únicamente los turnos pendientes del área del usuario en el data table',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:04:23',
                'updated_at' => '2025-02-03 17:04:23',
            ),
            35 => 
            array (
                'id' => 36,
                'is_active' => 1,
                'name' => 'shifts.completed.index',
                'module' => 'Turnos concluidos',
                'description' => 'Permite el acceso al modulo de turnos concluidos',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:04:58',
                'updated_at' => '2025-02-03 17:04:58',
            ),
            36 => 
            array (
                'id' => 37,
                'is_active' => 1,
                'name' => 'view.all.shifts.completed',
                'module' => 'Turnos concluidos',
                'description' => 'Permite ver todos los turnos concluidos en el data table',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:05:36',
                'updated_at' => '2025-02-03 17:05:36',
            ),
            37 => 
            array (
                'id' => 38,
                'is_active' => 1,
                'name' => 'view.shifts.completed.only.from.your.area',
                'module' => 'Turnos concluidos',
                'description' => 'Permite ver únicamente los turnos concluidos del área del usuario',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:06:36',
                'updated_at' => '2025-02-03 17:06:36',
            ),
            38 => 
            array (
                'id' => 39,
                'is_active' => 1,
                'name' => 'ccp.index',
                'module' => 'Cccp',
                'description' => 'Permite el acceso al modulo de Cccp',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:07:22',
                'updated_at' => '2025-02-03 17:07:22',
            ),
            39 => 
            array (
                'id' => 40,
                'is_active' => 1,
                'name' => 'view.all.ccp',
                'module' => 'Cccp',
                'description' => 'Permite ver todas las copias en el data table',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:08:05',
                'updated_at' => '2025-02-03 17:08:05',
            ),
            40 => 
            array (
                'id' => 41,
                'is_active' => 1,
                'name' => 'view.ccp.only.from.your.area',
                'module' => 'Cccp',
                'description' => 'Permite ver ver la copias únicamente del área del usuario',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 17:08:43',
                'updated_at' => '2025-02-03 17:08:43',
            ),
            41 => 
            array (
                'id' => 42,
                'is_active' => 1,
                'name' => 'documents.create',
                'module' => 'Documentos',
                'description' => 'Opción de crear',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 18:38:28',
                'updated_at' => '2025-02-03 18:38:28',
            ),
            42 => 
            array (
                'id' => 43,
                'is_active' => 1,
                'name' => 'documents.rotate',
                'module' => 'Documentos',
                'description' => 'Opción de turnar',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 18:38:50',
                'updated_at' => '2025-02-03 18:38:50',
            ),
            43 => 
            array (
                'id' => 44,
                'is_active' => 1,
                'name' => 'documents.details',
                'module' => 'Documentos',
                'description' => 'Opción de ver detalles',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 18:39:19',
                'updated_at' => '2025-02-03 18:39:19',
            ),
            44 => 
            array (
                'id' => 45,
                'is_active' => 1,
                'name' => 'documents.make.pdf',
                'module' => 'Documentos',
                'description' => 'Opción de generar PDF',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 18:39:39',
                'updated_at' => '2025-02-03 18:39:39',
            ),
            45 => 
            array (
                'id' => 46,
                'is_active' => 1,
                'name' => 'documents.download',
                'module' => 'Documentos',
                'description' => 'Opción de descargar',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 18:40:03',
                'updated_at' => '2025-02-03 18:40:03',
            ),
            46 => 
            array (
                'id' => 47,
                'is_active' => 1,
                'name' => 'documents.edit',
                'module' => 'Documentos',
                'description' => 'Opción de editar',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 18:40:37',
                'updated_at' => '2025-02-03 18:40:37',
            ),
            47 => 
            array (
                'id' => 48,
                'is_active' => 1,
                'name' => 'documents.destroy',
                'module' => 'Documentos',
                'description' => 'Opción de eliminar',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 18:40:57',
                'updated_at' => '2025-02-03 18:40:57',
            ),
            48 => 
            array (
                'id' => 49,
                'is_active' => 1,
                'name' => 'pending.shifts.copy',
                'module' => 'Turnos pendientes',
                'description' => 'Opción de generar copia',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:19:07',
                'updated_at' => '2025-02-03 21:19:07',
            ),
            49 => 
            array (
                'id' => 50,
                'is_active' => 1,
                'name' => 'pending.shifts.conclude',
                'module' => 'Turnos pendientes',
                'description' => 'Opción de concluir tuno',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:19:27',
                'updated_at' => '2025-02-03 21:19:27',
            ),
            50 => 
            array (
                'id' => 51,
                'is_active' => 1,
                'name' => 'pending.shifts.details',
                'module' => 'Turnos pendientes',
                'description' => 'Opción de ver detalles del registro',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:20:14',
                'updated_at' => '2025-02-03 21:20:14',
            ),
            51 => 
            array (
                'id' => 52,
                'is_active' => 1,
                'name' => 'pending.shifts.download',
                'module' => 'Turnos pendientes',
                'description' => 'Opción de descargar documento',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:20:43',
                'updated_at' => '2025-02-03 21:20:43',
            ),
            52 => 
            array (
                'id' => 53,
                'is_active' => 1,
                'name' => 'pending.shifts.edit',
                'module' => 'Turnos pendientes',
                'description' => 'Opción de editar turno',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:21:14',
                'updated_at' => '2025-02-03 21:21:14',
            ),
            53 => 
            array (
                'id' => 54,
                'is_active' => 1,
                'name' => 'pending.shifts.destroy',
                'module' => 'Turnos pendientes',
                'description' => 'Opción de eliminar turno',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:21:37',
                'updated_at' => '2025-02-03 21:21:37',
            ),
            54 => 
            array (
                'id' => 55,
                'is_active' => 1,
                'name' => 'pending.shifts.make.pdf',
                'module' => 'Turnos pendientes',
                'description' => 'Opción de generar reporte PDF',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:21:55',
                'updated_at' => '2025-02-03 21:21:55',
            ),
            55 => 
            array (
                'id' => 56,
                'is_active' => 1,
                'name' => 'ccp.destroy',
                'module' => 'Cccp',
                'description' => 'Opción de eliminar copia',
                'guard_name' => 'web',
                'created_at' => '2025-02-03 21:40:22',
                'updated_at' => '2025-02-03 21:40:22',
            ),
        ));
        
        
    }
}