<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleHasPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_has_permissions')->delete();
        
        \DB::table('role_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 3,
                'role_id' => 2,
            ),
            1 => 
            array (
                'permission_id' => 4,
                'role_id' => 2,
            ),
            2 => 
            array (
                'permission_id' => 14,
                'role_id' => 2,
            ),
            3 => 
            array (
                'permission_id' => 16,
                'role_id' => 2,
            ),
            4 => 
            array (
                'permission_id' => 17,
                'role_id' => 2,
            ),
            5 => 
            array (
                'permission_id' => 18,
                'role_id' => 2,
            ),
            6 => 
            array (
                'permission_id' => 19,
                'role_id' => 2,
            ),
            7 => 
            array (
                'permission_id' => 20,
                'role_id' => 2,
            ),
            8 => 
            array (
                'permission_id' => 21,
                'role_id' => 2,
            ),
            9 => 
            array (
                'permission_id' => 22,
                'role_id' => 2,
            ),
            10 => 
            array (
                'permission_id' => 23,
                'role_id' => 2,
            ),
            11 => 
            array (
                'permission_id' => 24,
                'role_id' => 2,
            ),
            12 => 
            array (
                'permission_id' => 25,
                'role_id' => 2,
            ),
            13 => 
            array (
                'permission_id' => 26,
                'role_id' => 2,
            ),
            14 => 
            array (
                'permission_id' => 27,
                'role_id' => 2,
            ),
            15 => 
            array (
                'permission_id' => 29,
                'role_id' => 2,
            ),
            16 => 
            array (
                'permission_id' => 30,
                'role_id' => 2,
            ),
            17 => 
            array (
                'permission_id' => 32,
                'role_id' => 2,
            ),
            18 => 
            array (
                'permission_id' => 33,
                'role_id' => 2,
            ),
            19 => 
            array (
                'permission_id' => 34,
                'role_id' => 2,
            ),
            20 => 
            array (
                'permission_id' => 36,
                'role_id' => 2,
            ),
            21 => 
            array (
                'permission_id' => 37,
                'role_id' => 2,
            ),
            22 => 
            array (
                'permission_id' => 39,
                'role_id' => 2,
            ),
            23 => 
            array (
                'permission_id' => 40,
                'role_id' => 2,
            ),
            24 => 
            array (
                'permission_id' => 42,
                'role_id' => 2,
            ),
            25 => 
            array (
                'permission_id' => 43,
                'role_id' => 2,
            ),
            26 => 
            array (
                'permission_id' => 44,
                'role_id' => 2,
            ),
            27 => 
            array (
                'permission_id' => 45,
                'role_id' => 2,
            ),
            28 => 
            array (
                'permission_id' => 46,
                'role_id' => 2,
            ),
            29 => 
            array (
                'permission_id' => 47,
                'role_id' => 2,
            ),
            30 => 
            array (
                'permission_id' => 48,
                'role_id' => 2,
            ),
            31 => 
            array (
                'permission_id' => 49,
                'role_id' => 2,
            ),
            32 => 
            array (
                'permission_id' => 50,
                'role_id' => 2,
            ),
            33 => 
            array (
                'permission_id' => 51,
                'role_id' => 2,
            ),
            34 => 
            array (
                'permission_id' => 52,
                'role_id' => 2,
            ),
            35 => 
            array (
                'permission_id' => 53,
                'role_id' => 2,
            ),
            36 => 
            array (
                'permission_id' => 54,
                'role_id' => 2,
            ),
            37 => 
            array (
                'permission_id' => 55,
                'role_id' => 2,
            ),
            38 => 
            array (
                'permission_id' => 56,
                'role_id' => 2,
            ),
            39 => 
            array (
                'permission_id' => 24,
                'role_id' => 3,
            ),
            40 => 
            array (
                'permission_id' => 28,
                'role_id' => 3,
            ),
            41 => 
            array (
                'permission_id' => 32,
                'role_id' => 3,
            ),
            42 => 
            array (
                'permission_id' => 33,
                'role_id' => 3,
            ),
            43 => 
            array (
                'permission_id' => 35,
                'role_id' => 3,
            ),
            44 => 
            array (
                'permission_id' => 36,
                'role_id' => 3,
            ),
            45 => 
            array (
                'permission_id' => 38,
                'role_id' => 3,
            ),
            46 => 
            array (
                'permission_id' => 39,
                'role_id' => 3,
            ),
            47 => 
            array (
                'permission_id' => 41,
                'role_id' => 3,
            ),
            48 => 
            array (
                'permission_id' => 42,
                'role_id' => 3,
            ),
            49 => 
            array (
                'permission_id' => 43,
                'role_id' => 3,
            ),
            50 => 
            array (
                'permission_id' => 44,
                'role_id' => 3,
            ),
            51 => 
            array (
                'permission_id' => 45,
                'role_id' => 3,
            ),
            52 => 
            array (
                'permission_id' => 46,
                'role_id' => 3,
            ),
            53 => 
            array (
                'permission_id' => 47,
                'role_id' => 3,
            ),
            54 => 
            array (
                'permission_id' => 49,
                'role_id' => 3,
            ),
            55 => 
            array (
                'permission_id' => 50,
                'role_id' => 3,
            ),
            56 => 
            array (
                'permission_id' => 51,
                'role_id' => 3,
            ),
            57 => 
            array (
                'permission_id' => 52,
                'role_id' => 3,
            ),
            58 => 
            array (
                'permission_id' => 53,
                'role_id' => 3,
            ),
            59 => 
            array (
                'permission_id' => 55,
                'role_id' => 3,
            ),
            60 => 
            array (
                'permission_id' => 56,
                'role_id' => 3,
            ),
            61 => 
            array (
                'permission_id' => 24,
                'role_id' => 4,
            ),
            62 => 
            array (
                'permission_id' => 28,
                'role_id' => 4,
            ),
            63 => 
            array (
                'permission_id' => 32,
                'role_id' => 4,
            ),
            64 => 
            array (
                'permission_id' => 33,
                'role_id' => 4,
            ),
            65 => 
            array (
                'permission_id' => 35,
                'role_id' => 4,
            ),
            66 => 
            array (
                'permission_id' => 36,
                'role_id' => 4,
            ),
            67 => 
            array (
                'permission_id' => 38,
                'role_id' => 4,
            ),
            68 => 
            array (
                'permission_id' => 44,
                'role_id' => 4,
            ),
            69 => 
            array (
                'permission_id' => 45,
                'role_id' => 4,
            ),
            70 => 
            array (
                'permission_id' => 46,
                'role_id' => 4,
            ),
            71 => 
            array (
                'permission_id' => 51,
                'role_id' => 4,
            ),
            72 => 
            array (
                'permission_id' => 52,
                'role_id' => 4,
            ),
            73 => 
            array (
                'permission_id' => 55,
                'role_id' => 4,
            ),
        ));
        
        
    }
}