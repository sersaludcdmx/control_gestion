<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            RoleSeeder::class,
            JurisdictionsTableSeeder::class,
            AreasTableSeeder::class,
            UsersTableSeeder::class,
            ModelHasRolesTableSeeder::class,
            AnnexSeeder::class,
            PrioritySeeder::class,
            IndicationSeeder::class,
            DocumentTypeSeeder::class
        ]);
        //Asignación de permisos al rol = role_has_permissions
        $this->call(PermissionsTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        //$this->call(DocumentsTableSeeder::class);
    }
}
