<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Priority::create([
            'id' => 1,
            'is_active' => 1,
            'name' => 'NO REQUIERE RESPUESTA',
            'response_time' => 0
        ]);

        \App\Models\Priority::create([
            'id' => 2,
            'is_active' => 1,
            'name' => 'RESPUESTA ORDINARIA',
            'response_time' => 120
        ]);

        \App\Models\Priority::create([
            'id' => 3,
            'is_active' => 1,
            'name' => 'RESPUESTA URGENTE',
            'response_time' => 24
        ]);
    }
}
