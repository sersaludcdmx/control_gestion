<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\DocumentType::create([
            'id' => 1,
            'is_active' => 1,
            'name' => 'CARTA'
        ]);

        \App\Models\DocumentType::create([
            'id' => 2,
            'is_active' => 1,
            'name' => 'CIRCULAR'
        ]);

        \App\Models\DocumentType::create([
            'id' => 3,
            'is_active' => 1,
            'name' => 'ESCRITO'
        ]);

        \App\Models\DocumentType::create([
            'id' => 4,
            'is_active' => 1,
            'name' => 'INFORME'
        ]);

        \App\Models\DocumentType::create([
            'id' => 5,
            'is_active' => 1,
            'name' => 'MEMO'
        ]);

        \App\Models\DocumentType::create([
            'id' => 6,
            'is_active' => 1,
            'name' => 'NOTA INFORMATIVA'
        ]);

        \App\Models\DocumentType::create([
            'id' => 7,
            'is_active' => 1,
            'name' => 'OFICIO'
        ]);

        \App\Models\DocumentType::create([
            'id' => 8,
            'is_active' => 1,
            'name' => 'REPORTE DIARIO'
        ]);

        \App\Models\DocumentType::create([
            'id' => 9,
            'is_active' => 1,
            'name' => 'SOLICITUD'
        ]);

        \App\Models\DocumentType::create([
            'id' => 10,
            'is_active' => 1,
            'name' => 'DOCUMENTO JUDICIAL'
        ]);

        \App\Models\DocumentType::create([
            'id' => 11,
            'is_active' => 1,
            'name' => 'INVITACIÓN'
        ]);

        \App\Models\DocumentType::create([
            'id' => 12,
            'is_active' => 1,
            'name' => 'EXPEDIENTE CLINICO'
        ]);

        \App\Models\DocumentType::create([
            'id' => 13,
            'is_active' => 1,
            'name' => 'GACETA OFICIAL'
        ]);

        \App\Models\DocumentType::create([
            'id' => 14,
            'is_active' => 1,
            'name' => 'CORREO ELECTRÓNICO'
        ]);
    }
}
