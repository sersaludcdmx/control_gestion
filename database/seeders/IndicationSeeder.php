<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class IndicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Indication::create([
            'id' => 1,
            'is_active' => 1,
            'name' => 'ATENCIÓN PROCEDENTE'
        ]);

        \App\Models\Indication::create([
            'id' => 2,
            'is_active' => 1,
            'name' => 'CONOCIMIENTO'
        ]);

        \App\Models\Indication::create([
            'id' => 3,
            'is_active' => 1,
            'name' => 'SEGUIMIENTO'
        ]);
    }
}
