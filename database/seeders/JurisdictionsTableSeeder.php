<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class JurisdictionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jurisdictions')->delete();
        
        \DB::table('jurisdictions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN ÁLVARO OBREGÓN',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN AZCAPOTZALCO',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN BENITO JUARÉZ',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN COYOACÁN',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN CUAUHTÉMOC',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN CUAJIMALPA',
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN IZTACALCO',
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN IZTAPALAPA',
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN GUSTAVO A. MADERO',
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN MAGDALENA CONTRERAS',
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN MIGUEL HIDALGO',
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN MILPA ALTA',
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN TLÁHUAC',
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN TLALPAN',
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN VENUSTIANO CARRANZA',
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'JURISDICCIÓN XOCHIMILCO',
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2019-10-21 12:53:59',
                'updated_at' => '2019-10-21 12:53:59',
                'is_active' => 1,
                'name' => 'OFICINAS CENTRALES',
            ),
        ));
        
        
    }
}