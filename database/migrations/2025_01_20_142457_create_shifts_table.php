<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->id()->comment('PK de la tabla de turnos.');
            $table->timestamp('created_at')->useCurrent()->comment('Columna para almacenar la fecha de creación del registro.');
            $table->timestamp('updated_at')->useCurrent()->comment('Columna para almacenar la fecha de la ultima modificación del registro');
            $table->boolean('is_active')->default(1)->comment('Columna para determinar si es registro se toma en cuenta.');

            //Foráneos
            $table->unsignedBigInteger('id_documents')->comment('FK de la tabla de documentos.');
            $table->unsignedBigInteger('id_indications')->comment('FK de la tabla de indicaciones.');
            $table->unsignedBigInteger('id_users')->comment('FK de la tabla de usuarios.');
            $table->unsignedBigInteger('id_priority')->comment('FK de la tabla de prioridades.');

            //campos
            $table->string('area', 255)->nullable();
            $table->string('turn', 255)->nullable();
            $table->date('turn_date')->nullable()->comment('Campo para la fecha de turnado.');
            $table->time('turn_hour')->nullable()->comment('Campo para la hora del turnado.');
            $table->text('observations')->nullable();
            $table->enum('status', ['PEN', 'CON'])->nullable();
            $table->json('history')->nullable()->comment('Campo para generar un historial.');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shifts');
    }
};
