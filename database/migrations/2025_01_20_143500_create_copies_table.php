<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('copys', function (Blueprint $table) {
            $table->id()->comment('PK de la tabla de copias.');
            $table->timestamp('created_at')->useCurrent()->comment('Columna para almacenar la fecha de creación del registro.');
            $table->timestamp('updated_at')->useCurrent()->comment('Columna para almacenar la fecha de la ultima modificación del registro');
            $table->boolean('is_active')->default(1)->comment('Columna para determinar si es registro se toma en cuenta.');

            //Foráneos
            $table->unsignedBigInteger('id_users')->nullable()->comment('FK de la tabla de usuarios.');
            $table->unsignedBigInteger('id_shifts')->nullable()->comment('FK de la tabla de turnos.');

            //Campos
            $table->string('area', 255)->nullable();
            $table->string('responsable', 255)->nullable();
            $table->text('description')->nullable();
            $table->json('history')->nullable()->comment('Campo para generar un historial.');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('copys');
    }
};
