<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->boolean('is_active')->default(1);

            $table->unsignedBigInteger('jurisdiction_id')->comment('FK de la tabla de jurisdicciones.');
            $table->unsignedBigInteger('area_id')->comment('FK de la tabla de areas.');

            $table->string('name', 50)->comment('Nombre de la persona');
            $table->string('paternal_surname', 50)->nullable()->comment('Nombre de la persona');
            $table->string('maternal_surname', 50)->nullable()->comment('Nombre de la persona');
            $table->string('phone', 15)->nullable()->comment('Teléfono de contacto de la persona');
            $table->string('extension', 10)->nullable()->comment('Extension donde se puede localizar a la persona');
            $table->string('email')->comment('Correo electrónico institucional');
            $table->string('username')->unique()->comment('Nombre se usuario para acceso a plataforma');

            $table->string('password');
            $table->rememberToken();
            $table->string('token_api')->nullable()->comment('Token para acceso a rutas API');
            $table->json('history')->nullable()->comment('Almacenamiento de historial de cambios');

            //Definición de llaves primarias/foráneas
            $table->primary('id');

            //Registro de FK
            $table->foreign('jurisdiction_id')->references('id')->on('jurisdictions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('areas')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('email')->primary();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id')->nullable()->index();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->longText('payload');
            $table->integer('last_activity')->index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_reset_tokens');
        Schema::dropIfExists('sessions');
    }
};
