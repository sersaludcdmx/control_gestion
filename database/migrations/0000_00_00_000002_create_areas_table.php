<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->id('id')->comment('PK de la tabla de areas.');
            $table->timestamp('created_at')->useCurrent()->comment('Columna para almacenar la fecha de creación del registro.');
            $table->timestamp('updated_at')->useCurrent()->comment('Columna para almacenar la fecha de la ultima modificación del registro');
            $table->boolean('is_active')->default(1)->comment('Columna para determinar si es registro se toma en cuenta.');

            $table->unsignedBigInteger('jurisdiction_id')->comment('FK de la tabla de areas.');
            $table->string('name', 80)->comment('Nombre de la jurisdicción.');

            $table->primary('id');
            //Registro de FK
            $table->foreign('jurisdiction_id')->references('id')->on('jurisdictions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('areas');
    }
};
