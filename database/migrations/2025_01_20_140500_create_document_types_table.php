<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_types', function (Blueprint $table) {
            $table->id('id')->comment('PK de la tabla de tipos de documentos.');
            $table->timestamp('created_at')->useCurrent()->comment('Columna para almacenar la fecha de creación del registro.');
            $table->timestamp('updated_at')->useCurrent()->comment('Columna para almacenar la fecha de la ultima modificación del registro');
            $table->boolean('is_active')->default(1)->comment('Columna para determinar si es registro se toma en cuenta.');
            $table->string('name', 50)->comment('Nombre del anexo.');

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_types');
    }
};
