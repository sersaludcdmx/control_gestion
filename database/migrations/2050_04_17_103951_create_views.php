<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement('DROP VIEW IF EXISTS view_users');
        DB::statement('DROP VIEW IF EXISTS view_documents');
        DB::statement('DROP VIEW IF EXISTS view_shifts');
        DB::statement('DROP VIEW IF EXISTS view_pending_shifts');
        DB::statement('DROP VIEW IF EXISTS view_completed_shifts');
        DB::statement('DROP VIEW IF EXISTS view_copies');

        DB::statement("
            CREATE VIEW `view_users` AS SELECT 
            t3.id AS id,
            t3.created_at as created_at,
            t3.updated_at as updated_at,
            t3.is_active,
            CONCAT_WS(' ', t3.`name`, t3.paternal_surname, t3.maternal_surname) AS full_name,
            t1.id AS role_id,
            t1.name AS role,
            t3.`name`,
            t3.paternal_surname,
            t3.maternal_surname,
            t3.username,
            t3.email,
            t3.token_api,
            t3.phone,
            t3.extension,
            t3.password,
						t3.area_id,
						t4.`name` AS area_name,
						t3.jurisdiction_id,
						t5.name AS jurisdiction_name
            FROM roles t1 JOIN model_has_roles t2 ON t2.role_id = t1.id JOIN users t3 ON t3.id = t2.model_id 
			JOIN areas t4 ON t4.id = t3.area_id JOIN jurisdictions t5 ON t5.id = t3.jurisdiction_id order by t3.`created_at` DESC
        ");

        DB::statement("
            CREATE VIEW view_documents AS (
            SELECT t1.id AS id, t1.volante AS volante, t1.created_at AS created_at, t1.updated_at AS updated_at, t1.is_active AS is_active, t1.reception_date AS reception_date,
            t1.document_date AS document_date,t1.history, t1.admin_unit AS admin_unit, t1.reference AS reference, t1.sender AS sender, t1.admin_position AS admin_position, t1.`subject` AS `subject`, t1.addressee AS addressee, t1.file AS file, t1.quantity AS quantity, t1.addres_position AS addres_position, priorities.`name` as priority_name, 
            document_types.`name` AS document_types_name, annexes.`name` AS annexes_name, users.`name` AS nombre, users.paternal_surname AS paternal_surname, users.maternal_surname AS maternal_surname, users.area_id FROM documents t1
            JOIN priorities ON priorities.id = t1.id_priority 
            JOIN document_types on document_types.id = t1.id_doctypes 
            JOIN annexes on annexes.id = t1.id_annexes
            JOIN users on users.id = t1.id_users)
        ");

        /* DB::statement("
            CREATE VIEW view_shifts AS (
            SELECT t1.id AS id, t1.created_at AS created_at, t1.updated_at AS updated_at, t1.is_active AS is_active, t1.area AS area, t1.turn AS turn, t1.turn_date AS turn_date, t1.observations AS observations, t1.`status` AS `status`, priorities.`name` AS priority_name, documents.id AS documents_id, documents.volante AS volante, 
            indications.`name` AS indications_name, users.username AS username 
            FROM shifts t1
            JOIN priorities ON priorities.id = t1.id_priority
            JOIN documents ON documents.id = t1.id_documents
            JOIN indications ON indications.id = t1.id_indications
            JOIN users ON users.id = t1.id_users)
        "); */

        DB::statement("
            CREATE VIEW view_pending_shifts AS (SELECT
                t2.id,
                t2.created_at,
                t2.updated_at,
                t2.is_active,
                t1.reference,
                t1.subject,
                t1.reception_date,
                t1.volante,
                t1.admin_unit,
                t4.id as `priority_id`,
                t4.name as priority,
                t1.sender,
                t1.addressee,
                t2.area,
                t2.turn,
                t2.turn_date,
                t2.turn_hour,
                t5.name as indications,
                t2.observations,
                t2.`status`,
                t3.area_id
            FROM `documents` t1 
            JOIN `shifts` t2 ON t2.id_documents = t1.id
            JOIN `users` t3 ON t3.id = t1.id_users
            JOIN  `priorities` t4 ON t4.id = t2.id_priority
            JOIN `indications` t5 ON t5.id = t2.id_indications AND t2.`status` = 'PEN')
        ");

        DB::statement("
            CREATE VIEW view_completed_shifts AS (SELECT 
                t1.`id`,
                t1.`created_at`,
                t1.`updated_at`,
                t1.`is_active`,
                t2.`id` AS `id_shifts`,
                t3.`volante`,
                t2.`area`,
                t2.`turn`,
                t2.`turn_date`,
                t5.`name` as `indication`,
                t2.`observations`,
                t1.`conclusion_date`,
                t1.`answer_document`,
                t1.`observation`,
                t4.`area_id`
            FROM `conclusions` t1 JOIN `shifts` t2 ON t2.id = t1.id_shifts
            JOIN `documents` t3 ON t3.id = t2.id_documents
            JOIN `users` t4 ON t4.id = t3.id_users
            JOIN `indications` t5 ON t5.id = t2.id_indications AND t2.`status` = 'CON')
        ");

        DB::statement("
            CREATE VIEW view_copies AS (SELECT
                t1.id AS id,
                t1.created_at AS created_at,
                t1.updated_at AS updated_at,
                t1.is_active AS is_active,
                t1.area AS area,
                t1.responsable AS responsable,
                t1.description AS description,
                users.username AS username,
                shifts.id AS shifts_id,
                documents.id AS documents_id,
                documents.volante AS volante,
				users.area_id AS area_id
            FROM copys t1
            JOIN users ON users.id = t1.id_users
            JOIN shifts ON shifts.id = t1.id_shifts
            JOIN documents ON documents.id = shifts.id_documents)
        ");


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS view_users');
        DB::statement('DROP VIEW IF EXISTS view_documents');
        DB::statement('DROP VIEW IF EXISTS view_shifts');
        DB::statement('DROP VIEW IF EXISTS view_pending_shifts');
        DB::statement('DROP VIEW IF EXISTS view_completed_shifts');
        DB::statement('DROP VIEW IF EXISTS view_copies');
    }
};
