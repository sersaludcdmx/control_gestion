<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id()->comment('PK de la tabla de documentos.');
            $table->timestamp('created_at')->useCurrent()->comment('Columna para almacenar la fecha de creación del registro.');
            $table->timestamp('updated_at')->useCurrent()->comment('Columna para almacenar la fecha de la ultima modificación del registro');
            $table->boolean('is_active')->default(1)->comment('Columna para determinar si es registro se toma en cuenta.');

            //Foráneos
            $table->unsignedBigInteger('id_priority')->comment('FK de la tabla de prioridades.');
            $table->unsignedBigInteger('id_doctypes')->comment('FK de la tabla de tipo de documentos.');
            $table->unsignedBigInteger('id_annexes')->comment('FK de la tabla de anexos.');
            $table->unsignedBigInteger('id_users')->comment('FK de la tabla de usuarios.');

            //campos
            $table->date('reception_date')->nullable();
            $table->string('volante', 255)->nullable();
            $table->date('document_date')->nullable();
            $table->string('admin_unit', 255)->nullable();
            $table->string('reference', 255)->nullable();
            $table->string('sender', 255)->nullable();
            $table->string('admin_position', 255)->nullable();
            $table->text('subject')->nullable();
            $table->string('addressee', 255)->nullable();
            $table->text('file')->nullable();
            $table->integer('quantity')->default(0);
            $table->string('addres_position', 255)->nullable();
            $table->json('history')->nullable()->comment('Campo para generar un historial.');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
};
