var chartDocuments;
var chartPending;
var chartCompleted;

var targetChartDocument;
var targetChartPending;
var targetChartCompleted;

const emptyFillDoughnut = {
  id: 'emptyDoughnut',
  afterDraw(chart, args, options) {
    const {datasets} = chart.data;
    const {color, width, radiusDecrease} = options;
    let hasData = false;

    for (let i = 0; i < datasets.length; i += 1) {
      const dataset = datasets[i];
      hasData |= dataset.data.length > 0;
    }

    if (!hasData) {
      const {chartArea: {left, top, right, bottom}, ctx} = chart;
      const centerX = (left + right) / 2;
      const centerY = (top + bottom) / 2;
      const r = Math.min(right - left, bottom - top) / 2;

      ctx.beginPath();
      ctx.lineWidth = width || 2;
      ctx.strokeStyle = color || 'rgba(255, 128, 0, 0.5)';
      ctx.arc(centerX, centerY, (r - radiusDecrease || 0), 0, 2 * Math.PI);
      ctx.stroke();
    }
  }
};

function builChartDocuments(status, area, year) {
    if (document.getElementById('chart__documents') != null) {
        targetChartDocument = document.getElementById('chart__documents').getContext('2d');

        if (status == 1) {
            chartDocuments.destroy();
        }

        $.ajax({
            url: `${$('#body-url').data('url') }/api/charts/documents/${area}/${year}`,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + $('.wrapper__main__container').data('token'),
                "Accept": "Application/Json"
            },
            //async: false,
            beforeSend: function () {
                $('.container__spinner__documents').removeClass('hidden');
                $('.container__spinner__documents').addClass('flex');
            },
            success: function (response) {
                chartDocuments = new Chart(targetChartDocument, {
                    type: 'line',
                    data: response,
                    options: {
                        onResize: (chart) => {
                            var widthChart = 0;
                            var heightChart = 450;
                            if ($(window).width() < 1024) {
                                widthChart = $(".container__chart__documents").width();
                                heightChart = 350;
                            } else {
                                widthChart = $(".container__chart__documents").width();
                                heightChart = 370;
                            }

                            if (heightChart < 370) {
                                heightChart = 260;
                            }

                            chart.resize(widthChart, heightChart);
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            y: {
                                ticks: {
                                    display: true
                                },
                                beginAtZero: true,
                                position: 'left',
                            },
                            x: {
                                beginAtZero: true,
                                autoSkip: false,
                                maxRotation: 80,
                                minRotation: 0
                            }
                        },
                        plugins: {
                            legend: false,
                            title: {
                                display: true,
                                text: `Documentos gestionados en el ${year}`,
                            }
                        }
                    }
                });
                $('.container__spinner__documents').addClass('hidden');
            }
        });
    }
}

function builChartPendings(status, area, year) {
    if (document.getElementById('chart__pending') != null) {
        targetChartPending = document.getElementById('chart__pending').getContext('2d');

        if (status == 1) {
            chartPending.destroy();
        }

        $.ajax({
            url: `${$('#body-url').data('url') }/api/charts/pending/${area}/${year}`,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + $('.wrapper__main__container').data('token'),
                "Accept": "Application/Json"
            },
            //async: false,
            beforeSend: function () {
                $('.container__spinner__pending').removeClass('hidden');
                $('.container__spinner__pending').addClass('flex');
            },
            success: function (response) {
                chartPending = new Chart(targetChartPending, {
                    type: 'doughnut',
                    data: response,
                    plugins: [emptyFillDoughnut],
                    options: {
                        onResize: (chart) => {
                            var widthChart = 0;
                            var heightChart = 450;
                            if ($(window).width() < 1024) {
                                widthChart = $(".container__chart__documents").width();
                                heightChart = 350;
                            } else {
                                widthChart = $(".container__chart__documents").width();
                                heightChart = 370;
                            }

                            if (heightChart < 370) {
                                heightChart = 260;
                            }

                            chart.resize(widthChart, heightChart);
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            emptyDoughnut: {
                                color: 'rgba(214, 219, 223, 1)',
                                width: 2,
                                radiusDecrease: 20
                            },
                            legend: true,
                            title: {
                                display: true,
                                text: `Turnos pendientes del ${year}`,
                            }
                        }
                    }
                });
                $('.container__spinner__pending').addClass('hidden');
            }
        });
    }
}

function builChartCompleted(status, area, year) {
    if (document.getElementById('chart__completed') != null) {
        targetChartCompleted = document.getElementById('chart__completed').getContext('2d');

        if (status == 1) {
            chartCompleted.destroy();
        }

        $.ajax({
            url: `${$('#body-url').data('url') }/api/charts/completed/${area}/${year}`,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + $('.wrapper__main__container').data('token'),
                "Accept": "Application/Json"
            },
            //async: false,
            beforeSend: function () {
                $('.container__spinner__completed').removeClass('hidden');
                $('.container__spinner__completed').addClass('flex');
            },
            success: function (response) {
                chartCompleted = new Chart(targetChartCompleted, {
                    type: 'bar',
                    data: response,
                    options: {
                        onResize: (chart) => {
                            var widthChart = 0;
                            var heightChart = 450;
                            if ($(window).width() < 1024) {
                                widthChart = $(".container__chart__completed").width();
                                heightChart = 350;
                            } else {
                                widthChart = $(".container__chart__completed").width();
                                heightChart = 370;
                            }

                            if (heightChart < 370) {
                                heightChart = 260;
                            }

                            chart.resize(widthChart, heightChart);
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            y: {
                                ticks: {
                                    display: true
                                },
                                beginAtZero: true,
                                position: 'left',
                            },
                            x: {
                                beginAtZero: true,
                                autoSkip: false,
                                maxRotation: 80,
                                minRotation: 0
                            }
                        },
                        plugins: {
                            legend: false,
                            title: {
                                display: true,
                                text: `Turnos concluidos en el ${year}`,
                            }
                        }
                    }
                });
                $('.container__spinner__completed').addClass('hidden');
            }
        });
    }
}