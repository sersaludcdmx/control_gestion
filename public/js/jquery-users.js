var table, modal;
$(document).ready(function () {
    //Comprueba que la librería dataTable este cargada
    if (typeof DataTable === 'function') {
        table = $(".container__table table").DataTable({
            layout: {
                topStart: 'pageLength',
                topEnd: $('<div id="toolbar__table__users" class="h-full flex items-center"></div>'),
                bottomStart: 'info',
                bottomEnd: 'paging'
            },
            paging: true, //Genera la pagination de la tabla
            lengthChange: true, //Muestra el numero de registros a visualizar por pagina dentro de la tabla
            processing: true, //Muestra el loader de cargando entre cambio de pagina
            searching: true, //Habilita la búsqueda de términos dentro de la tabla
            //bFilter: true, //Habilita los filtros personalizados
            ordering: true, //Habilita la opción de ordenar por columnas
            info: true, //Muestra la información de los registros en el pie de la tabla
            bAutoWidth: false,
            pagingType: 'full_numbers', //Para poner la paginacíon completa
            order: [2, 'DESC'],
            lengthMenu: [5, 10, 25, 50, 100],
            iDisplayLength: 10,
            serverSide: true, //Indica que la tabla cargara la información por parte
            ajax: { //genera la petición de información a la ruta api
                headers: {
                    'Authorization': 'Bearer ' + $('.wrapper__main__container').data('token'),
                    "Accept": "Application/Json"
                },
                url: $("#body-url").data('url') + '/api/datatables/users/' + $('.wrapper__main__container').data('key'),
                type: "GET"
            },
            columns: [{ //Se indican los campos que se emplearan en la tabla y se le asigna las clases correspondientes
                    data: 'id',
                    orderable: false,
                    className: '!text-center',
                },
                {
                    data: 'role',
                    className: '!text-center',
                },
                {
                    data: 'username',
                    className: '!text-center',
                },
                {
                    data: 'full_name',
                },
                {
                    data: 'jurisdiction_name',
                },
                {
                    data: 'area_name',
                },
                {
                    data: 'phone',
                    className: '!text-center',
                },
                {
                    data: 'extension',
                    className: '!text-center',
                },
                {
                    data: 'email',
                },
                
                /* {
                    data: 'created_at',
                }, */
                {
                    data: 'is_active',
                    className: '!text-center',
                },
                {
                    className: 'w-40 min-w-40',
                    data: 'btn',
                    orderable: false,
                }
            ],
            "language": { //Se genera la traducción de los botones
                "lengthMenu": "Mostrando: _MENU_ por pagina",
                "zeroRecords": "No se encontraron usuarios disponibles",
                "sEmptyTable": "No se encontraron usuarios disponibles",
                "infoEmpty": "No hay usuarios registrados",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "sProcessing": "Procesando...",
                "search": "Buscar:",
                "sInfo": "_END_ de _TOTAL_ usuarios",
                "oPaginate": {
                    "sNext": "<i class='fa-solid fa-angle-right'></i>",
                    "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                    "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                    "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            }
        });

        //Una vez que la pagina de la tabla este cargada con la información
        table.on('draw.td', function () {
            applyOrder(table, 'users');
            var info = table.page.info();
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) { //Genera un numero consecutivo en la tabla como index
                cell.innerHTML = i + 1 + info.start;
            });
        });

        //Crea la fila de indicador de ordenamiento
        table.on('order', function () {
            applyOrder(table, 'users');
        });
    }

    if (typeof Livewire != 'undefined') {

        $('.container__table').on('click', '.btn-option-inactive', function (e) {
            e.preventDefault();
            var user = $(this).data('identifier');
            showAlertActive('Desactivar usuario', '¿Deseas desactivar este usuario?', 'Si, desactivar', user, 0);

        });

        $('.container__table').on('click', '.btn-option-active', function (e) {
            e.preventDefault();
            var user = $(this).data('identifier');
            showAlertActive('Activar usuario', '¿Deseas activar este usuario?', 'Si, activar', user, 1);

        });

        Livewire.on('deleted', ({title, text}) => {
            Swal.fire({
                title: title,
                text: text,
                icon: 'success',
                confirmButtonColor: '#00A896',
                confirmButtonText: 'Aceptar',
            })
            table.ajax.reload(null, true);
        });

        Livewire.on('refreshTable', () => {
            table.ajax.reload(null, true);
        });

    }

    function showAlertActive(title, text, buttonText, user, status) {
        Swal.fire({
            title: title,
            text: text,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: buttonText,
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                Livewire.dispatchTo('users-management.new-or-update-component', 'destroy-users', {
                    user: user,
                    status: status
                });
            }
        })
    }
});
