var table, modal, modalPermission;
$(document).ready(function () {
    //Comprueba que la librería dataTable este cargada
    if (typeof DataTable === 'function') {
        table = $(".container__table table").DataTable({
            layout: {
                topStart: 'pageLength',
                topEnd: $('<div id="toolbar__table__roles" class="h-full flex items-center"></div>'),
                bottomStart: 'info',
                bottomEnd: 'paging'
            },
            paging: true, //Genera la pagination de la tabla
            lengthChange: true, //Muestra el numero de registros a visualizar por pagina dentro de la tabla
            processing: true, //Muestra el loader de cargando entre cambio de pagina
            searching: true, //Habilita la búsqueda de términos dentro de la tabla
            bFilter: true, //Habilita los filtros personalizados
            ordering: true, //Habilita la opción de ordenar por columnas
            info: true, //Muestra la información de los registros en el pie de la tabla
            bAutoWidth: false,
            pagingType: 'full_numbers', //Para poner la paginacíon completa
            order: [1, 'DESC'],
            serverSide: true, //Indica que la tabla cargara la información por parte
            ajax: { //genera la petición de información a la ruta api
                headers: {
                    'Authorization': 'Bearer ' + $('.wrapper__main__container').data('token'),
                    "Accept": "Application/Json"
                },
                url: $("#body-url").data('url') + '/api/datatables/roles',
                type: "GET"
            },
            columns: [{ //Se indican los campos que se emplearan en la tabla y se le asigna las clases correspondientes
                    data: 'id',
                    className: '!text-center',
                    orderable: false,
                },
                {
                    data: 'created_at',
                    className: '!text-center',
                },
                {
                    data: 'name',
                },
                {
                    data: 'is_active',
                    className: '!text-center',
                },
                {
                    className: 'w-36',
                    data: 'btn',
                    orderable: false,
                }
            ],
            "language": { //Se genera la traducción de los botones
                "lengthMenu": "Mostrando: _MENU_ por pagina",
                "zeroRecords": "No se encontraron roles disponibles",
                "sEmptyTable": "No se encontraron roles disponibles",
                "infoEmpty": "No hay roles registrados",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "sProcessing": "Procesando...",
                "search": "Buscar:",
                "sInfo": "_END_ de _TOTAL_ roles",
                "oPaginate": {
                    "sNext": "<i class='fa-solid fa-angle-right'></i>",
                    "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                    "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                    "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            }
        });

        //Una vez que la pagina de la tabla este cargada con la información
        table.on('draw.td', function () {
            applyOrder(table, 'roles');
            var info = table.page.info();
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) { //Genera un numero consecutivo en la tabla como index
                cell.innerHTML = i + 1 + info.start;
            });
        });

        //Crea la fila de indicador de ordenamiento
        table.on('order', function () {
            applyOrder(table, 'roles');
        });
    }

    if (typeof Livewire != 'undefined') {

        Livewire.on('refreshTable', () => {
            table.ajax.reload(null, true);
        });

        $('.container__table').on('click', '.btn-option-inactive', function (e) {
            e.preventDefault();
            var rol = $(this).data('identifier');
            Swal.fire({
                title: 'Desactivar rol',
                text: "¿Deseas desactivar este rol?",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, desactivar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.dispatchTo('roles-management.role-component', 'destroy-roles', { rol: rol, status: 0});
                }
            })

        });

        $('.container__table').on('click', '.btn-option-active', function (e) {
            e.preventDefault();
            var rol = $(this).data('identifier');
            Swal.fire({
                title: 'Activación de rol',
                text: "¿Deseas reactivar este rol?",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, activar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.dispatchTo('roles-management.role-component', 'destroy-roles', { rol: rol, status: 1});
                }
            })

        });

        Livewire.on('deleted', ({title, text}) => {
            Swal.fire({
                title: title,
                text: text,
                icon: 'success',
                confirmButtonColor: '#00A896',
                confirmButtonText: 'Aceptar',
            })
            table.ajax.reload(null, true);
        });
    }
});