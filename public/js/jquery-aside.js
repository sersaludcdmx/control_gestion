$(document).ready(function () {
    var hasCloseSideBar = true;

    $('.wrapper__aside').addClass('aside__close');
    $('.wrapper__main__container').addClass('container__full');

    if ($(window).width() < 1024) {
        $('.toggle__menu__bar').addClass('aside__responsive');
        $('.wrapper__aside').addClass('aside__close');
        $('.wrapper__main__container').addClass('container__full');
        hasCloseSideBar = true;
    } else {
        hasCloseSideBar = false;
        $('.toggle__menu__bar').removeClass('aside__responsive');
        $('.wrapper__main__container').removeClass('container__full');
        $('.wrapper__aside').addClass('aside__open');
        $('.wrapper__aside').removeClass('aside__close');
    }

    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('.toggle__menu__bar').addClass('aside__responsive');
            $('.wrapper__main__container').addClass('container__full');
        } else {
            if (!hasCloseSideBar) {
                $('.wrapper__main__container').removeClass('container__full');
            }
            
            $('.toggle__menu__bar').removeClass('aside__responsive');
        }
    });

    $('.wrapper__main__container').click(function (e) {
        if ($(e.target).hasClass('fa-bars') || $(e.target).hasClass('toggle__menu__bar')) {
            e.preventDefault();
            return;
        } else if (hasCloseSideBar == false && $('.toggle__menu__bar').hasClass('aside__responsive')) {
            closeOpenAsideMovil(true);
        }
    });

    $('.toggle__menu__bar').click(function (e) {
        if ($('.wrapper__aside').hasClass('aside__close')) {
            hasCloseSideBar = false;
        } else {
            hasCloseSideBar = true;
        }

        if ($('.toggle__menu__bar').hasClass('aside__responsive')) {
            closeOpenAsideMovil(hasCloseSideBar);
        } else {
            showAsideDesktop(hasCloseSideBar);
        }
    });

    $(".down__item").click(function (e) {
        if ($('a', this).hasClass('down__active')) {
            $('a i:last-child', this).css('transform', 'rotate(0deg)');
            $('a', this).removeClass('down__active');
            $('ul', this).css('height', '0');

        } else {
            $('a i:last-child', this).css('transform', 'rotate(-90deg)');
            $('a', this).addClass('down__active');
            $('ul', this).css('height', $('a', this).data('height'));
        }
    });

    $('.down__item').each(function (key, value) {
        $('a', this).attr('data-height', $('ul', this).height() + 'px');
        $('ul', this).css('height', '0');
        $('ul', this).css('display', 'block');
        if($('a', this).hasClass('down__active') ) {
            $('a i:last-child', this).css('transform', 'rotate(-90deg)');
            $('ul', this).css('height', $('a', this).data('height'));
        }
    });
});

function showAsideDesktop(hasCloseSideBar) {
    if (hasCloseSideBar == true) {
        $('.wrapper__aside').addClass('aside__close');
        $('.wrapper__aside').removeClass('aside__open');
        $('.wrapper__main__container').addClass('container__full');
    } else {
        $('.wrapper__aside').removeClass('aside__close');
        $('.wrapper__aside').addClass('aside__open');
        $('.wrapper__main__container').removeClass('container__full');
    }
}

function closeOpenAsideMovil(hasCloseSideBar) {
    if (hasCloseSideBar == false) {
        $('.wrapper__aside').addClass('aside__open');
        $('.wrapper__aside').removeClass('aside__close');
    } else {
        $('.wrapper__aside').removeClass('aside__open');
        $('.wrapper__aside').addClass('aside__close');
    }
}