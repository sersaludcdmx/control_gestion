var table, modal;
$(document).ready(function () {
    //Comprueba que la librería dataTable este cargada
    if (typeof DataTable === 'function') {
        table = $(".container__table table").DataTable({
            layout: {
                topStart: 'pageLength',
                topEnd: $('<div id="toolbar__table__documents" class="h-full flex items-center"></div>'),
                bottomStart: 'info',
                bottomEnd: 'paging'
            },
            paging: true, //Genera la pagination de la tabla
            lengthChange: true, //Muestra el numero de registros a visualizar por pagina dentro de la tabla
            processing: true, //Muestra el loader de cargando entre cambio de pagina
            searching: true, //Habilita la búsqueda de términos dentro de la tabla
            //bFilter: true, //Habilita los filtros personalizados
            ordering: true, //Habilita la opción de ordenar por columnas
            info: true, //Muestra la información de los registros en el pie de la tabla
            bAutoWidth: false,
            pagingType: 'full_numbers', //Para poner la paginacíon completa
            order: [1, 'DESC'],
            lengthMenu: [5, 10, 25, 50, 100],
            iDisplayLength: 10,
            serverSide: true, //Indica que la tabla cargara la información por parte
            ajax: { //genera la petición de información a la ruta api
                headers: {
                    'Authorization': 'Bearer ' + $('.wrapper__main__container').data('token'),
                    "Accept": "Application/Json"
                },
                url: $("#body-url").data('url') + '/api/datatables/documents',
                type: "GET"
            },
            columns: [{ //Se indican los campos que se emplearan en la tabla y se le asigna las clases correspondientes
                    data: 'id',
                    orderable: false,
                    className: 'dt-body-center',
                },
                {
                    data: 'created_at',
                    className: 'dt-body-center',
                },
                {
                    data: 'reception_date',
                    className: 'dt-body-center',
                },
                {
                    data: 'volante',
                    className: 'dt-body-center',
                },
                {
                    data: 'document_date',
                    className: 'dt-body-center',
                },
                {
                    data: 'admin_unit',
                },
                {
                    data: 'reference',
                    className: 'dt-body-center',
                },
                {
                    data: 'sender',
                },
                {
                    data: 'admin_position'
                },
/*                 {
                    data: 'addressee',
                },
                {
                    data: 'addres_position'
                }, */
                {
                    data: 'priority_name',
                    className: 'dt-body-center'
                },
/*                 {
                    data: 'document_types_name',
                    className: 'dt-body-center'
                },
                {
                    data: 'annexes_name',
                    className: 'dt-body-center'
                }, */
                {
                    data: 'subject'
                },
                {
                    className: 'w-40 min-w-40',
                    data: 'btn',
                    orderable: false,
                }
            ],
            "language": { //Se genera la traducción de los botones
                "lengthMenu": "Mostrando: _MENU_ por pagina",
                "zeroRecords": "No se encontraron registros disponibles",
                "sEmptyTable": "No se encontraron registros disponibles",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "sProcessing": "Procesando...",
                "search": "Buscar:",
                "sInfo": "_END_ de _TOTAL_ registros",
                "oPaginate": {
                    "sNext": "<i class='fa-solid fa-angle-right'></i>",
                    "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                    "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                    "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            },
            initComplete: function () {
                var classTh = "px-1 py-1 text-white bg-slate-50 border-y border-box first:border-l-0 last:border-r-0";
                $('#table__documents thead').append("<tr id='documents__input__search'></tr>");
                this.api()
                    .columns()
                    .every(function () {
                        var column = this;
                        var title = column.header().textContent;
                        if (title != '#' && title != 'Opciones' && title != 'Fecha de registro' && title != 'Fecha de recepción' && title != 'Fecha del documento' && title != 'Categoría') {
                            // Create input element and add event listener
                            $('<th class="'+classTh+'"><input type="text" placeholder="Filtrar..." class="border-slate-300 block text-sm md:text-sm w-full font-semibold text-slate-600 py-1.5 rounded transition ease-in-out duration-100 focus:outline-none focus:ring-transparent"/></th>')
                                .appendTo( $("#documents__input__search") )
                                .on('keyup change clear', $.debounce(250, function(e) {
                                    if (column.search() !== $(this).children().val() ) {
                                        column.search($(this).children().val()).draw();                               
                                    }
                                    })
                                );
                        } else if (title == 'Fecha de registro' || title == 'Fecha de recepción' || title == 'Fecha del documento') {
                            // Create input element and add event listener
                            $('<th class="'+classTh+'"><input type="text" placeholder="aaaa-mm-dd" class="border-slate-300 block text-sm md:text-sm w-full font-semibold text-slate-600 py-1.5 rounded transition ease-in-out duration-100 focus:outline-none focus:ring-transparent"/></th>')
                                .appendTo( $("#documents__input__search") )
                                .on('keyup change clear', $.debounce(250, function(e) {
                                    if (column.search() !== $(this).children().val() ) {
                                        column.search($(this).children().val()).draw();                               
                                    }
                                    })
                                );
                        } else {
                            $('<th class="'+classTh+'"></th>')
                                .appendTo( $("#documents__input__search") );
                        }
                         
                    });
            }
        });

        table.on('draw.td', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) { //Genera un numero consecutivo en la tabla como index
                cell.innerHTML = i + 1 + info.start;
            });
        });

    }

    if (typeof Livewire != 'undefined') {

        $('.container__table').on('click', '.btn-option-inactive', function (e) {
            e.preventDefault();
            var document = $(this).data('identifier');
            showAlertActive('Desactivar documento', '¿Deseas desactivar este documento?', 'Si, desactivar', document, 0);

        });

        $('.container__table').on('click', '.btn-option-active', function (e) {
            e.preventDefault();
            var document = $(this).data('identifier');
            showAlertActive('Activar documento', '¿Deseas activar este documento?', 'Si, activar', document, 1);

        });

        Livewire.on('deleted', ({title, text}) => {
            Swal.fire({
                title: title,
                text: text,
                icon: 'success',
                confirmButtonColor: '#00A896',
                confirmButtonText: 'Aceptar',
            })
            table.ajax.reload(null, true);
        });

        Livewire.on('refreshTable', () => {
            table.ajax.reload(null, true);
        });

    }

    function showAlertActive(title, text, buttonText, document, status) {
        Swal.fire({
            title: title,
            text: text,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: buttonText,
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                Livewire.dispatchTo('documents-management.index-component', 'destroy-documents', {
                    document: document,
                    status: status
                });
            }
        })
    }

});
