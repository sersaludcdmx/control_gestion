/*! DataTables Tailwind CSS integration
 */

(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		var jq = require('jquery');
		var cjsRequires = function (root, $) {
			if ( ! $.fn.dataTable ) {
				require('datatables.net')(root, $);
			}
		};

		if (typeof window === 'undefined') {
			module.exports = function (root, $) {
				if ( ! root ) {
					// CommonJS environments without a window global must pass a
					// root. This will give an error otherwise
					root = window;
				}

				if ( ! $ ) {
					$ = jq( root );
				}

				cjsRequires( root, $ );
				return factory( $, root, root.document );
			};
		}
		else {
			cjsRequires( window, jq );
			module.exports = factory( jq, window, window.document );
		}
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document ) {
'use strict';
var DataTable = $.fn.dataTable;



/*
 * This is a tech preview of Tailwind CSS integration with DataTables.
 */

// Set the defaults for DataTables initialisation
$.extend( true, DataTable.defaults, {
	renderer: 'tailwindcss'
} );


// Default class modification
$.extend( true, DataTable.ext.classes, {
	container: "dt-container dt-tailwindcss relative",
	search: {
		input: "border placeholder-gray-500 ml-2 px-3 py-2 rounded-lg border-gray-200 focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50 dark:bg-gray-800 dark:border-gray-600 dark:focus:border-blue-500 dark:placeholder-gray-400"
	},
	length: {
		container: "text-slate-500 font-semibold text-xs md:text-sm",
		select: "border pl-2 pr-2 py-1.5 min-w-16 text-xs md:text-sm rounded border-slate-300 focus:border-blue-500 focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50"
	},
	processing: {
		container: "dt-processing absolute flex-col font-semibold text-slate-500 items-center  justify-center h-full w-full top-0 left-0 bg-white/60 z-20"
	},
	paging: {
		container: "border border-slate-200 rounded md:rounded-lg shadow px-1 py-2 font-semibold text-xs text-slate-500",
		active: ' bg-emerald-800 text-white rounded-md',
		notActive: 'bg-white',
		button: 'py-0.5 px-2 hover:z-10 focus:z-10 active:z-10 text-xs md:text-sm',
		first: '',
		last: '',
		enabled: '',
		notEnabled: 'text-slate-300'
	},
	table: 'dataTable min-w-full',
	thead: {
		row: 'border-y first:border-t-0 border-box',
		cell: 'px-2.5 py-2 text-white bg-emerald-900 font-semibold text-center text-sm md:text-base border-emerald-800 border-box'
	},
	tbody: {
		row: 'even:bg-slate-50 border-y last:border-b-0',
		cell: 'text-start text-slate-500 text-xs md:text-sm px-1.5 py-1.5 font-medium border-x first:border-l-0 last:border-r-0 border-box'
	},
	tfoot: {
		row: 'even:bg-slate-50',
		cell: 'p-3 text-left'
	},
	info: {
		container: "font-semibold text-xs md:text-sm text-slate-500 text-center md:text-start"
	},
	empty: {
		row: 'text-center text-slate-500 text-xs md:text-sm px-1.5 py-2 md:py-3 font-semibold border-box',
		cell: ''
	}
} );

DataTable.ext.renderer.pagingButton.tailwindcss = function (settings, buttonType, content, active, disabled) {
	var classes = settings.oClasses.paging;
	var btnClasses = [classes.button];

	btnClasses.push(active ? classes.active : classes.notActive);
	btnClasses.push(disabled ? classes.notEnabled : classes.enabled);

	var a = $('<a>', {
		'href': disabled ? null : '#',
		'class': btnClasses.join(' ')
	})
		.html(content);

	return {
		display: a,
		clicker: a
	};
};

DataTable.ext.renderer.pagingContainer.tailwindcss = function (settings, buttonEls) {
	var classes = settings.oClasses.paging;

	buttonEls[0].addClass(classes.first);
	buttonEls[buttonEls.length -1].addClass(classes.last);

	return $('<ul/>').addClass('pagination flex items-center justify-center space-x-1').append(buttonEls);
};

DataTable.ext.renderer.layout.tailwindcss = function ( settings, container, items ) {
	var row = $( '<div/>', {
			"class": items.full ?
				'grid grid-cols-1 gap-4 mb-4 overflow-y-auto rounded border' :
				'grid grid-cols-1 md:grid-cols-2 gap-4 mb-4'
		} )
		.appendTo( container );

	$.each( items, function (key, val) {
		var klass;

		// Apply start / end (left / right when ltr) margins
		if (val.table) {
			klass = 'col-span-2';
		}
		else if (key === 'start') {
			klass = 'md:justify-self-start';
		}
		else if (key === 'end') {
			klass = 'md:col-start-2 md:justify-self-end';
		}
		else {
			klass = 'col-span-2 justify-self-center';
		}

		$( '<div/>', {
				id: val.id || null,
				"class": klass + ' ' + (val.className || '')
			} )
			.append( val.contents )
			.appendTo( row );
	} );
};

return DataTable;
}));
