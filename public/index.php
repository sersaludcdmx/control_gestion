<?php
// Obtiene la URL actual
$currentURL = $_SERVER['REQUEST_URI'];

// Verifica si la URL contiene "/public/"
if (strpos($currentURL, '/public/') !== false) {
    // Reemplaza "/public/" con una cadena vacía para eliminarlo de la URL
    $newURL = str_replace('/public/', '', $currentURL);

    // Redirige al usuario a la URL corregida sin "/public/"
    header("Location: $newURL", true, 301);
    exit();
}
