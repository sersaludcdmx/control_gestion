<?php

use Illuminate\Support\Facades\Route;
set_time_limit(0);

Route::get('/', function () {

    if (isset(auth()->user()->id)) {
        return redirect()->route('home.index');
    }
    return view('auth.login');

})->name('login');

Route::post('login', [\App\Actions\Fortify\AuthenticatedSessionController::class, 'store'])->name('login.store');

Route::middleware(['auth:sanctum', config('jetstream.auth_session')])->group(function () {

    Route::get('/inicio', [\App\Http\Controllers\HomeController::class, 'index'])->name('home.index');

    Route::get('permisos', \App\Livewire\PermissionsManagement\PermissionComponent::class)->name('permissions.index'); //->middleware('permission:view.permissions');

    Route::prefix('roles')->group(function () {
        Route::get('/', \App\Livewire\RolesManagement\RoleComponent::class)->name('roles.index'); //->middleware('permission:view.roles');

        Route::get('{role}/asignacion-de-permisos', [\App\Http\Controllers\RolesPermissionsController::class, 'edit'])->name('rolespermisos.edit'); //->middleware('permission:assignment.permissions.roles');

        Route::put('{role}/asignacion-de-permisos', [\App\Http\Controllers\RolesPermissionsController::class, 'update'])->name('rolespermisos.update'); //->middleware('permission:assignment.permissions.roles');
    });

    Route::get('usuarios', \App\Livewire\UsersManagement\UserComponent::class)->name('users.index'); //->middleware('permission:view.users');

    Route::get('jurisdicciones', \App\Livewire\JurisdictionsManagement\JurisdictionComponent::class)->name('jurisdictions.index');
    
    Route::prefix('documentos')->group(function() {
        Route::get('/', \App\Livewire\DocumentsManagement\IndexComponent::class)->name('documents.index');

        Route::get('/nuevo-documento', \App\Livewire\DocumentsManagement\NewComponent::class)->name('documents.new');

        Route::get('/{document}/editar-documento', \App\Livewire\DocumentsManagement\EditComponent::class)->name('documents.edit');

        Route::get('/{document}/generar-archivo', \App\Http\Controllers\Pdf\MakeDocumentController::class)->name('document.print');

        Route::get('/{document}/asignacion-de-turno', \App\Livewire\ShiftsManagement\NewComponent::class)->name('shifts.new');

        Route::get('/{document}/historico', \App\Livewire\DocumentsManagement\HistoricalComponent::class)->name('documents.historical');

    });

    Route::prefix('turnos-pendientes')->group(function() {
        Route::get('/', \App\Livewire\ShiftsManagement\IndexComponent::class)->name('shifts.index');

        Route::get('/{shift}/editar-turno', \App\Livewire\ShiftsManagement\EditComponent::class)->name('shifts.edit');

        Route::get('/{shift}/generar-archivo', \App\Http\Controllers\Pdf\MakePendingController::class)->name('pending.print');

        Route::get('/{shift}/concluir-turno', \App\Livewire\CompletedShiftsManagement\NewComponent::class)->name('completed.new');
    });

    Route::prefix('turnos-concluidos')->group(function () {
        Route::get('/', \App\Livewire\CompletedShiftsManagement\IndexComponent::class)->name('completed.index');
    });

    Route::prefix('copias')->group(function () {
        Route::get('/', \App\Livewire\CopiesManagement\IndexComponent::class)->name('copies.index');

        Route::get('/{shift}/nueva-copia', \App\Livewire\CopiesManagement\NewComponent::class)->name('copies.new');   
    });

    Route::get('reportes', \App\Livewire\Reports\IndexComponent::class)->name('reports.index');

});