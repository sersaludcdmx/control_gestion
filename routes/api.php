<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::prefix('datatables')->group(function () {
        Route::get('/permissions', \App\Http\Controllers\Api\DataTables\PermissionsDataTable::class);

        Route::get('/roles', \App\Http\Controllers\Api\DataTables\RolesDataTable::class);

        //Ruta para acceso a todos los usuarios excepto el que realiza la solicitud
        Route::get('users/{user}', \App\Http\Controllers\Api\DataTables\UsersDataTable::class);

        //Ruta para acceso a la tabla de jurisdicciones
        Route::get('/jurisdictions', \App\Http\Controllers\Api\DataTables\JurisdictionsDataTable::class);

        //Ruta para acceso a la tabla de documentos
        Route::get('/documents', \App\Http\Controllers\Api\DataTables\DocumentsDataTable::class);

        //Ruta para acceso a la tabla de turnos
        Route::get('/shifts', \App\Http\Controllers\Api\DataTables\ShiftsDataTable::class);

        //Ruta para acceso a la tabla de copias
        Route::get('/copies', \App\Http\Controllers\Api\DataTables\CopyDataTable::class);

        //Ruta para acceso a la tabla de concluidos
        Route::get('/conclusions', \App\Http\Controllers\Api\DataTables\ConclusionsDataTable::class);
    });

    Route::prefix('charts')->group(function () {
        //Ruta para accesder a la grafica de documentos
        Route::get('/documents/{area}/{year}', \App\Http\Controllers\Api\Charts\DocumentsController::class);

        //Ruta para accesder a la grafica de turnos pendientes
        Route::get('/pending/{area}/{year}', \App\Http\Controllers\Api\Charts\PendingShiftsController::class);

        //Ruta para accesder a la grafica de turnos completos
        Route::get('/completed/{area}/{year}', \App\Http\Controllers\Api\Charts\CompletedShiftsController::class);
    });
});
