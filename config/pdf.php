<?php

return [
    'mode'                     => '',
    'format'                   => 'A4',
    'default_font_size'        => '12',
    'default_font'             => 'sans-serif',
    'margin_left'              => 10,
    'margin_right'             => 10,
    'margin_top'               => 10,
    'margin_bottom'            => 10,
    'margin_header'            => 0,
    'margin_footer'            => 0,
    'orientation'              => 'P',
    'title'                    => 'Laravel mPDF',
    'subject'                  => '',
    'author'                   => '',
    'watermark'                => '',
    'show_watermark'           => false,
    'show_watermark_image'     => false,
    'watermark_font'           => 'sans-serif',
    'display_mode'             => 'fullpage',
    'watermark_text_alpha'     => 0.1,
    'watermark_image_path'     => '',
    'watermark_image_alpha'    => 0.2,
    'watermark_image_size'     => 'D',
    'watermark_image_position' => 'P',
    'custom_font_dir'          => '',
    'custom_font_data'         => [],
    'auto_language_detection'  => false,
    'temp_dir'                 => storage_path('app'),
    'pdfa'                     => false,
    'pdfaauto'                 => false,
    'use_active_forms'         => false,
    'custom_font_dir'  => base_path('public/fonts/'), // don't forget the trailing slash!
    'custom_font_data' => [
        'lato' => [ // must be lowercase and snake_case
            'R'  => 'Lato-Regular.ttf',    // regular font
            'B'  => 'Lato-Bold.ttf',       // optional: bold font
            'I'  => 'Lato-Italic.ttf',     // optional: italic font
            'BI' => 'Lato-BoldItalic.ttf' // optional: bold-italic font
        ],
        'cabin' => [
            'R'  => 'Cabin-Regular.ttf',    // regular font
            'B'  => 'Cabin-Bold.ttf',       // optional: bold font
            'I'  => 'Cabin-Italic.ttf',     // optional: italic font
            'BI' => 'Cabin-BoldItalic.ttf' // optional: bold-italic font
        ]
    ]
];
